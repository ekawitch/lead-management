import React from 'react';
import {
    ComponentRegistry,
    COMPONENT_WIDTH
} from './components/BlockComponent'
import Toolbar from '../../components/Toolbar/Toolbar';
import BlockList from './BlockList';
import Properties from './Properties';
import { onTableChange } from './properties/Query/QueryTable';
import { onFieldChange } from './properties/Query/QueryField';
import { onWhereChange } from './properties/Query/QueryWhere';
import { onCommonPropertyChange } from './properties/CommonProperty';
import { autoCode, cloneObject, getNextSeq, removeNode } from '../../service/ObjectService';
import Navbar, { onNavbarChangeHandler } from '../../components/Navbar/Navbar';
import { onSourcePropertyChange } from './properties/SourceProperty';
import { onSegmentationPropertyChange } from './properties/SegmentationProperty/SegmentationProperty';
import { onSourceKeyPropertyChange } from './properties/SourceKeyProperty';
import { onGroupByChange } from './properties/Query/QueryGroupBy';
import { onNotificationPropertyChange } from './properties/NotificationProperty';
import { preventDefault } from '../../service/EventService';
import SQLVisualizer from '../SQLVisualizer/SQLVisualizer';
import { sqlInfo } from '../../payload/sqlinfo';
import { onToolbarChangeHandler } from '../../components/Toolbar/handlers/ToolbarHandler';
import { getUpdatedScore, saveUndoState } from '../../components/Toolbar/handlers/UndoHandler';
import { onOrderByChange } from './properties/Query/QueryOrderBy';
import { onWaterfallAdded } from './shapes/Waterfall';
import { onExcludeAdded } from './shapes/Exclude';
import { onPartitionByChange } from './properties/Query/QueryPartitionBy';
import { useContext } from 'react';
import workflowContext from '../../context/WorkflowContext';
import navbarContext from '../../context/NavbarContext';
import toolbarContext from '../../context/ToolbarContext';
import undoContext from '../../context/UndoContext';
import mouseContext, { mouseStateINI } from '../../context/MouseContext';
import { calcTree, calcTreeOffset, renderBlockLevel, updateChildLevel } from '../../service/WorkflowService';
import { getInputJson } from '../../service/NodeService';
import fieldHelperContext, { fieldHelperINI } from '../../context/FieldHelperContext';
import Template from '../Template/Template';

const Workflow = () => {
    const { workflowState, setWorkflowState } = useContext(workflowContext)
    const { fieldHelperState, setFieldHelperState } = useContext(fieldHelperContext)
    const { navbarState, setNavbarState } = useContext(navbarContext)
    const { toolbarState, setToolbarState } = useContext(toolbarContext)
    const { mouseState, setMouseState } = useContext(mouseContext)
    const { undoState, setUndoState } = useContext(undoContext)

    const handleMouse = (e, shape, code) => {
        const isPropertyActive = document.activeElement.closest("#properties") !== null
        if (isPropertyActive) {
            return
        }

        let drag = undefined

        code = code ? code : "new"

        if (e.type === "mousedown") {//first touch
            if (mouseState.action === "focus" &&
                mouseState.shape === shape &&
                mouseState.code === code) {
                drag = { shape: undefined, id: undefined, action: undefined }
            } else {
                drag = { shape, code, action: "start-move" }
            }
        } else if (e.type === "mouseup") {//release
            const isProperty = e.target.closest("#properties") !== null
            const isModal = e.target.closest(".modal") !== null

            if (mouseState.action === "start-move") {
                drag = { ...mouseState, action: "focus" }
            } else if (!isProperty && !isModal) {
                drag = { shape: undefined, id: undefined, action: undefined }

                if (shape) {
                    handleMouseOnPanel(e, shape, code)
                }
            }
        } else if (e.type === "mousemove" && ["start-move", "move"].indexOf(mouseState.action) > -1) {
            // const canvasPos = document.getElementById("canvas").getBoundingClientRect()
            e.preventDefault()
            drag = {
                ...mouseState,
                x: e.pageX,
                y: e.pageY,
                action: "move"
                // canvasY: e.pageY - canvasPos.y,
                // canvasX: e.pageX - canvasPos.x
            }
        }

        if (drag) {
            const parentCode = getParentOnHover()
            const parentLevel = getLevelOnHover()

            drag.parent = undefined
            if (parentCode) {
                drag.parent = parentCode
                drag.level = undefined
            } else {
                if (mouseState.level !== parentLevel) {
                    drag.level = parentLevel
                }
            }
        }

        if (drag) {
            if (navbarState.focus) {
                setNavbarState({ ...navbarState, focus: undefined })
            }

            if (toolbarState.focus) {
                setToolbarState({ ...toolbarState, focus: undefined })
            }

            setMouseState({ ...drag })
        }
    }

    const getParentOnHover = () => {
        const arr = Array.from(document.getElementsByClassName("blockelem"))
            .filter(ele => {
                const idStr = ele.dataset.code

                if (!idStr) {
                    return false
                }

                const code = ele.dataset.code
                return code && code !== mouseState.code
            }).map(ele => ele.dataset.code)

        for (let idx in arr) {
            const code = arr[idx]
            const isMoving = mouseState.code === code
            const myRef = document.getElementById("box_" + code)

            if (!isMoving && myRef) {
                const myPos = myRef.getBoundingClientRect()

                let isOnMe = myPos.x <= mouseState.x && mouseState.x <= (myPos.x + myPos.width)
                isOnMe = isOnMe && (myPos.y <= mouseState.y && mouseState.y <= (myPos.y + myPos.height))

                if (isOnMe) {
                    return code
                }
            }
        }

        return undefined
    }

    const getLevelOnHover = () => {
        const arr = Array.from(document.getElementsByClassName("block-level"))
            .map(ele => parseInt(ele.dataset.level))

        for (let idx in arr) {
            const level = arr[idx]
            const myRef = document.getElementById("level_" + level)
            if (myRef) {
                const myPos = myRef.getBoundingClientRect()
                let isOnMe = myPos.x <= mouseState.x && mouseState.x <= (myPos.x + myPos.width)
                isOnMe = isOnMe && (myPos.y <= mouseState.y && mouseState.y <= (myPos.y + myPos.height))

                if (isOnMe) {
                    return level
                }
            }
        }

        return undefined
    }

    const handleMouseOnPanel = (e, shape, code) => {
        const blockListPos = document.getElementById("blocklist").getBoundingClientRect()
        const canvasPos = document.getElementById("canvas").getBoundingClientRect()
        const isInBlockList = (blockListPos.x <= e.pageX && e.pageX <= (blockListPos.x + COMPONENT_WIDTH))
            && blockListPos.y <= e.pageY && e.pageY <= (blockListPos.y + blockListPos.height)
        const isInCanvas = (canvasPos.x <= e.pageX && e.pageX <= (canvasPos.x + canvasPos.width))
            && canvasPos.y <= e.pageY && e.pageY <= (canvasPos.y + canvasPos.height)
        let nodes

        // console.log([isInBlockList, isInCanvas, focus])
        if (isInBlockList) { //cancel action, delete
            nodes = removeNode(workflowState.nodes, [code])
        } else if (isInCanvas) {
            nodes = workflowState.nodes
            if (code !== "new") {
                const node = nodes.filter(ele => ele.code === code)[0]
                let newParent
                let newLevel

                if (mouseState.parent) {
                    const parent = nodes.filter(ele => ele.code === mouseState.parent)[0]

                    newParent = parent.code
                    newLevel = parent.level + 1
                } else if (mouseState.level) {
                    newLevel = mouseState.level
                }

                if (newParent || newLevel) {
                    node.parent = newParent
                    node.level = newLevel

                    updateChildLevel(nodes, node)
                }
            } else {
                const unfoldNodes = nodes //expandChild(nodes)
                const nextSeq = getNextSeq(unfoldNodes, "seq")
                let level = 1

                if (mouseState.parent) {
                    level = workflowState.tree[mouseState.parent].level + 1
                } else {
                    if (mouseState.level) {
                        level = mouseState.level
                    }
                }

                const newNode = {
                    ...cloneObject(ComponentRegistry[shape]),
                    seq: nextSeq,
                    parent: mouseState.parent,
                    level: level
                }

                nodes.push(autoCode(newNode, unfoldNodes, "code", "WF_", nextSeq))

                onWaterfallAdded(newNode)
                // if (newNode.shape === "Waterfall") {
                //     if (newNode.parent) {
                //         newNode.datasource = "STAGING"
                //     }
                // }
                onExcludeAdded(newNode)
                // if (newNode.shape === "Exclude") {
                //     newNode.segmentations.push({ seq: 1, value: "Qualified" })
                //     newNode.segmentations.push({ seq: 2, value: "Disqualified" })
                // }
                if (newNode.staging !== undefined) {
                    newNode.staging = newNode.code
                }
            }

        }

        if (nodes !== undefined) {
            setWorkflowState({ ...workflowState, nodes: nodes, tree: calcTreeOffset(calcTree(nodes)) })
            if (saveUndoState(nodes, undoState)) {
                setUndoState({ ...undoState })
            }
        }
    }

    const onPropertyChange = ({ e, sqlInfo }) => {
        const currentTarget = e.currentTarget
        const dataset = currentTarget.dataset
        const event = dataset.event
        const returnEvent = dataset.returnEvent
        const node = workflowState.nodes.filter(ele => ele.code === mouseState.code)[0]
        const undoScore = getUpdatedScore(workflowState.nodes, node)

        // if ("A" === e.currentTarget.tagName) {
        //     e.preventDefault()
        // }
        preventDefault(e)

        if (event === "node-property-close") {
            setMouseState({ ...mouseStateINI })
            return
        }
        if (event === "sql-visualize-on" ||
            event === "sql-visualize-off") {
            setWorkflowState({ ...workflowState, screen: event === "sql-visualize-on" ? "sql-visualize" : "" })
            return
        }

        if (event === "sql-field-helper") {
            onSQLFieldHelperSubmit(e, node)
            return
        }

        if (event === "node-property") {
            onCommonPropertyChange(node, currentTarget)
        } else if (event === "node-datasource") {
            onCommonPropertyChange(node, currentTarget, dataset)
        } else if (event === "node-table") {
            onTableChange(e, node, currentTarget, dataset, workflowState.nodes, sqlInfo)
        } else if (event === "node-field" || returnEvent === "node-field") {
            onFieldChange(node, currentTarget, dataset, workflowState.nodes, sqlInfo)
        } else if (event === "node-group-by") {
            onGroupByChange(node, currentTarget, dataset)
        } else if (event === "node-order-by") {
            onOrderByChange(node, currentTarget, dataset, workflowState.nodes, sqlInfo)
        } else if (event === "node-partition-by") {
            onPartitionByChange(node, currentTarget, dataset)
        } else if (event === "node-condition" || returnEvent === "node-condition") {
            onWhereChange(node, currentTarget)
        } else if (event === "node-notification") {
            onNotificationPropertyChange(node, currentTarget, dataset)
        } else if (event === "make-a-copy") {
            const nextSeq = getNextSeq(workflowState.nodes, "seq")
            const newNode = { ...node, seq: nextSeq, code: "", description: "" }
            workflowState.nodes.push(autoCode(cloneObject(newNode), workflowState.nodes, "code", "WF_", nextSeq))
            workflowState.tree = calcTreeOffset(calcTree(workflowState.nodes))
        } else if (event === "node-source" || event === "source-condition") {
            onSourcePropertyChange(e, node, currentTarget)
        } else if (event === "node-sourcekey") {
            onSourceKeyPropertyChange(e, node, currentTarget)
        } else if (event === "node-segmentation" ||
            event === "segmentation-condition" ||
            event === "attach-outputs") {
            onSegmentationPropertyChange(e, node, workflowState.nodes)
            workflowState.tree = calcTreeOffset(calcTree(workflowState.nodes))
        } else if (event === "table-condition") {
            const tbl = node.tables.filter(ele => ele.seq === parseInt(dataset.tableSeq))[0]
            onWhereChange(tbl, currentTarget)
        }

        setWorkflowState({ ...workflowState })
        if (saveUndoState(workflowState.nodes, undoState, node, undoScore)) {
            setUndoState({ ...undoState })
        }
    }

    const onSQLFieldHelperSubmit = (e, node) => {
        preventDefault(e)

        const currentTarget = e.currentTarget
        const dataset = currentTarget.dataset
        const returnEvent = dataset.returnEvent
        let curVal

        if (dataset.tag) {
            curVal = dataset.tag
        } else if (dataset.table && dataset.property) {
            curVal = dataset.table + "." + dataset.property
        }

        if (dataset.type === "field") {
            const field = node.fields.filter(ele => ele.seq === fieldHelperState.seq)[0]
            field.name = field.name ? field.name : ""

            if (fieldHelperState.append === "Y") {
                field.name += curVal
            } else {
                field.name = curVal
            }

            // if (!field.alias) {
            // field.alias = dataset.property
            // }

            onPropertyChange({
                e: {
                    currentTarget: {
                        dataset: {
                            event: "node-fields-update",
                            fieldSeq: fieldHelperState.seq,
                            value: field.name,
                            append: fieldHelperState.append
                        }
                    }
                }
            })
        } else if (returnEvent === "node-condition") {
            const val = curVal
            onPropertyChange({
                e: {
                    currentTarget: {
                        dataset: {
                            event: returnEvent,
                            conditionSeq: fieldHelperState.seq,
                            field: dataset.field,
                            append: fieldHelperState.append
                        },
                        value: val
                    }
                }
            })
        } else {
            onPropertyChange({
                e: {
                    currentTarget: {
                        dataset: {
                            event: returnEvent,
                            seq: "" + fieldHelperState.seq,
                            tableSeq: fieldHelperState.tableSeq,
                            conditionSeq: fieldHelperState.seq,
                            sourceCode: fieldHelperState.sourceCode ? fieldHelperState.sourceCode : undefined,
                            segmentationSeq: fieldHelperState.segmentationSeq ? fieldHelperState.segmentationSeq : undefined,
                            field: dataset.field,
                            append: fieldHelperState.append
                        },
                        name: fieldHelperState.field,
                        value: curVal
                    }
                }
            })
            // onPropertyChange({ e, val: curVal })
        }

        setFieldHelperState({ ...fieldHelperINI })
    }

    const onFocusHandle = (e) => {
        const currentTarget = e.currentTarget
        const dataset = currentTarget.dataset
        const event = dataset.event
        const id = dataset.id

        preventDefault(e, "A")

        let newFocus = { ...mouseState }
        let newToolState = { ...toolbarState }
        let nodes
        if (event === "toolbar") {
            if (id === "visualize") {
                setWorkflowState({ ...workflowState, screen: "sql-visualize" })
            } else if (id === "visualize-close") {
                setWorkflowState({ ...workflowState, screen: "" })
            } else if (id === "zoom_in") {
                let tool = toolbarState.tool !== "" ? toolbarState.tool : "100%";
                tool = (parseInt(tool.substring(0, tool.length - 1)) + 10) + "%";
                document.getElementById("canvas").style.width = tool
                newToolState.tool = parseInt(toolbarState.tool.substring(0, toolbarState.tool.length - 1)) + 10 + "%";
            } else if (id === "zoom_out") {
                let tool = toolbarState.tool !== "" ? toolbarState.tool : "100%";
                tool = (parseInt(tool.substring(0, tool.length - 1)) - 10) + "%";
                document.getElementById("canvas").style.width = tool
                newToolState.tool = parseInt(toolbarState.tool.substring(0, toolbarState.tool.length - 1)) - 10 + "%";
            } else if (id === "delete" && mouseState.code !== undefined) {
                let code = mouseState.code
                nodes = removeNode(workflowState.nodes, [code])
                newFocus = { shape: undefined, code: undefined, action: undefined }

                setWorkflowState({ ...workflowState, nodes: nodes, tree: calcTreeOffset(calcTree(nodes)) })
                if (saveUndoState(workflowState.nodes, undoState)) {
                    setUndoState({ ...undoState })
                }
            }
        }

        setToolbarState({ ...newToolState })
        setMouseState({ ...newFocus })
    }

    const getOutout = () => {
        return {
            ...workflowState,
            nodes: workflowState.nodes.filter(ele => ele.code && ele.level).map(ele => { return { ...ele, icon: undefined, type: ele.shape } }),
            // tree: undefined
        }
    }

    const renderSampleData = (sampleData) => {
        const workflowSampleData = getInputJson(sampleData)

        setWorkflowState({ ...workflowSampleData, tree: calcTreeOffset(calcTree(workflowSampleData.nodes)) })
        if (saveUndoState(workflowState.nodes, undoState)) {
            setUndoState({ ...undoState })
        }
    }

    const focusNodeRslt = workflowState.nodes.filter(ele => mouseState && ele.code === mouseState.code)
    const focusNode = focusNodeRslt.length > 0 ? focusNodeRslt[0] : undefined

    const onNavbarChange = (e) => {
        const rslt = onNavbarChangeHandler({
            e,
            navState: navbarState,
            renderSampleData
        })

        if (rslt.navState) {
            setNavbarState({ ...rslt.navState })
        }
        if (rslt.workflowState) {
            setWorkflowState({ ...rslt.workflowState, tree: calcTreeOffset(calcTree(rslt.workflowState.nodes)) })
        }
    }

    const onToolbarChange = (e) => {
        let rslt

        rslt = onToolbarChangeHandler({
            e,
            navState: navbarState,
            undo: undoState,
            workflowState,
            focus: mouseState,
            toolState: toolbarState
        })

        if (rslt) {
            if (rslt.navState) {
                setNavbarState({ ...rslt.navState })
            }
            if (rslt.focus) {
                setMouseState({ ...rslt.focus })
            }
            if (rslt.undo) {
                setUndoState({ ...rslt.undo })
            }
            if (rslt.toolState) {
                setToolbarState({ ...rslt.toolState })
            }
            if (rslt.nodes) {
                setWorkflowState({ ...workflowState, nodes: rslt.nodes, tree: calcTreeOffset(calcTree(rslt.nodes)) })

                const currentTarget = e.currentTarget
                if (currentTarget) {
                    const dataset = currentTarget.dataset
                    const id = dataset.id

                    if (id !== "undo" && id !== "redo") {
                        if (saveUndoState(workflowState.nodes, undoState)) {
                            setUndoState({ ...undoState })
                        }
                    }
                }
            }
        }
    }

    const canvasStyle = {
        width: ((workflowState.tree.CANVAS_LEVEL_SPACE * 150) + 500) + "px",
        height: ((workflowState.tree.CANVAS_SPACE * 126) + 100) + "px"
    }
    return <>
        <Navbar
            workflowState={workflowState}
            navState={navbarState}
            onNavbarChange={onNavbarChange}
            getOutout={getOutout}>
        </Navbar>
        <Toolbar
            toolState={toolbarState}
            setToolState={setToolbarState}
            onFocusHandle={onFocusHandle}
            onToolbarChange={onToolbarChange}
            focus={mouseState}
            node={focusNode}
            setFocus={setMouseState}
            removeNode={removeNode}
            calcTreeOffset={calcTreeOffset}
            calcTree={calcTree}
            undo={undoState}
        ></Toolbar>

        {workflowState.screen === "" && <div id="workflow" onMouseMove={handleMouse} onMouseUp={handleMouse}>

            <div className="row">
                <div className="col-2 scroll-y height-85vh">
                    <BlockList focus={mouseState} handleMouse={handleMouse}></BlockList>
                </div>
                <div className="col-10">
                    <div className="canvas-wrapper border">
                        <div id="canvas" style={canvasStyle}>
                            {renderBlockLevel(workflowState, handleMouse, 1, workflowState.tree.CANVAS_SPACE)}
                        </div>
                    </div>
                </div>
                {mouseState.action === "focus" &&
                    focusNode && <Properties onChange={onPropertyChange}
                        sqlInfo={sqlInfo}
                        tree={workflowState.tree}
                        node={focusNode}>
                    </Properties>}
            </div>
        </div>}
        {workflowState.screen === "sql-visualize" && mouseState.code && <SQLVisualizer
            node={focusNode}
            sqlInfo={sqlInfo}
            onChange={onPropertyChange}>
        </SQLVisualizer>}
        {workflowState.screen === "template" && <Template></Template>}
    </>
}
export default Workflow