import { Fragment, useContext } from "react"
import { AGGREGATION_OPERATOR } from "../../../../config/Operator"
import { getDefaultFields, getSQLFields } from "../../../../service/NodeService"
import { getNextSeq } from "../../../../service/ObjectService"
import workflowContext from '../../../../context/WorkflowContext';
import fieldHelperContext, { popFieldHelper } from "../../../../context/FieldHelperContext";

export default function QueryField({
    sqlInfo,
    node,
    onChange }) {
    const { workflowState } = useContext(workflowContext)
    const { setFieldHelperState } = useContext(fieldHelperContext)
    let defaultFields = getDefaultFields(sqlInfo, workflowState.nodes, node)
    const fieldFunctions = [
        {
            id: "TablesAndFields",
            component: <a href="/#"
                className="text-success font-70pc"
                data-is-selection="true"
                data-return-event="node-field"
                data-type="fields"
                data-field="name"
                onClick={(e) => popFieldHelper(e, setFieldHelperState)}>
                Existing Field
            </a>
        },
        {
            id: "AddFunction",
            component: <a href="/#" className="text-success font-70pc" data-event="node-field" name="new-aggregation" onClick={(e) => onChange({ e })}>
                Add Function
            </a>
        },
        {
            id: "Add",
            component: <a href="/#" className="text-success font-70pc" data-event="node-field" name="new" onClick={(e) => onChange({ e })}>
                New Field
            </a>
        },
        {
            id: "empty",
            component: <a href="/#" className="text-success font-70pc" data-event="node-field" name="empty" onClick={(e) => onChange({ e })}>
                Empty
            </a>
        }]

    return (<>
        {!node.isLegacyField && <div className="row">
            <div className="col-12 d-flex justify-content-end my-3">
                {Array.isArray(node.fields) && <>
                    {fieldFunctions.filter(fn => node.fieldFunctions === undefined || node.fieldFunctions.indexOf(fn.id) > -1).map((fn, fnIdx) => <Fragment key={fn.id}>
                        {fnIdx > 0 && <span className="mx-2">/</span>}
                        {fn.component}
                    </Fragment>)}
                </>}
            </div>
        </div>}

        {
            node.isLegacyField && <div className="row">
                <div className="col-12"><strong>Legacy Fields: </strong>{defaultFields.map(fld => fld.alias).join(", ")}</div>
            </div>
        }

        {
            node.isDefaultField && node.fields.length === 0 && <div className="row">
                <div className="col-12"><strong>Default Fields: </strong>{defaultFields.map(fld => fld.alias).join(", ")}</div>
            </div>
        }

        {
            Array.isArray(node.fields) && node.fields.length > 0 && node.fields.map(field => <div key={field.seq} className="row mt-1">
                <div className="col-6">
                    {!field.aggregation && <div className="input-group">
                        <input className="form-control"
                            data-event="node-field"
                            data-seq={field.seq}
                            name="name"
                            value={field.name}
                            onChange={(e) => onChange({ e })}
                            placeholder="field">
                        </input>

                        <button type="button"
                            className="btn btn-outline-secondary icon-more"
                            data-type="field"
                            data-return-event="node-field"
                            data-seq={field.seq}
                            data-append="Y"
                            onClick={(e) => popFieldHelper(e, setFieldHelperState)}>
                        </button>
                    </div>}

                    {field.aggregation && <div className="input-group">
                        <select className="form-control"
                            data-event="node-field"
                            data-seq={field.seq}
                            name="function"
                            value={field.function}
                            onChange={(e) => onChange({ e })}>
                            {!field.function && <option></option>}
                            {AGGREGATION_OPERATOR.map(fn => <option key={fn.value} value={fn.value}>{fn.label}</option>)}
                        </select>
                        <input className="form-control"
                            data-seq={field.seq}
                            value={field.name}
                            readOnly={true}
                            data-return-event="node-field"
                            data-type="field"
                            onClick={(e) => popFieldHelper(e, setFieldHelperState)}>
                        </input>
                    </div>}
                </div>
                <div className="col-4">
                    <input className="form-control" data-event="node-field" data-seq={field.seq} name="alias" value={field.alias} onChange={(e) => onChange({ e })} placeholder="alias"></input>
                </div>
                <div className="col-2">
                    <a href="/#" className="text-danger font-70pc" data-event="node-field" data-seq={field.seq} name="remove" onClick={(e) => onChange({ e })}>remove</a>
                </div>
            </div>)
        }
    </>)
}

export function onFieldChange(
    node,
    currentTarget,
    dataset,
    nodes,
    sqlInfo) {
    const name = currentTarget.name

    if (name === "new" || name === "new-aggregation") {
        const nextSeq = getNextSeq(node.fields, "seq")
        let val = ""
        if (dataset.table && dataset.property) {
            val = dataset.table + "." + dataset.property
        }

        node.fields.push({
            seq: nextSeq,
            aggregation: name === "new-aggregation",
            function: "",
            name: val,
            alias: ""
            // alias: dataset.property
        })
    } else if (name === "remove") {
        const isSelectedAll = node.fields.filter(ele => ele.name === dataset.table + ".*").length > 0

        node.fields = node.fields.filter(ele => ele.seq !== parseInt(dataset.seq))
        if (isSelectedAll) {
            const nextSeq = getNextSeq(node.fields, "seq")
            const sqlTables = getSQLFields(nodes, undefined, sqlInfo, node, dataset.table)
            const validSqlFields = sqlTables.length > 0 ?
                sqlTables[0].properties.filter(fld => fld.name !== dataset.property)
                    .map((vFld, iFld) => {

                        return {
                            seq: nextSeq + iFld,
                            aggregation: false,
                            function: "",
                            name: dataset.table + "." + vFld.name,
                            alias: ""
                        }
                    }) :
                []

            node.fields = [...node.fields, ...validSqlFields]
        }
    } else if (["selectall", "unselectall"].indexOf(name) > -1) {
        const tableAlias = dataset.tableAlias
        const sqlTables = getSQLFields(nodes, undefined, sqlInfo, node, tableAlias)
        const sqlFields = sqlTables.length > 0 ? sqlTables[0].properties
            .map(fld => tableAlias + "." + fld.name) : []

        node.fields = node.fields.filter(fld => fld.name !== tableAlias + ".*" &&
            (sqlFields.indexOf(fld.name) === -1 || fld.alias))

        if (name === "selectall") {
            const nextSeq = getNextSeq(node.fields, "seq")
            node.fields.push({
                seq: nextSeq,
                aggregation: false,
                function: "",
                name: dataset.tableAlias + ".*",
                alias: ""
            })
        }
    }
    else if (name === "empty") {
        node.fields = []
    } else if (["name", "alias", "function"].indexOf(name) > -1) {
        const field = node.fields.filter(ele => ele.seq === parseInt(dataset.seq))[0]

        field[name] = currentTarget.value
    }
}