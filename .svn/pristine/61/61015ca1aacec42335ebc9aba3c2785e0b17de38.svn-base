import { Fragment, useState } from "react"
import { useCallback } from "react"
import { useEffect } from "react"
import { cloneObject } from "../../service/ObjectService"
import Relation from "./Relation"
import { getRelations } from "./SQLVisualizeService"
import TableGroup from "./TableGroup"
import TableView from "./TableView"
import TreeView from "./TreeView"

const SQLVisualizerV2 = ({
    sqlInfo,
    onChange,
    node
}) => {
    // const [mystate, setMystate] = useState({ datasource: "" })
    const schemaArr = sqlInfo.tables.reduce((acc, cur) => {
        if (acc.indexOf(cur.schema) === -1) {
            acc.push(cur.schema)
        }
        return acc
    }, [])

    const relations = getRelations(node)
    const iniDrag = {
        x: undefined,
        y: undefined,
        table: undefined,
        tableSeq: undefined,
        tableAlias: undefined,
        property: undefined
    }
    const [mycanvas, setMycanvas] = useState({ x: undefined, y: undefined })
    const [dragfrom, setDragfrom] = useState(cloneObject(iniDrag))
    const [dragto, setDragto] = useState(cloneObject(iniDrag))

    const addRelation = useCallback(() => {
        const canvas = document.getElementById("sqlCanvas")
        const dataset = canvas.dataset

        let seqVal
        let leftVal
        let rightVal
        if (dataset.fromTable && dataset.toTable) {

            if (parseInt(dataset.fromTableSeq) < parseInt(dataset.toTableSeq)) {
                seqVal = dataset.toTableSeq
                leftVal = dataset.fromTableAlias + "." + dataset.fromProperty
                rightVal = dataset.toTableAlias + "." + dataset.toProperty
            } else {
                seqVal = dataset.fromTableSeq
                leftVal = dataset.toTableAlias + "." + dataset.toProperty
                rightVal = dataset.fromTableAlias + "." + dataset.fromProperty
            }

            if (leftVal !== rightVal) {
                onChange({
                    e: {
                        currentTarget: {
                            name: "condition-new",
                            dataset: {
                                seq: seqVal,
                                event: "node-table",
                                left: leftVal,
                                right: rightVal
                            }
                        }
                    }
                })
            }
        }
    }, [onChange])

    const mouseHandler = useCallback((e) => {
        const canvas = document.getElementById("sqlCanvas")
        const event = e.type

        e.preventDefault()
        if (event === "mousedown") {
            const prop = getTableProperty(e)

            if (prop) {
                setDragfrom({
                    x: e.pageX - mycanvas.x,
                    y: e.pageY - mycanvas.y,
                    table: prop.table,
                    property: prop.property,
                    tableSeq: parseInt(prop.tableSeq),
                    tableAlias: prop.tableAlias
                })
                
                canvas.addEventListener('mousemove', mouseHandler)
                canvas.addEventListener('mouseup', mouseHandler)
            }
        } else if (event === "mousemove") {
            const prop = getTableProperty(e)
            setDragto({
                x: e.pageX - mycanvas.x,
                y: e.pageY - mycanvas.y,
                table: prop ? prop.table : undefined,
                property: prop ? prop.property : undefined,
                tableSeq: prop ? parseInt(prop.tableSeq) : undefined,
                tableAlias: prop ? prop.tableAlias : undefined
            })
        } else if (event === "mouseup") {
            addRelation()
            setDragfrom({
                x: undefined,
                y: undefined,
                table: undefined,
                tableSeq: undefined,
                tableAlias: undefined,
                property: undefined
            })
            setDragto({
                x: undefined,
                y: undefined,
                table: undefined,
                tableSeq: undefined,
                tableAlias: undefined,
                property: undefined
            })

            canvas.removeEventListener('mousemove', mouseHandler)
            canvas.removeEventListener('mouseup', mouseHandler)
        }
    }, [mycanvas, setDragfrom, setDragto, addRelation])

    const getTableProperty = (e) => {
        const fields = [...document.getElementsByClassName("data-table-field")]
            .filter(fld => {
                const fldPos = fld.getBoundingClientRect()
                const x = fldPos.x

                if (x <= e.pageX) {

                    const w = fldPos.width
                    if (e.pageX <= (x + w)) {

                        const y = fldPos.top
                        if (y <= e.pageY) {

                            const h = fldPos.height
                            if (e.pageY <= (y + h)) {

                                return true
                            }
                        }
                    }
                }

                return false
            })

        return fields.length === 0 ? undefined : fields[0].dataset
    }

    // const onSQLVisualizerChange = (e) => {
    //     const currentTarget = e.currentTarget
    //     const dataset = currentTarget.dataset
    //     const event = dataset.event


    //     if (event === "datasource") {
    //         mystate.datasource = currentTarget.value
    //     }

    //     setMystate({ ...mystate })
    // }

    useEffect(() => {
        const canvas = document.getElementById("sqlCanvas")
        const canvasPos = canvas.getBoundingClientRect()

        setMycanvas({ x: canvasPos.left, y: canvasPos.top })
    }, [setMycanvas])

    useEffect(() => {
        const canvas = document.getElementById("sqlCanvas")
        canvas.addEventListener('mousedown', mouseHandler)

        return () => {
            canvas.removeEventListener('mousedown', mouseHandler)
        }
    }, [mouseHandler])

    return <>


        <div id="workflow">
            <div className="row">
                <div className="col-12 d-flex justify-content-end">
                    <a href="/#"
                        name="remove"
                        className="text-danger m-2"
                        data-event="sql-visualize-off"
                        onClick={(e) => onChange({ e })}>
                        <i className="fa fa-close"></i>
                    </a>
                </div>
            </div>


            <div className="row">
                <div className="col-12">
                    <div id="sqlCanvas"
                        className="canvas"
                        data-from-table={dragfrom.table}
                        data-from-table-seq={dragfrom.tableSeq}
                        data-from-table-alias={dragfrom.tableAlias}
                        data-from-property={dragfrom.property}
                        data-to-table={dragto.table}
                        data-to-table-seq={dragto.tableSeq}
                        data-to-table-alias={dragto.tableAlias}
                        data-to-property={dragto.property}>
                        {node.tables.map(tbl => {
                            const tableInfo = sqlInfo.tables.filter(ele => ele.name === tbl.name)[0]
                            return <TableGroup key={tbl.seq}>
                                <TableView
                                    node={node}
                                    added={tbl}
                                    table={tableInfo}
                                    dragfrom={dragfrom}
                                    dragto={dragto}
                                    onChange={onChange}>
                                </TableView>
                            </TableGroup>
                        })}
                        <Relation
                            dragfrom={dragfrom}
                            dragto={dragto}>
                        </Relation>
                        {mycanvas.x !== undefined && relations.map(ele => <Relation
                            relation={ele}
                            canvas={mycanvas}
                            key={[ele.tableSeq, ele.seq]}></Relation>)}

                    </div>

                </div>
            </div>
        </div>
    </>
}

export default SQLVisualizerV2