import React from 'react'
import * as Waterfall from '../shapes/Waterfall'
import * as Merge from '../shapes/Merge'
import * as Exclude from '../shapes/Exclude'
import * as Segmentation from '../shapes/Segmentation'
import * as Notification from '../shapes/Notification'
import * as Populate from '../shapes/Populate'
import * as Input from '../shapes/Input'
import * as Output from '../shapes/Output'
import * as Consolidate from '../shapes/Consolidate'
import * as TopPriority from '../shapes/TopPriority'
import * as SQL from '../shapes/SQL'
import * as Workflow from '../shapes/Workflow'
import * as Analytic from '../shapes/Analytic'

const BlockComponent = ({
    shape,
    // icon,
    // image,
    seq,
    title,
    description,
    code,
    parent,
    segmentations,
    handleMouse,
    level,
    offset,
    relOffset,
    space,
    focus,
    staging
}) => {
    const style = {}
    const htmlClasses = ["blockelem"]

    if (relOffset || space > 1) {
        const BOX_HEIGHT = (COMPONENT_HEIGHT + COMPONENT_SPACE_Y)
        style.marginTop = (relOffset * BOX_HEIGHT) + "px"
    }

    if (code) {
        if (focus.shape === shape && focus.code === code) {

            if (code) {
                htmlClasses.push("selectedblock")
            }

            if (focus.action === "move") {
                htmlClasses.push("dragging")
                style.marginTop = "0px"
                style.top = (focus.y - (COMPONENT_HEIGHT / 2)) + "px"
                style.left = (focus.x - (COMPONENT_WIDTH / 2)) + "px"
            }
        }

        if (focus.parent) {
            const isNodeFocused = focus.parent === code
            if (isNodeFocused) {
                htmlClasses.push("parent-focus")
            }
        }

        if (!code) {
            htmlClasses.push("dragging")
        }

        if (Array.isArray(segmentations) && segmentations.length > 1) {
            style.height = (segmentations.length * COMPONENT_HEIGHT) + "px"
        }
    }

    const isMoving = focus.code === code && focus.action === "move"

    return (<>
        <div
            id={"box_" + code}
            data-level={level}
            data-space={space}
            data-offset={offset}
            data-rel-offset={relOffset}
            className={htmlClasses.join(" ")}
            style={style}
            data-shape={shape}
            data-seq={seq}
            data-code={code}
            data-parent={parent}
            onMouseDown={(e) => handleMouse(e, shape, code)}
            onMouseUp={(e) => handleMouse(e, shape, code)}
            onMouseOver={(e) => handleMouse(e, shape, code)}>


            {code && !isMoving && <>
                {/* <div className="blockdesc">{staging ? staging : description}</div> */}
                <div className="blockdesc"><strong>{staging ? staging : code}</strong> {description}</div>
            </>}

            <div className="blockin">
                <div className="blocktext">
                    <p className="blocktitle">{title}</p>
                    <div className="blockico">
                        <img alt={shape} src={"./assets/icons/" + ComponentRegistry[shape].image}></img>
                    </div>
                </div>
            </div>
        </div>
    </>)
}

const COMPONENT_WIDTH = 150
const COMPONENT_HEIGHT = 126
const COMPONENT_SPACE_X = 0
const COMPONENT_SPACE_Y = 0
const ComponentRegistry = {
    Waterfall: Waterfall.Props,
    Merge: Merge.Props,
    Exclude: Exclude.Props,
    Segmentation: Segmentation.Props,
    Notification: Notification.Props,
    Populate: Populate.Props,
    TopPriority: TopPriority.Props,
    SQL: SQL.Props,
    Workflow: Workflow.Props,
    Analytic: Analytic.Props,
    Input: Input.Props,
    Output: Output.Props,
    Consolidate: Consolidate.Props
}
const TabRegistry = {
    Waterfall: Waterfall.getTabs(),
    Merge: Merge.getTabs(),
    Exclude: Exclude.getTabs(),
    Segmentation: Segmentation.getTabs(),
    Notification: Notification.getTabs(),
    TopPriority: TopPriority.getTabs(),
    Analytic: Analytic.getTabs(),
    Input: Input.getTabs()
}

export default BlockComponent
export {
    TabRegistry,
    COMPONENT_WIDTH,
    COMPONENT_HEIGHT,
    COMPONENT_SPACE_X,
    COMPONENT_SPACE_Y,
    ComponentRegistry
}