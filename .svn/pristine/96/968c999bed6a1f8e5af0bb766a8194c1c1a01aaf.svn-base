import React from 'react'
import * as Waterfall from './Waterfall'
import * as Merge from './Merge'
import * as Exclude from './Exclude'

const Render = ({ shape, icon, title, description, code, parent, onSelected, handleMouse, level, offset, space, focus }) => {
    const style = {}
    const htmlClasses = ["blockelem"]
    const onClick = (shape, code) => {
        onSelected(shape, code)
    }

    if (code) {
        htmlClasses.push("block")
        const isMoving = focus.code === code
        const myRef = document.getElementById("shape_" + code)

        if (!isMoving && myRef) {
            const myPos = myRef.getBoundingClientRect()
            let isOnMe = myPos.x <= focus.x && focus.x <= (myPos.x + COMPONENT_WIDTH)
            isOnMe = isOnMe && (myPos.y <= focus.y && focus.y <= (myPos.y + COMPONENT_HEIGHT))

            if (isOnMe) {
                if (focus.parent !== code) {
                    handleMouse({ type: "mouseonme" }, shape, code ? code : "")
                }
            } else {
                if (focus.parent === code) {
                    handleMouse({ type: "mouseonme" }, shape, undefined)
                }
            }
        }
    }

    if (focus.parent && focus.parent === code) {
        htmlClasses.push("parent-focus")
    }


    if (level) {
        // style.top = ((level - 1) * 120) + "px"
        style.top = ((level - 1) * (COMPONENT_HEIGHT + COMPONENT_SPACE_Y)) + "px"
    }
    if (offset || space) {
        // style.left = (offset * 350) + "px"
        const BOX_WIDTH = (COMPONENT_WIDTH + COMPONENT_SPACE_X)
        style.left = ((offset + ((space - 1) / 2)) * BOX_WIDTH) + "px"
    }

    if (code) {
        if (focus.shape === shape && focus.code === code) {
            htmlClasses.push("selectedblock")

            if (focus.action === "move") {
                htmlClasses.push("dragging")
                style.top = (focus.y - (COMPONENT_HEIGHT / 2)) + "px"
                style.left = (focus.x - (COMPONENT_WIDTH / 2)) + "px"
            }
        }

        if (!code) {
            htmlClasses.push("dragging")
        }
    }


    return (<div id={"shape_" + code} className={htmlClasses.join(" ")} onClick={e => onClick(shape, code)} style={style} data-id={id} data-parent={parent}
        onMouseDown={(e) => handleMouse(e, shape, code)}
        onMouseUp={(e) => handleMouse(e, shape, code)}
        onMouseOver={(e) => handleMouse(e, shape, code)}>
        <div className="grabme">
            <img src="assets/grabme.svg"></img>
        </div>
        <div className="blockin">
            <div className="blockico">
                <span></span>
                <i className={Array.isArray(icon) ? icon.join(" ") : ""}></i>
            </div>
            <div className="blocktext">
                <p className="blocktitle">{title}</p>
                <p className="blockdesc">{description}</p>
            </div>
        </div>
    </div>)
}
const COMPONENT_WIDTH = 318
const COMPONENT_HEIGHT = 78
const COMPONENT_SPACE_X = 45
const COMPONENT_SPACE_Y = 55
const ComponentRegistry = {
    Waterfall,
    Merge,
    Exclude
}

export {
    Render,
    COMPONENT_WIDTH,
    COMPONENT_HEIGHT,
    COMPONENT_SPACE_X,
    COMPONENT_SPACE_Y,
    ComponentRegistry
}