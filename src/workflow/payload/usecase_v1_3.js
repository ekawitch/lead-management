const usecase_v1_3 = {
  "nodes": [
    {
      "shape": "Waterfall",
      "title": "Waterfall",
      "description": "Waterfall",
      "image": "waterfall.png",
      "datasource": "CRMWORK",
      "tables": [
        {
          "seq": 1,
          "name": "crm.account",
          "alias": "a11",
          "join": "",
          "conditions": []
        },
        {
          "seq": 2,
          "name": "crm.customer",
          "alias": "a12",
          "join": "INNER",
          "conditions": [
            {
              "seq": 1,
              "connection": "",
              "connector": "",
              "operator": "EQ",
              "left": "a11.customer_key",
              "right": "a12.customer_key"
            },
            {
              "seq": 2,
              "cmd": "AND a12.GBI NOT IN ('I')"
            }
          ]
        },
        {
          "seq": 3,
          "name": "crmwork.etm_registered_kcc_vip",
          "alias": "a13",
          "join": "LEFT",
          "conditions": [
            {
              "seq": 1,
              "connection": "",
              "connector": "",
              "operator": "EQ",
              "left": "a11.account_key",
              "right": "a13.account_key"
            }
          ]
        },
        {
          "seq": 4,
          "name": "crm.address_info",
          "alias": "a14",
          "join": "INNER",
          "conditions": [
            {
              "seq": 1,
              "connection": "",
              "connector": "",
              "operator": "EQ",
              "left": "a11.customer_key",
              "right": "a14.customer_key"
            },
            {
              "seq": 2,
              "connection": "",
              "connector": "AND",
              "operator": "EQ",
              "left": "a11.business_code",
              "right": "a14.business_code"
            }
          ]
        }
      ],
      "fields": [
        {
          "seq": 1,
          "aggregation": false,
          "function": "",
          "name": "a11.account_key",
          "alias": ""
        },
        {
          "seq": 2,
          "aggregation": false,
          "function": "",
          "name": "a11.account_number",
          "alias": ""
        },
        {
          "seq": 3,
          "aggregation": false,
          "function": "",
          "name": "a11.customer_key",
          "alias": ""
        },
        {
          "seq": 4,
          "aggregation": false,
          "function": "",
          "name": "a11.business_code",
          "alias": ""
        },
        {
          "seq": 5,
          "aggregation": false,
          "function": "",
          "name": "a11.account_status_key_0",
          "alias": ""
        },
        {
          "seq": 6,
          "aggregation": false,
          "function": "",
          "name": "a11.account_opened_date",
          "alias": ""
        },
        {
          "seq": 7,
          "aggregation": false,
          "function": "",
          "name": "a11.credit_limit_0",
          "alias": ""
        },
        {
          "seq": 8,
          "aggregation": false,
          "function": "",
          "name": "a11.nationality",
          "alias": ""
        },
        {
          "seq": 9,
          "aggregation": false,
          "function": "",
          "name": "SUBSTR(a12.nationality, 1, 2)",
          "alias": "substr_nationality"
        },
        {
          "seq": 10,
          "aggregation": false,
          "function": "",
          "name": "CASE            WHEN a12.CUSTOMER_FIXED_INCOME >= a12.CUSTOMER_INCOME THEN 1            ELSE 0        END",
          "alias": "income_flg"
        },
        {
          "seq": 11,
          "aggregation": false,
          "function": "",
          "name": "a12.customer_fixed_income",
          "alias": ""
        },
        {
          "seq": 12,
          "aggregation": false,
          "function": "",
          "name": "a12.customer_income",
          "alias": ""
        },
        {
          "seq": 13,
          "aggregation": false,
          "function": "",
          "name": "a11.risk_grade",
          "alias": ""
        },
        {
          "seq": 14,
          "aggregation": false,
          "function": "",
          "name": "a12.gbi",
          "alias": ""
        },
        {
          "seq": 15,
          "aggregation": false,
          "function": "",
          "name": "a11.last_payment_date",
          "alias": ""
        },
        {
          "seq": 16,
          "aggregation": false,
          "function": "",
          "name": "a11.last_purchase_date",
          "alias": ""
        },
        {
          "seq": 17,
          "aggregation": false,
          "function": "",
          "name": "CASE            WHEN a14.CURRENT_REGION = 'กรุงเทพและปริมณฑล' THEN 'BKK'            ELSE 'UPC'        END",
          "alias": "region_eng"
        }
      ],
      "conditions": [
        {
          "seq": 1,
          "cmd": "1=1\n  AND a11.BUSINESS_CODE IN ('KCCVS',\n                            'KCCMC',\n                            'HPVS')\n  AND a11.ACCOUNT_STATUS_KEY_0 in ('101',\n                                   '111')\n  AND a11.ACCOUNT_OPENED_DATE BETWEEN to_date('2021-01-01', 'yyyy-mm-dd') AND to_date('2021-03-31', 'yyyy-mm-dd')\n  AND (a11.CREDIT_LIMIT_0 >= 40000\n       OR (a11.b_score >= 800\n           AND a11.credit_limit_0 >= 30000))\n  AND SUBSTR(a12.nationality, 1, 2) LIKE 'T%'\n  AND a13.account_key IS NULL --filter VIP\nAND CASE\n        WHEN a12.CUSTOMER_FIXED_INCOME >= a12.CUSTOMER_INCOME THEN 1\n        ELSE 0\n    END = 1\n  AND a11.customer_key NOT IN\n    (SELECT DISTINCT CUSTOMER_KEY\n     FROM CRMWORK.MISUSE_ALLPRODUCT\n     WHERE entity='KCS')\n  AND NOT EXISTS\n    (SELECT 1\n     FROM crmwork.view_bankruptcy x11\n     WHERE x11.customer_key = a11.CUSTOMER_KEY)\n  AND a11.account_key NOT IN\n    (SELECT DISTINCT x122.account_key\n     FROM crm.campaign x121\n     JOIN crm.contact_history x122 ON x121.campcode = x122.campcode\n     WHERE x121.campcode like 'SFK%'\n       AND x121.start_date <= SYSDATE - 30\n       AND x121.start_date >= SYSDATE - 60)"
        }
      ],
      "groupBy": [],
      "orderBy": [],
      "queryCmd": true,
      "oracleNo": 1,
      "isSqlVisualizer": true,
      "isQueryProperty": true,
      "isTemplate": false,
      "seq": 1,
      "level": 1,
      "code": "WF_1",
      "type": "Waterfall"
    },
    {
      "shape": "TopPriority",
      "title": "Top Priority",
      "datasource": "STAGING",
      "datasourceReadOnly": true,
      "description": "TopPriority",
      "image": "top_priority.png",
      "partitionBy": [
        {
          "seq": 1,
          "name": "a11.customer_key"
        }
      ],
      "tables": [
        {
          "seq": 1,
          "name": "WF_1",
          "alias": "a11",
          "join": "",
          "conditions": []
        }
      ],
      "fields": [
        {
          "seq": 1,
          "aggregation": false,
          "function": "",
          "name": "a11.*",
          "alias": ""
        }
      ],
      "conditions": [],
      "orderBy": [
        {
          "seq": 1,
          "name": "GREATEST(nvl(a11.last_payment_date, to_date('1900-01-01', 'yyyy-mm-dd')), nvl(a11.last_purchase_date, to_date('1900-01-01', 'yyyy-mm-dd')))",
          "direction": "DESC"
        }
      ],
      "queryCmd": true,
      "oracleNo": 1,
      "isQueryProperty": true,
      "seq": 2,
      "parent": "WF_1",
      "level": 2,
      "code": "WF_2",
      "type": "TopPriority"
    },
    {
      "shape": "Waterfall",
      "title": "Waterfall",
      "description": "Group",
      "image": "waterfall.png",
      "datasource": "STAGING",
      "tables": [
        {
          "seq": 1,
          "name": "WF_2",
          "alias": "a11",
          "join": "",
          "conditions": []
        }
      ],
      "fields": [
        {
          "seq": 1,
          "aggregation": false,
          "function": "",
          "name": "a11.*",
          "alias": ""
        },
        {
          "seq": 2,
          "aggregation": false,
          "function": "",
          "name": "CASE WHEN a11.risk_grade = 10 THEN                          CASE                           WHEN a11.gbi = 'G' THEN 'group_a'                          WHEN a11.gbi = 'I' THEN 'group_b'                          ELSE 'group_c'                          END WHEN a11.risk_grade = 20 THEN 'group_d' END",
          "alias": "segment_lv1"
        }
      ],
      "conditions": [],
      "groupBy": [],
      "orderBy": [],
      "queryCmd": true,
      "oracleNo": 1,
      "isSqlVisualizer": true,
      "isQueryProperty": true,
      "isTemplate": false,
      "seq": 3,
      "parent": "WF_2",
      "level": 3,
      "code": "WF_3",
      "type": "Waterfall"
    },
    {
      "shape": "Segmentation",
      "title": "Segmentation",
      "description": "Segmentation",
      "image": "segmentation.png",
      "datasource": "STAGING",
      "datasourceReadOnly": true,
      "segmentations": [
        {
          "seq": 1,
          "value": "GROUPA",
          "conditions": [
            {
              "seq": 1,
              "name": "",
              "left": "WF_3.segment_lv1",
              "right": "'group_a'",
              "operator": "EQ",
              "connector": ""
            }
          ]
        },
        {
          "seq": 2,
          "value": "GROUPB",
          "conditions": [
            {
              "seq": 1,
              "name": "",
              "left": "WF_3.segment_lv1",
              "right": "'group_b'",
              "operator": "EQ",
              "connector": ""
            }
          ]
        },
        {
          "seq": 3,
          "value": "GROUPC",
          "conditions": [
            {
              "seq": 1,
              "name": "",
              "left": "WF_3.segment_lv1",
              "right": "'group_c'",
              "operator": "EQ",
              "connector": ""
            }
          ]
        },
        {
          "seq": 4,
          "value": "GROUPD",
          "conditions": [
            {
              "seq": 1,
              "name": "",
              "left": "WF_3.segment_lv1",
              "right": "'group_d'",
              "operator": "EQ",
              "connector": ""
            }
          ]
        }
      ],
      "isLegacyField": true,
      "segmentType": "CONDITION",
      "segmentPrefix": "GROUP",
      "fields": [],
      "queryCmd": false,
      "oracleNo": 1,
      "isStagingProperty": true,
      "seq": 4,
      "parent": "WF_3",
      "level": 4,
      "code": "WF_4",
      "type": "Segmentation"
    },
    {
      "parentSegment": 1,
      "shape": "Output",
      "title": "Output",
      "description": "GROUP A",
      "image": "output.png",
      "isAutoField": true,
      "oracleNo": 1,
      "queryCmd": true,
      "seq": 5,
      "parent": "WF_4",
      "level": 5,
      "type": "Output",
      "code": "WF_5",
      "staging": "WF_5"
    },
    {
      "parentSegment": 2,
      "shape": "Output",
      "title": "Output",
      "description": "GROUP B",
      "image": "output.png",
      "isAutoField": true,
      "oracleNo": 1,
      "queryCmd": true,
      "seq": 6,
      "parent": "WF_4",
      "level": 5,
      "type": "Output",
      "code": "WF_6",
      "staging": "WF_6"
    },
    {
      "parentSegment": 3,
      "shape": "Output",
      "title": "Output",
      "description": "GROUP C",
      "image": "output.png",
      "isAutoField": true,
      "oracleNo": 1,
      "queryCmd": true,
      "seq": 7,
      "parent": "WF_4",
      "level": 5,
      "type": "Output",
      "code": "WF_7",
      "staging": "WF_7"
    },
    {
      "parentSegment": 4,
      "shape": "Output",
      "title": "Output",
      "description": "GROUP D",
      "image": "output.png",
      "isAutoField": true,
      "oracleNo": 1,
      "queryCmd": true,
      "seq": 8,
      "parent": "WF_4",
      "level": 5,
      "type": "Output",
      "code": "WF_8",
      "staging": "WF_8"
    },
    {
      "shape": "Waterfall",
      "title": "Waterfall",
      "description": "GROUP X",
      "image": "waterfall.png",
      "datasource": "STAGING",
      "tables": [
        {
          "seq": 1,
          "name": "WF_2",
          "alias": "a11",
          "join": "",
          "conditions": []
        }
      ],
      "fields": [
        {
          "seq": 1,
          "aggregation": false,
          "function": "",
          "name": "a11.*",
          "alias": ""
        },
        {
          "seq": 2,
          "aggregation": false,
          "function": "",
          "name": "'group_x'",
          "alias": "segment_lv2"
        }
      ],
      "conditions": [],
      "groupBy": [],
      "orderBy": [],
      "queryCmd": true,
      "oracleNo": 1,
      "isSqlVisualizer": true,
      "isQueryProperty": true,
      "isTemplate": false,
      "seq": 9,
      "level": 5,
      "code": "WF_9",
      "type": "Waterfall"
    },
    {
      "shape": "Merge",
      "title": "Merge",
      "description": "Merge",
      "image": "merge.png",
      "parentLabel": "Merge",
      "sourceLabel": "with",
      "conditions": [],
      "isLegacyField": true,
      "datasource": "STAGING",
      "datasourceReadOnly": true,
      "sources": [
        {
          "code": "WF_5",
          "seq": 1,
          "conditions": []
        },
        {
          "code": "WF_9",
          "seq": 2,
          "conditions": []
        }
      ],
      "queryCmd": true,
      "oracleNo": 1,
      "isStagingProperty": true,
      "seq": 10,
      "parent": "WF_8",
      "level": 6,
      "code": "WF_10",
      "type": "Merge"
    },
    {
      "shape": "Waterfall",
      "title": "Waterfall",
      "description": "Lead X1",
      "image": "waterfall.png",
      "datasource": "CRMWORK",
      "tables": [
        {
          "seq": 1,
          "name": "crm.account",
          "alias": "a11",
          "join": "",
          "conditions": []
        }
      ],
      "fields": [
        {
          "seq": 1,
          "aggregation": false,
          "function": "",
          "name": "a11.account_key",
          "alias": ""
        }
      ],
      "conditions": [],
      "groupBy": [],
      "orderBy": [],
      "queryCmd": true,
      "oracleNo": 1,
      "isSqlVisualizer": true,
      "isQueryProperty": true,
      "isTemplate": false,
      "seq": 11,
      "level": 6,
      "code": "WF_11",
      "type": "Waterfall"
    },
    {
      "shape": "Waterfall",
      "title": "Waterfall",
      "description": "Lead X2",
      "image": "waterfall.png",
      "datasource": "CRMWORK",
      "tables": [
        {
          "seq": 1,
          "name": "crm.account",
          "alias": "a11",
          "join": "",
          "conditions": []
        }
      ],
      "fields": [
        {
          "seq": 1,
          "aggregation": false,
          "function": "",
          "name": "a11.customer_key",
          "alias": ""
        }
      ],
      "conditions": [],
      "groupBy": [],
      "orderBy": [],
      "queryCmd": true,
      "oracleNo": 1,
      "isSqlVisualizer": true,
      "isQueryProperty": true,
      "isTemplate": false,
      "seq": 12,
      "level": 6,
      "code": "WF_12",
      "type": "Waterfall"
    },
    {
      "shape": "Exclude",
      "title": "Exclude",
      "description": "Exclude",
      "image": "excluding.png",
      "isSegmentationReadOnly": true,
      "segmentations": [
        {
          "seq": 1,
          "value": "Qualified"
        },
        {
          "seq": 2,
          "value": "Disqualified"
        }
      ],
      "isLegacyField": true,
      "datasource": "STAGING",
      "datasourceReadOnly": true,
      "sources": [
        {
          "code": "WF_11",
          "seq": 1,
          "conditions": [
            {
              "seq": 1,
              "name": "",
              "left": "WF_10.account_key",
              "right": "WF_11.account_key",
              "operator": "NIN",
              "connector": ""
            }
          ]
        },
        {
          "code": "WF_12",
          "seq": 2,
          "conditions": [
            {
              "seq": 1,
              "name": "",
              "left": "WF_10.customer_key",
              "right": "WF_12.customer_key",
              "operator": "NIN",
              "connector": ""
            }
          ]
        }
      ],
      "parentLabel": "Exclude",
      "sourceLabel": "from",
      "oracleNo": 1,
      "isIgnoreSegmentationPrefix": true,
      "isStagingProperty": true,
      "seq": 13,
      "parent": "WF_10",
      "level": 7,
      "code": "WF_13",
      "type": "Exclude"
    },
    {
      "parentSegment": 1,
      "shape": "Output",
      "title": "Output",
      "description": "Qualified",
      "image": "output.png",
      "isAutoField": true,
      "oracleNo": 1,
      "queryCmd": true,
      "seq": 14,
      "parent": "WF_13",
      "level": 8,
      "type": "Output",
      "code": "WF_14",
      "staging": "WF_14"
    },
    {
      "parentSegment": 2,
      "shape": "Output",
      "title": "Output",
      "description": "Disqualified",
      "image": "output.png",
      "isAutoField": true,
      "oracleNo": 1,
      "queryCmd": true,
      "seq": 15,
      "parent": "WF_13",
      "level": 8,
      "type": "Output",
      "code": "WF_15",
      "staging": "WF_15"
    },
    {
      "shape": "Segmentation",
      "title": "Segmentation",
      "description": "Segmentation",
      "image": "segmentation.png",
      "datasource": "STAGING",
      "datasourceReadOnly": true,
      "segmentations": [
        {
          "seq": 1,
          "value": "GROUPE",
          "conditions": [
            {
              "seq": 1,
              "name": "",
              "left": "MOD(round_robin, 5)",
              "right": "(0,3,1,4)",
              "operator": "IN",
              "connector": ""
            }
          ],
          "capacity": "80"
        },
        {
          "seq": 2,
          "value": "GROUPF",
          "conditions": [
            {
              "seq": 1,
              "name": "",
              "left": "MOD(round_robin, 5)",
              "right": "(2)",
              "operator": "IN",
              "connector": ""
            }
          ],
          "capacity": "20"
        }
      ],
      "isLegacyField": true,
      "segmentType": "ROUNDROBIN",
      "segmentPrefix": "GROUP",
      "fields": [],
      "queryCmd": false,
      "oracleNo": 1,
      "isStagingProperty": true,
      "seq": 16,
      "parent": "WF_14",
      "level": 9,
      "code": "WF_16",
      "orderBy": [],
      "type": "Segmentation"
    },
    {
      "parentSegment": 1,
      "shape": "Output",
      "title": "Output",
      "description": "GROUP E",
      "image": "output.png",
      "isAutoField": true,
      "oracleNo": 1,
      "queryCmd": true,
      "seq": 17,
      "parent": "WF_16",
      "level": 10,
      "type": "Output",
      "code": "WF_17",
      "staging": "WF_17"
    },
    {
      "parentSegment": 2,
      "shape": "Output",
      "title": "Output",
      "description": "GROUP F",
      "image": "output.png",
      "isAutoField": true,
      "oracleNo": 1,
      "queryCmd": true,
      "seq": 18,
      "parent": "WF_16",
      "level": 10,
      "type": "Output",
      "code": "WF_18",
      "staging": "WF_18"
    },
    {
      "shape": "Waterfall",
      "title": "Waterfall",
      "description": "Calculated",
      "image": "waterfall.png",
      "datasource": "STAGING",
      "tables": [
        {
          "seq": 1,
          "name": "WF_17",
          "alias": "a11",
          "join": "",
          "conditions": []
        }
      ],
      "fields": [
        {
          "seq": 1,
          "aggregation": false,
          "function": "",
          "name": "a11.*",
          "alias": ""
        },
        {
          "seq": 2,
          "aggregation": false,
          "function": "",
          "name": "trunc(LEAST(nvl(a11.customer_fixed_income, 0), nvl(a11.customer_income, 0)) * 5, -3)",
          "alias": "offer_new_credit_limit"
        },
        {
          "seq": 3,
          "aggregation": false,
          "function": "",
          "name": "rownum",
          "alias": "no"
        },
        {
          "seq": 4,
          "aggregation": false,
          "function": "",
          "name": "count(*) over()",
          "alias": "tot"
        }
      ],
      "conditions": [],
      "groupBy": [],
      "orderBy": [],
      "queryCmd": true,
      "oracleNo": 1,
      "isSqlVisualizer": true,
      "isQueryProperty": true,
      "isTemplate": true,
      "seq": 19,
      "parent": "WF_17",
      "level": 11,
      "code": "WF_19",
      "type": "Waterfall"
    },
    {
      "shape": "Waterfall",
      "title": "Waterfall",
      "description": "Final",
      "image": "waterfall.png",
      "datasource": "STAGING",
      "tables": [
        {
          "seq": 1,
          "name": "WF_19",
          "alias": "b11",
          "join": "",
          "conditions": []
        }
      ],
      "fields": [
        {
          "seq": 2,
          "aggregation": false,
          "function": "",
          "name": "b11.*",
          "alias": ""
        },
        {
          "seq": 3,
          "aggregation": false,
          "function": "",
          "name": "trunc(b11.offer_new_credit_limit/12, -3)",
          "alias": "offer_new_credit_limit_12"
        },
        {
          "seq": 4,
          "aggregation": false,
          "function": "",
          "name": "round(b11.no * 100 / b11.tot)",
          "alias": "running_percentage"
        },
        {
          "seq": 5,
          "aggregation": false,
          "function": "",
          "name": "GET_REF_NO(b11.business_code)",
          "alias": "ref_no"
        },
        {
          "seq": 6,
          "aggregation": false,
          "function": "",
          "name": "case when business_code = 'KCC' then 'KCC' when business_code = 'CCC' then 'CCC' else 'Others' end",
          "alias": "test_bus_code"
        },
        {
          "seq": 7,
          "aggregation": false,
          "function": "",
          "name": "least( credit_limit_0,offer_new_credit_limit)",
          "alias": "new_credit_line1"
        },
        {
          "seq": 8,
          "aggregation": false,
          "function": "",
          "name": "greatest( credit_limit_0,offer_new_credit_limit)",
          "alias": "new_credit_line2"
        }
      ],
      "conditions": [],
      "groupBy": [],
      "orderBy": [],
      "queryCmd": true,
      "oracleNo": 1,
      "isSqlVisualizer": true,
      "isQueryProperty": true,
      "isTemplate": false,
      "seq": 20,
      "parent": "WF_19",
      "level": 12,
      "code": "WF_20",
      "type": "Waterfall"
    }
  ],
  "tree": {
    "WF_20": {
      "seq": 20,
      "parent": "WF_19",
      "children": [],
      "space": 1,
      "level": 12,
      "offset": 3,
      "relOffset": 3
    },
    "WF_19": {
      "seq": 19,
      "parent": "WF_17",
      "children": [
        "WF_20"
      ],
      "space": 1,
      "level": 11,
      "offset": 3,
      "relOffset": 3
    },
    "WF_17": {
      "seq": 17,
      "parent": "WF_16",
      "children": [
        "WF_19"
      ],
      "space": 1,
      "level": 10,
      "offset": 3,
      "relOffset": 3
    },
    "WF_18": {
      "seq": 18,
      "parent": "WF_16",
      "children": [],
      "space": 1,
      "level": 10,
      "offset": 4,
      "relOffset": 0
    },
    "WF_16": {
      "seq": 16,
      "parent": "WF_14",
      "children": [
        "WF_17",
        "WF_18"
      ],
      "space": 2,
      "level": 9,
      "offset": 3,
      "relOffset": 3
    },
    "WF_14": {
      "seq": 14,
      "parent": "WF_13",
      "children": [
        "WF_16"
      ],
      "space": 2,
      "level": 8,
      "offset": 3,
      "relOffset": 3
    },
    "WF_15": {
      "seq": 15,
      "parent": "WF_13",
      "children": [],
      "space": 1,
      "level": 8,
      "offset": 5,
      "relOffset": 1
    },
    "WF_13": {
      "seq": 13,
      "parent": "WF_10",
      "children": [
        "WF_14",
        "WF_15"
      ],
      "space": 3,
      "level": 7,
      "offset": 3,
      "relOffset": 3
    },
    "WF_10": {
      "seq": 10,
      "parent": "WF_8",
      "children": [
        "WF_13"
      ],
      "space": 3,
      "level": 6,
      "offset": 3,
      "relOffset": 3
    },
    "WF_11": {
      "seq": 11,
      "children": [],
      "space": 1,
      "level": 6,
      "offset": 4,
      "relOffset": 0
    },
    "WF_12": {
      "seq": 12,
      "children": [],
      "space": 1,
      "level": 6,
      "offset": 5,
      "relOffset": 0
    },
    "WF_5": {
      "seq": 5,
      "parent": "WF_4",
      "children": [],
      "space": 1,
      "level": 5,
      "offset": 0,
      "relOffset": 0
    },
    "WF_6": {
      "seq": 6,
      "parent": "WF_4",
      "children": [],
      "space": 1,
      "level": 5,
      "offset": 1,
      "relOffset": 0
    },
    "WF_7": {
      "seq": 7,
      "parent": "WF_4",
      "children": [],
      "space": 1,
      "level": 5,
      "offset": 2,
      "relOffset": 0
    },
    "WF_8": {
      "seq": 8,
      "parent": "WF_4",
      "children": [
        "WF_10"
      ],
      "space": 3,
      "level": 5,
      "offset": 3,
      "relOffset": 0
    },
    "WF_9": {
      "seq": 9,
      "children": [],
      "space": 1,
      "level": 5,
      "offset": 4,
      "relOffset": 0
    },
    "WF_4": {
      "seq": 4,
      "parent": "WF_3",
      "children": [
        "WF_5",
        "WF_6",
        "WF_7",
        "WF_8"
      ],
      "space": 6,
      "level": 4,
      "offset": 0,
      "relOffset": 0
    },
    "WF_3": {
      "seq": 3,
      "parent": "WF_2",
      "children": [
        "WF_4"
      ],
      "space": 6,
      "level": 3,
      "offset": 0,
      "relOffset": 0
    },
    "WF_2": {
      "seq": 2,
      "parent": "WF_1",
      "children": [
        "WF_3"
      ],
      "space": 6,
      "level": 2,
      "offset": 0,
      "relOffset": 0
    },
    "WF_1": {
      "seq": 1,
      "children": [
        "WF_2"
      ],
      "space": 6,
      "level": 1,
      "offset": 0,
      "relOffset": 0
    },
    "CANVAS_SPACE": 6,
    "CANVAS_LEVEL_SPACE": 12
  },
  "visualize": false
}


export default usecase_v1_3