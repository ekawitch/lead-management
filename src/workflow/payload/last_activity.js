const last_activity = {
  "nodes": [
    {
      "shape": "SQL",
      "title": "SQL",
      "description": "Last Activity",
      "image": "sql.png",
      "sql": "SELECT * FROM\n  (SELECT a11.*,row_number() OVER (PARTITION BY customer_key ORDER BY GREATEST(nvl(a11.last_payment_date,to_date('1900-01-01','yyyy-mm-dd')),nvl(a11.last_purchase_date,to_date('1900-01-01','yyyy-mm-dd'))) DESC) AS rn\n  from crmwork.meng_NLM_prep_1 a11) x11\nWHERE x11.rn = 1",
      "oracleNo": 1,
      "id": 1,
      "level": 1,
      "code": "WF_1",
      "type": "SQL"
    }
  ],
  "tree": {
    "1": {
      "children": [],
      "space": 1,
      "level": 1,
      "offset": 0,
      "relOffset": 0
    }
  },
  "visualize": false
}


export default last_activity