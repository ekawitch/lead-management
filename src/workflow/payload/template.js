const template = {
    "nodes": [
        {
            "shape": "Waterfall",
            "title": "Waterfall",
            "description": "KCS Misuse",
            "image": "waterfall.png",
            "datasource": "CRMWORK",
            "tables": [
                {
                    "seq": 1,
                    "name": "crmwork.misuse_allproduct",
                    "alias": "misuse",
                    "join": "",
                    "conditions": []
                }
            ],
            "fields": [
                {
                    "seq": 1,
                    "aggregation": true,
                    "function": "DISTINCT",
                    "name": "misuse.customer_key",
                    "alias": "customer_key"
                }
            ],
            "conditions": [
                {
                    "seq": 1,
                    "name": "",
                    "left": "misuse.entity",
                    "right": "'KCS'",
                    "operator": "EQ",
                    "connector": ""
                }
            ],
            "groupBy": [],
            "queryCmd": true,
            "oracleNo": 1,
            "isSqlVisualizer": true,
            "isQueryProperty": true,
            "tabs": [
                {
                    "value": "table",
                    "label": "Table"
                },
                {
                    "value": "field",
                    "label": "Field"
                },
                {
                    "value": "where",
                    "label": "Where"
                },
                {
                    "value": "group_by",
                    "label": "Group By"
                }
            ],
            "id": 1,
            "level": 1,
            "code": "KCSMisuse",
            "type": "Waterfall",
            "isTemplate": true
        },
        {
            "shape": "Waterfall",
            "title": "Waterfall",
            "description": "Bankrupt",
            "image": "waterfall.png",
            "datasource": "CRMWORK",
            "tables": [
                {
                    "seq": 1,
                    "name": "crmwork.view_bankruptcy",
                    "alias": "bankruptcy",
                    "join": "",
                    "conditions": []
                }
            ],
            "fields": [
                {
                    "seq": 1,
                    "aggregation": true,
                    "function": "DISTINCT",
                    "name": "bankruptcy.customer_key",
                    "alias": "customer_key"
                }
            ],
            "conditions": [],
            "groupBy": [],
            "queryCmd": true,
            "oracleNo": 1,
            "isSqlVisualizer": true,
            "isQueryProperty": true,
            "tabs": [
                {
                    "value": "table",
                    "label": "Table"
                },
                {
                    "value": "field",
                    "label": "Field"
                },
                {
                    "value": "where",
                    "label": "Where"
                },
                {
                    "value": "group_by",
                    "label": "Group By"
                }
            ],
            "id": 2,
            "level": 1,
            "code": "Bankrupt",
            "type": "Waterfall",
            "isTemplate": true
        },
        {
            "shape": "Waterfall",
            "title": "Waterfall",
            "description": "SFK 2M",
            "image": "waterfall.png",
            "datasource": "CRMWORK",
            "tables": [
                {
                    "seq": 1,
                    "name": "crm.campaign",
                    "alias": "x121",
                    "join": "",
                    "conditions": []
                },
                {
                    "seq": 2,
                    "name": "crm.contact_history",
                    "alias": "x122",
                    "join": "INNER",
                    "conditions": [
                        {
                            "seq": 1,
                            "connection": "",
                            "connector": "",
                            "operator": "EQ",
                            "left": "x121.campcode",
                            "right": "x122.campcode"
                        }
                    ]
                }
            ],
            "fields": [
                {
                    "seq": 1,
                    "aggregation": true,
                    "function": "DISTINCT",
                    "name": "x122.account_key",
                    "alias": "account_key"
                }
            ],
            "conditions": [
                {
                    "seq": 1,
                    "name": "",
                    "left": "x121.campcode",
                    "right": "'SFK%'",
                    "operator": "LIKE",
                    "connector": ""
                },
                {
                    "seq": 2,
                    "name": "",
                    "left": "x121.start_date",
                    "right": "GETDATE() - 30",
                    "operator": "LEQ",
                    "connector": "AND"
                },
                {
                    "seq": 3,
                    "name": "",
                    "left": "x121.start_date",
                    "right": "GETDATE() - 60",
                    "operator": "GEQ",
                    "connector": "AND"
                }
            ],
            "groupBy": [],
            "queryCmd": true,
            "oracleNo": 1,
            "isSqlVisualizer": true,
            "isQueryProperty": true,
            "tabs": [
                {
                    "value": "table",
                    "label": "Table"
                },
                {
                    "value": "field",
                    "label": "Field"
                },
                {
                    "value": "where",
                    "label": "Where"
                },
                {
                    "value": "group_by",
                    "label": "Group By"
                }
            ],
            "id": 3,
            "level": 1,
            "code": "SFK2M",
            "type": "Waterfall",
            "isTemplate": true
        }
    ],
    "tree": {
        "1": {
            "children": [],
            "space": 1,
            "level": 1,
            "offset": 0,
            "relOffset": 0
        },
        "2": {
            "children": [],
            "space": 1,
            "level": 1,
            "offset": 1,
            "relOffset": 0
        },
        "3": {
            "children": [],
            "space": 1,
            "level": 1,
            "offset": 2,
            "relOffset": 0
        }
    }
}

export default template