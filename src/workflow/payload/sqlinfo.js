const sqlInfo = {
    "name": null,
    "driverClassName": "com.microsoft.sqlserver.jdbc.SQLServerDriver",
    "url": "jdbc:sqlserver://LAPTOP-Q7CDI4BV:1433;DatabaseName=LMT;encrypt=true;trustServerCertificate=true;",
    "username": "sa",
    "password": "E@55ET",
    "tables": [{
        "catalog": "LMT",
        "schema": "crmwork",
        "name": "misuse_allproduct",
        "properties": [{
            "name": "customer_key",
            "type": "varchar2",
            "length": "50",
            "null": "NULL"
        }, {
            "name": "entity",
            "type": "varchar2",
            "length": "50",
            "null": "NULL"
        }
        ]
    }, {
        "catalog": "LMT",
        "schema": "crmwork",
        "name": "view_bankruptcy",
        "properties": [{
            "name": "customer_key"
        }
        ]
    }, {
        "catalog": "LMT",
        "schema": "crmwork",
        "name": "etm_registered_kcc_vip",
        "properties": [{
            "name": "account_key"
        }
        ]
    }, {
        "catalog": "LMT",
        "schema": "crmwork",
        "name": "meng_nlm_prep_1",
        "properties": [{
            "name": "last_payment_date",
            "type": "date",
            "null": "NULL"
        }, {
            "name": "last_purchase_date",
            "type": "date",
            "null": "NULL"
        }
        ]
    }, {
        "catalog": "LMT",
        "schema": "crmwork",
        "name": "meng_nlm_prep_2",
        "properties": [{
            "name": "gbi",
            "type": "varchar2",
            "length": "10",
            "null": "NULL"
        }, {
            "name": "risk_grade",
            "type": "number",
            "length": "10",
            "null": "NULL"
        }
        ]
    }, {
        "catalog": "LMT",
        "schema": "crm",
        "name": "account",
        "properties": [{
            "name": "account_key"
        }, {
            "name": "account_number"
        }, {
            "name": "account_opened_date",
            "type": "date",
            "null": "NULL"
        }, {
            "name": "account_status_key_0",
            "type": "varchar2",
            "length": "10",
            "null": "NULL"
        }, {
            "name": "b_score",
            "type": "number",
            "length": "10",
            "null": "NULL"
        }, {
            "name": "business_code",
            "type": "varchar2",
            "length": "10",
            "null": "NULL"
        }, {
            "name": "credit_limit_0",
            "type": "number",
            "length": "10",
            "null": "NULL"
        }, {
            "name": "customer_key"
        }, {
            "name": "last_payment_date",
            "type": "date",
            "null": "NULL"
        }, {
            "name": "last_purchase_date",
            "type": "date",
            "null": "NULL"
        }, {
            "name": "nationality",
            "type": "varchar2",
            "length": "50",
            "null": "NULL"
        }, {
            "name": "risk_grade",
            "type": "number",
            "length": "10",
            "null": "NULL"
        }
        ]
    }, {
        "catalog": "LMT",
        "schema": "crm",
        "name": "customer",
        "properties": [{
            "name": "customer_fixed_income",
            "type": "number",
            "length": "10",
            "null": "NULL"
        }, {
            "name": "customer_income",
            "type": "number",
            "length": "10",
            "null": "NULL"
        }, {
            "name": "customer_key"
        }, {
            "name": "gbi",
            "type": "varchar2",
            "length": "10",
            "null": "NULL"
        }, {
            "name": "nationality",
            "type": "varchar2",
            "length": "50",
            "null": "NULL"
        }
        ]
    }, {
        "catalog": "LMT",
        "schema": "crm",
        "name": "address_info",
        "properties": [{
            "name": "business_code",
            "type": "varchar2",
            "length": "10",
            "null": "NULL"
        }, {
            "name": "current_region",
            "type": "varchar2",
            "length": "100",
            "null": "NULL"
        }, {
            "name": "customer_key"
        }
        ]
    }, {
        "catalog": "LMT",
        "schema": "crm",
        "name": "campaign",
        "properties": [{
            "name": "campcode",
            "type": "varchar2",
            "length": "50",
            "null": "NULL"
        }, {
            "name": "start_date",
            "type": "date",
            "null": "NULL"
        }
        ]
    }, {
        "catalog": "LMT",
        "schema": "crm",
        "name": "contact_history",
        "properties": [{
            "name": "account_key"
        }, {
            "name": "campcode",
            "type": "varchar2",
            "length": "50",
            "null": "NULL"
        }
        ]
    }
    ]
        .map(tbl => {
            return { ...tbl, value: tbl.schema + "." + tbl.name, label: tbl.schema + "." + tbl.name }
        })
}

export {
    sqlInfo
}