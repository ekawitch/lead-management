const sample_waterfall_2 = {
  "nodes": [
    {
      "shape": "Waterfall",
      "title": "Waterfall",
      "description": "waterfall",
      "image": "waterfall.png",
      "datasource": "CRMWORK",
      "tables": [
        {
          "seq": 1,
          "name": "crm.account",
          "alias": "a11",
          "join": "",
          "conditions": []
        },
        {
          "seq": 2,
          "name": "crm.customer",
          "alias": "a12",
          "join": "INNER",
          "conditions": [
            {
              "seq": 1,
              "connection": "",
              "connector": "",
              "operator": "EQ",
              "left": "a11.customer_key",
              "right": "a12.customer_key"
            },
            {
              "seq": 2,
              "name": "",
              "left": "a12.gbi",
              "right": "('I')",
              "operator": "NIN",
              "connector": "AND"
            }
          ]
        },
        {
          "seq": 3,
          "name": "crmwork.etm_registered_kcc_vip",
          "alias": "a13",
          "join": "LEFT",
          "conditions": [
            {
              "seq": 1,
              "connection": "",
              "connector": "",
              "operator": "EQ",
              "left": "a11.account_key",
              "right": "a13.account_key"
            }
          ]
        },
        {
          "seq": 4,
          "name": "crm.address_info",
          "alias": "a14",
          "join": "INNER",
          "conditions": [
            {
              "seq": 1,
              "connection": "",
              "connector": "",
              "operator": "EQ",
              "left": "a11.customer_key",
              "right": "a14.customer_key"
            },
            {
              "seq": 2,
              "connection": "",
              "connector": "AND",
              "operator": "EQ",
              "left": "a11.business_code",
              "right": "a14.business_code"
            }
          ]
        }
      ],
      "fields": [
        {
          "seq": 1,
          "aggregation": false,
          "function": "",
          "name": "a11.account_key",
          "alias": "account_key"
        },
        {
          "seq": 2,
          "aggregation": false,
          "function": "",
          "name": "a11.account_number",
          "alias": "account_number"
        },
        {
          "seq": 3,
          "aggregation": false,
          "function": "",
          "name": "a11.customer_key",
          "alias": "customer_key"
        },
        {
          "seq": 4,
          "aggregation": false,
          "function": "",
          "name": "a11.business_code",
          "alias": "business_code"
        },
        {
          "seq": 5,
          "aggregation": false,
          "function": "",
          "name": "a11.account_status_key_0",
          "alias": "account_status_key_0"
        },
        {
          "seq": 6,
          "aggregation": false,
          "function": "",
          "name": "a11.account_opened_date",
          "alias": "account_opened_date"
        },
        {
          "seq": 7,
          "aggregation": false,
          "function": "",
          "name": "a11.credit_limit_0",
          "alias": "credit_limit_0"
        },
        {
          "seq": 8,
          "aggregation": false,
          "function": "",
          "name": "a11.nationality",
          "alias": "nationality"
        },
        {
          "seq": 9,
          "aggregation": false,
          "function": "",
          "name": "SUBSTRING(a12.nationality,1,2)",
          "alias": "substr_nationality"
        },
        {
          "seq": 10,
          "aggregation": false,
          "function": "",
          "name": "case when a12.customer_fixed_income >= a12.customer_income then 1 else 0 END",
          "alias": "income_flg"
        },
        {
          "seq": 11,
          "aggregation": false,
          "function": "",
          "name": "a12.customer_fixed_income",
          "alias": "customer_fixed_income"
        },
        {
          "seq": 12,
          "aggregation": false,
          "function": "",
          "name": "a12.customer_income",
          "alias": "customer_income"
        },
        {
          "seq": 13,
          "aggregation": false,
          "function": "",
          "name": "a11.risk_grade",
          "alias": "risk_grade"
        },
        {
          "seq": 14,
          "aggregation": false,
          "function": "",
          "name": "a12.gbi",
          "alias": "gbi"
        },
        {
          "seq": 15,
          "aggregation": false,
          "function": "",
          "name": "a11.last_payment_date",
          "alias": "last_payment_date"
        },
        {
          "seq": 16,
          "aggregation": false,
          "function": "",
          "name": "a11.last_purchase_date",
          "alias": "last_purchase_date"
        },
        {
          "seq": 17,
          "aggregation": false,
          "function": "",
          "name": "CASE WHEN a14.current_region = 'กรุงเทพ' THEN 'BKK' ELSE 'UPC' END",
          "alias": "region_eng"
        }
      ],
      "conditions": [
        {
          "seq": 1,
          "name": "",
          "left": "1",
          "right": "1",
          "operator": "EQ",
          "connector": ""
        },
        {
          "seq": 2,
          "name": "",
          "left": "a11.business_code",
          "right": "('KCCVS','KCCMC','HPVS')",
          "operator": "IN",
          "connector": "AND"
        },
        {
          "seq": 3,
          "name": "",
          "left": "a11.account_status_key_0",
          "right": "('101','111')",
          "operator": "IN",
          "connector": "AND"
        },
        {
          "seq": 4,
          "cmd": "AND a11.account_opened_date \nBETWEEN CONVERT(DATETIME, '2021-01-01', 102) \nAND CONVERT(DATETIME, '2021-03-31', 102)"
        },
        {
          "seq": 5,
          "cmd": "AND (a11.credit_limit_0 >= 40000\nOR (a11.b_score >= 800 AND a11.credit_limit_0 >= 30000))"
        },
        {
          "seq": 6,
          "cmd": "AND SUBSTRING(a12.nationality,1,2) LIKE 'T%'"
        },
        {
          "seq": 7,
          "cmd": "AND a13.account_key IS NULL"
        },
        {
          "seq": 8,
          "cmd": "AND case when a12.customer_fixed_income >= a12.customer_income then 1 else 0 END = 1"
        },
        {
          "seq": 9,
          "cmd": "AND a11.customer_key NOT IN ({$KCSMisuse})"
        },
        {
          "seq": 10,
          "cmd": "AND NOT EXISTS ({$Bankrupt})"
        },
        {
          "seq": 11,
          "cmd": "AND a11.account_key NOT IN ({$SFK2M})"
        }
      ],
      "groupBy": [],
      "queryCmd": true,
      "oracleNo": 1,
      "isSqlVisualizer": true,
      "isQueryProperty": true,
      "tabs": [
        {
          "value": "table",
          "label": "Table"
        },
        {
          "value": "field",
          "label": "Field"
        },
        {
          "value": "where",
          "label": "Where"
        },
        {
          "value": "group_by",
          "label": "Group By"
        }
      ],
      "id": 1,
      "level": 1,
      "code": "WF_1",
      "type": "Waterfall",
      "isTemplate": false
    }
  ],
  "visualize": false,
  "tree": {
    "1": {
      "children": [],
      "space": 1,
      "level": 1,
      "offset": 0,
      "relOffset": 0
    }
  }
}


export default sample_waterfall_2