const segmentation = {
  "nodes": [
    {
      "shape": "Waterfall",
      "title": "Waterfall",
      "description": "Segmentation",
      "image": "waterfall.png",
      "datasource": "CRMWORK",
      "tables": [
        {
          "seq": 1,
          "name": "crmwork.meng_nlm_prep_2",
          "alias": "a11",
          "join": "",
          "conditions": []
        }
      ],
      "fields": [
        {
          "seq": 1,
          "aggregation": false,
          "function": "",
          "name": "a11.gbi",
          "alias": "gbi"
        },
        {
          "seq": 2,
          "aggregation": false,
          "function": "",
          "name": "a11.risk_grade",
          "alias": "risk_grade"
        },
        {
          "seq": 3,
          "aggregation": false,
          "function": "",
          "name": "CASE WHEN a11.risk_grade = 10 THEN CASE WHEN a11.gbi = 'G' THEN 'group_a' WHEN a11.gbi = 'I' THEN 'group_b' ELSE 'group_c' END WHEN a11.risk_grade = 20 THEN 'group_d' END",
          "alias": "segment_lv1"
        }
      ],
      "conditions": [],
      "groupBy": [],
      "queryCmd": true,
      "oracleNo": 1,
      "isSqlVisualizer": true,
      "isQueryProperty": true,
      "tabs": [
        {
          "value": "table",
          "label": "Table"
        },
        {
          "value": "field",
          "label": "Field"
        },
        {
          "value": "where",
          "label": "Where"
        },
        {
          "value": "group_by",
          "label": "Group By"
        }
      ],
      "id": 1,
      "level": 1,
      "code": "WF_1",
      "type": "Waterfall",
      "isTemplate": false
    }
  ],
  "tree": {
    "1": {
      "children": [],
      "space": 1,
      "level": 1,
      "offset": 0,
      "relOffset": 0
    }
  },
  "visualize": false
}


export default segmentation