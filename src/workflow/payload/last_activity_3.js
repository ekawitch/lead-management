const last_activity_3 = {
  "nodes": [
    {
      "shape": "Waterfall",
      "title": "Waterfall",
      "description": "x11",
      "image": "waterfall.png",
      "datasource": "STAGING",
      "tables": [
        {
          "seq": 1,
          "name": "2",
          "alias": "x11",
          "join": "",
          "conditions": []
        }
      ],
      "fields": [
        {
          "seq": 1,
          "aggregation": false,
          "function": "",
          "name": "x11.last_payment_date",
          "alias": "last_payment_date"
        },
        {
          "seq": 2,
          "aggregation": false,
          "function": "",
          "name": "x11.last_purchase_date",
          "alias": "last_purchase_date"
        },
        {
          "seq": 3,
          "aggregation": false,
          "function": "",
          "name": "x11.rn",
          "alias": "rn"
        }
      ],
      "conditions": [
        {
          "seq": 1,
          "name": "",
          "left": "x11.rn",
          "right": "1",
          "operator": "EQ",
          "connector": ""
        }
      ],
      "groupBy": [],
      "queryCmd": true,
      "oracleNo": 1,
      "isSqlVisualizer": true,
      "isQueryProperty": true,
      "tabs": [
        {
          "value": "table",
          "label": "Table"
        },
        {
          "value": "field",
          "label": "Field"
        },
        {
          "value": "where",
          "label": "Where"
        },
        {
          "value": "group_by",
          "label": "Group By"
        }
      ],
      "id": 1,
      "level": 1,
      "code": "WF_1",
      "type": "Waterfall",
      "isTemplate": false
    },
    {
      "shape": "Waterfall",
      "title": "Waterfall",
      "description": "x11",
      "image": "waterfall.png",
      "datasource": "CRMWORK",
      "tables": [
        {
          "seq": 1,
          "name": "crmwork.meng_nlm_prep_1",
          "alias": "a11",
          "join": "",
          "conditions": []
        }
      ],
      "fields": [
        {
          "seq": 1,
          "aggregation": false,
          "function": "",
          "name": "a11.last_payment_date",
          "alias": "last_payment_date"
        },
        {
          "seq": 2,
          "aggregation": false,
          "function": "",
          "name": "a11.last_purchase_date",
          "alias": "last_purchase_date"
        },
        {
          "seq": 3,
          "aggregation": false,
          "function": "",
          "name": "row_number() OVER (PARTITION BY customer_key ORDER BY GREATEST(nvl(a11.last_payment_date,to_date('1900-01-01','yyyy-mm-dd')),nvl(a11.last_purchase_date,to_date('1900-01-01','yyyy-mm-dd'))) DESC)",
          "alias": "rn"
        }
      ],
      "conditions": [],
      "groupBy": [],
      "queryCmd": true,
      "oracleNo": 1,
      "isSqlVisualizer": true,
      "isQueryProperty": true,
      "tabs": [
        {
          "value": "table",
          "label": "Table"
        },
        {
          "value": "field",
          "label": "Field"
        },
        {
          "value": "where",
          "label": "Where"
        },
        {
          "value": "group_by",
          "label": "Group By"
        }
      ],
      "id": 2,
      "level": 1,
      "code": "WF_2",
      "type": "Waterfall",
      "isTemplate": false
    }
  ],
  "tree": {
    "1": {
      "children": [],
      "space": 1,
      "level": 1,
      "offset": 0,
      "relOffset": 0
    },
    "2": {
      "children": [],
      "space": 1,
      "level": 1,
      "offset": 1,
      "relOffset": 0
    }
  },
  "visualize": false
}


export default last_activity_3