const buildMapping = (acc, cur) => {
    acc[cur.value] = cur.label
    return acc
}

const WHERE_OPERATOR = [
    { "value": "EQ", "label": "=" },
    { "value": "LT", "label": "<" },
    { "value": "GT", "label": ">" },
    { "value": "NEQ", "label": "<>" },
    { "value": "LEQ", "label": "<=" },
    { "value": "GEQ", "label": ">=" },
    { "value": "LIKE", "label": "LIKE" },
    { "value": "NLIKE", "label": "NOT LIKE" },
    { "value": "BTW", "label": "BETWEEN" },
    { "value": "NBTW", "label": "NOT BETWEEN" },
    { "value": "IN", "label": "IN" },
    { "value": "NIN", "label": "NOT IN" }
]

const CONNECTOR_OPERATOR = [
    { "value": "AND", "label": "AND" },
    { "value": "OR", "label": "OR" }
]
const CONNECTOR_OPERATOR_REVERSE = {
    "AND": "OR",
    "OR": "AND"
}

const WHERE_OPERATOR_MAPPING = WHERE_OPERATOR.reduce(buildMapping, {})
const WHERE_OPERATOR_REVERSE = {
    "EQ": "NEQ",
    "LT": "GEQ",
    "GT": "LEQ",
    "NEQ": "EQ",
    "LEQ": "GT",
    "GEQ": "LT",
    "LIKE": "NLIKE",
    "BTW": "NBTW",
    "IN": "NIN",
    "NIN": "IN"
}
const CONNECTOR_OPERATOR_MAPPING = CONNECTOR_OPERATOR.reduce(buildMapping, {})

const AGGREGATION_OPERATOR = [
    { "value": "MIN", "label": "MIN" },
    { "value": "MAX", "label": "MAX" },
    { "value": "SUM", "label": "SUM" },
    { "value": "COUNT", "label": "COUNT" },
    { "value": "AVG", "label": "AVG" },
    { "value": "DATE", "label": "DATE" },
    { "value": "MONTH", "label": "MONTH" },
    { "value": "DISTINCT", "label": "DISTINCT" }
]

const JOIN_OPERATOR = [
    { "value": "INNER", "label": "JOIN" },
    { "value": "LEFT", "label": "LEFT JOIN" },
    { "value": "RIGHT", "label": "RIGHT JOIN" },
    { "value": "FULL", "label": "FULL JOIN" }
]
const JOIN_OPERATOR_MAPPING = JOIN_OPERATOR.reduce(buildMapping, {})

const NOTIFICATION_OPERATOR = [
    { "value": "EMAIL", "label": "Email" },
    { "value": "SMS", "label": "SMS" },
]

const ORDER_BY_OPERATOR = [
    { "value": "ASC", "label": "ASC" },
    { "value": "DESC", "label": "DESC" },
]
const ORDER_BY_OPERATOR_MAPPING = ORDER_BY_OPERATOR.reduce(buildMapping, {})

export {
    WHERE_OPERATOR,
    WHERE_OPERATOR_MAPPING,
    WHERE_OPERATOR_REVERSE,
    CONNECTOR_OPERATOR,
    CONNECTOR_OPERATOR_MAPPING,
    CONNECTOR_OPERATOR_REVERSE,
    AGGREGATION_OPERATOR,
    NOTIFICATION_OPERATOR,
    JOIN_OPERATOR,
    JOIN_OPERATOR_MAPPING,
    ORDER_BY_OPERATOR,
    ORDER_BY_OPERATOR_MAPPING
}