import React from 'react'
import { useState } from 'react'

const PlainTextPopup = ({ text, onChange }) => {
    const [state, setState] = useState({ isCopied: false })

    const copyToClipboard = (e) => {
        e.preventDefault()
        navigator.clipboard.writeText(text);
        setState({ isCopied: true })
    }

    if (state.isCopied) {
        setTimeout(() => {
            setState({ isCopied: false })
        }, 1000)
    }

    return (<>

        <div className="modal-backdrop fade show"></div>

        <div id="WFOutputModal" className="modal fade show d-block" tabIndex="-1" role="dialog">
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">
                            Workflow Output
                            {state.isCopied && <strong className="text-success ms-2 font-70pc">Copied</strong>}
                            {!state.isCopied && <a href="/#" className="text-secondary ms-2 font-70pc" onClick={(e) => copyToClipboard(e)}>Copy</a>}
                        </h5>
                        <button type="button" className="btn" data-dismiss="modal" aria-label="Close" data-event="hide-popup" onClick={(e) => onChange(e)}>
                            <i className="fa fa-close"></i>
                        </button>
                    </div>
                    <div className="modal-body">
                        <pre>{text}</pre>
                    </div>
                </div>
            </div>
        </div>
    </>)
}

export default PlainTextPopup