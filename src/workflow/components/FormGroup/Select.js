
export default function Select({
    name,
    value,
    label = undefined,
    labelClass = "col-form-label",
    controlDivClass = undefined,
    options = [],
    onChange,
}) {
    return <>
        <label htmlFor={"frmGrpInput" + name} className={labelClass}>{label}</label>
        <div className={controlDivClass}>
            <select className="form-control"
                data-name={name}
                value={value}
                onChange={(e) => onChange(e)}>
                <option value=""></option>
                {options && options.length > 0 && options.map(opt => <option key={opt.value}
                    value={opt.value}>{opt.label}
                </option>
                )}
            </select>
        </div>
    </>
}