export const INPUT_TYPE_TEXT = "text"
export const INPUT_TYPE_TEXTAREA = "textarea"
export default function InputText({
    name,
    value,
    label = undefined,
    getLabel = undefined,
    placeholder,
    onChange,
    type = INPUT_TYPE_TEXT
}) {
    const inputProps = {
        className: "form-control",
        id: "frmGrpInput" + name,
        name: name,
        value: value,
        placeholder: placeholder,
        onChange: (e) => {
            onChange(e)
        }
    }

    return <div className="form-group">
        {label && <label htmlFor={"frmGrpInput" + name}>{label}</label>}
        {getLabel && getLabel()}

        {INPUT_TYPE_TEXT === type && <input type="text" {...inputProps}></input>}
        {INPUT_TYPE_TEXTAREA === type && <textarea {...inputProps}></textarea>}
    </div>
}