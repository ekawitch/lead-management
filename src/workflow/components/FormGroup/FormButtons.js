import { useState } from "react"

const FORM_BUTTONS_INI = { edit: false }
export default function FormButtons() {
    const [frm, setFrm] = useState({ ...FORM_BUTTONS_INI })

    const onClick = (e) => {
        const currentTarget = e.currentTarget
        const id = currentTarget.id

        if (id === "edit" || id === "cancel") {
            setFrm({ ...frm, edit: id === "edit" })
        }
    }

    return <div className="row">
        <div className="col-12 d-flex justify-content-end">
            {frm.edit === false && <button id="edit"
                className="btn ms-1 btn-secondary"
                onClick={onClick}>Edit</button>}
            {frm.edit === true && <>
                <button className="btn ms-1 btn-success">Save</button>
                <button id="cancel"
                    className="btn ms-1 btn-danger"
                    onClick={onClick}>Cancel</button>
            </>}
        </div>
    </div>
}