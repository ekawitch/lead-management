export default function Textarea({
    name,
    value,
    label,
    placeholder,
    onChange
}) {
    return <div className="form-group">
        <label htmlFor={"frmGrpInput" + name}>{label}</label>
        <textarea type="text"
            className="form-control"
            id={"frmGrpInput" + name}
            name={name}
            value={value}
            placeholder={placeholder}
            onChange={(e) => { onChange(e) }}></textarea>
    </div>
}