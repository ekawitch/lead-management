import TreeNode from "./TreeNode";

export default function TreeView({
    values,
    parent = undefined,
    getTreeNode,
}) {
    const leafs = values ? values.filter(ele => ele.parent === parent) : []
    return <>
        {leafs.length > 0 && leafs.map(ele => <TreeNode key={[ele.parent, ele.order]}
            getTreeNode={getTreeNode}
            leaf={ele}
            values={values}></TreeNode>)}
    </>
} 