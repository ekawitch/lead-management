const toolbarConf = [
    {
        id: "zoom",
        Children: [
            { id: "zoom_out", icon: "fa-magnifying-glass-minus" },
            { id: "zoom_in", icon: "fa-magnifying-glass-plus" }
        ]
    },
    {
        id: "doc",
        Children: [
            { id: "save", icon: "fa-save" },
            { id: "delete", icon: "fa-trash" },
            { id: "undo", icon: "fa-undo" },
            { id: "redo", icon: "fa-redo" }
        ]
    },
    {
        id: "run",
        Children: [
            { id: "play", icon: "fa-play" },
            { id: "pause", icon: "fa-pause" },
            { id: "stop", icon: "fa-stop" }
        ]
    }
]

export default toolbarConf