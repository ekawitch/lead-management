import React from 'react';
import { preventDefault } from '../../service/EventService';
import toolbarConf from './ToolbarConf';

const Toolbar = ({
    toolState = {},
    setToolState,
    onToolBarChange,
}) => {

    const style = {};

    style.color = "currentcolor"
    style.textDecoration = "none"

    const onChange = (e) => {
        // e.preventDefault()
        preventDefault(e, "A")
        const currentTarget = e.currentTarget
        const dataset = currentTarget.dataset
        const event = dataset.event
        let tool = dataset.tool
        const id = dataset.id

        let newToolState = { ...toolState }
        tool = tool !== undefined ? tool : toolState.tool;

        if (event === "dropdown") {
            newToolState = { ...toolState, focus: "zoom" }
        } else if (event === "toolbar") {
            if (id === "zoom_in") {
                tool = (parseInt(tool.substring(0, tool.length - 1)) + 10) + "%";
                document.getElementById("canvas").style.width = tool
                newToolState.tool = parseInt(toolState.tool.substring(0, toolState.tool.length - 1)) + 10 + "%";
            } else if (id === "zoom_out") {
                tool = (parseInt(tool.substring(0, tool.length - 1)) - 10) + "%";
                document.getElementById("canvas").style.width = tool
                newToolState.tool = parseInt(toolState.tool.substring(0, toolState.tool.length - 1)) - 10 + "%";
            } else {
                onToolBarChange(e)
                return
            }
        }
        else {
            newToolState = { ...toolState, isShowOutput: false, tool, focus: "" }
            document.getElementById("canvas").style.width = tool
        }

        setToolState({ ...newToolState })
    }

    const isActive = (tool) => {
        let rslt = false

        // if (tool.id === "visualize") {
        //     rslt = focus.visualize
        // }
        // if (tool.id === "visualize-close") {
        //     rslt = !focus.visualize
        // }

        return rslt
    }

    const filterToolBar = (grp) => {

        return true
    }

    return (<>
        <div className="navbar navbar-expand-lg bg-gray p-0 border-bottom">
            <ul className='tool-ul'>
                <li>
                    <div className='tooldiv me-3'>
                        <a className="dropdown-toggle" style={style} href="/#" role="button" data-bs-toggle="dropdown" aria-expanded="false" data-event="dropdown" data-tool="100%" onClick={(e) => onChange(e)}>
                            {toolState.tool}
                        </a>
                        <ul className={"dropdown-menu" + (toolState.focus === "zoom" ? " show" : "")}>
                            <li><a className="dropdown-item" href="/#" data-tool="25%" onClick={(e) => onChange(e)}>25%</a></li>
                            <li><a className="dropdown-item" href="/#" data-tool="50%" onClick={(e) => onChange(e)}>50%</a></li>
                            <li><a className="dropdown-item" href="/#" data-tool="100%" onClick={(e) => onChange(e)}>100%</a></li>
                            <li><a className="dropdown-item" href="/#" data-tool="150%" onClick={(e) => onChange(e)}>150%</a></li>
                            <li><a className="dropdown-item" href="/#" data-tool="200%" onClick={(e) => onChange(e)}>200%</a></li>
                        </ul>
                    </div>
                    {toolbarConf.filter(icnGrp => filterToolBar(icnGrp)).map((icnGrp, icnGrpIdx) => {
                        return <div key={icnGrpIdx} className="icon-sep tooldiv px-2 border-left">
                            {icnGrp.Children.map(ele => <a key={ele.id}
                                data-event="toolbar"
                                data-id={ele.id}
                                href="/#"
                                className={"btn btn-icon" + (isActive(ele) ? " active" : "")}
                                onClick={(e) => onChange(e)}
                            >
                                <i className={"fa " + ele.icon}></i>
                            </a>
                            )}
                        </div>
                    })}
                </li>
            </ul>
        </div>
    </>)
}

const COMPONENT_WIDTH = 150
const COMPONENT_HEIGHT = 126
const COMPONENT_SPACE_X = 0
const COMPONENT_SPACE_Y = 0


export default Toolbar

export {
    COMPONENT_WIDTH,
    COMPONENT_HEIGHT,
    COMPONENT_SPACE_X,
    COMPONENT_SPACE_Y
}
