import onDeleteHandler from "./DeleteHandler"
import { onSaveHandler } from "./SaveHandler"
import { onUndoHandler } from "./UndoHandler"
import { onRunWorkflowHandler } from "./RunWorkflowHandler"

export function onToolbarChangeHandler({
    e,
    navState,
    workflow,
    focus,
    undo,
    toolState
}) {
    const id = e.currentTarget.dataset.id

    if (id === "save") {
        return onSaveHandler({ e, navState })
    } else if (id === "delete") {
        return onDeleteHandler({ e, workflow, focus })
    } else if (id === "undo" || id === "redo") {
        return onUndoHandler({ e, undo })
    } else if (id === "play" || id === "pause" || id === "stop") {
        return onRunWorkflowHandler({ e, workflow })
    } else {
        console.error("invalid event", e)
    }

    return {}
}