import { removeNode } from "../../../service/ObjectService"

const onDeleteHandler = ({
    wf,
    focus
}) => {
    return {
        nodes: removeNode(wf.nodes, [focus.code]),
        focus: { shape: undefined, code: undefined, action: undefined }
    }
}

export default onDeleteHandler