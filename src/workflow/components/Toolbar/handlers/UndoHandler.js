import { cloneObject } from "../../../service/ObjectService"

const UNDO_LIMIT = 10
const onUndoHandler = ({ e, undo }) => {
    const currentTarget = e.currentTarget
    const dataset = currentTarget.dataset
    const id = dataset.id
    const len = undo.nodes.length
    let step

    if (id === "undo") {
        if (undo.step < (len + 1)) {
            step = undo.step + 1
        }
    } else if (id === "redo") {
        if (undo.step > 0) {
            step = undo.step - 1
        }
    }

    if (step === undefined) {
        return
    }

    return {
        undo: { ...undo, step },
        nodes: undo.nodes[len - step - 1]
    }
}

const getUpdatedScore = (nodes, node) => {
    let rslt = [nodes.length]

    if (node) {
        if (Array.isArray(node.tables)) {
            rslt = [...rslt, node.tables.length]
            rslt = [...rslt, ...node.tables.map(ele => Array.isArray(ele.conditions) ? ele.conditions.length : 0)]
        }
        if (Array.isArray(node.fields)) {
            rslt = [...rslt, node.fields.length]
        }
        if (Array.isArray(node.conditions)) {
            rslt = [...rslt, node.conditions.length]
        }
        if (Array.isArray(node.sources)) {
            rslt = [...rslt, node.sources.length]
        }
        if (Array.isArray(node.segmentations)) {
            rslt = [...rslt, node.segmentations.length]
            rslt = [...rslt, ...node.segmentations.map(ele => Array.isArray(ele.conditions) ? ele.conditions.length : 0)]
        }
    }

    return rslt
}

const saveUndoState = (nodes, undo, node = undefined, preScore = undefined) => {
    const curScore = preScore ? getUpdatedScore(nodes, node) : undefined

    if (preScore === undefined ||
        preScore.length !== curScore.length ||
        !preScore.every((val, idx) => val === curScore[idx])) {
        undo.nodes.push(cloneObject(nodes))
        undo.step = 0

        if (undo.nodes.length > UNDO_LIMIT) {
            undo.nodes.shift()
        }

        return true
    }

    return false
}

export {
    onUndoHandler,
    getUpdatedScore,
    saveUndoState
}