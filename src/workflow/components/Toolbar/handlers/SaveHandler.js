const onSaveHandler = ({
    e,
    navState }) => {
    const currentTarget = e.currentTarget
    const dataset = currentTarget.dataset
    // const event = dataset.event
    const id = dataset.id
    let newNavState

    if (id === "save") {
        newNavState = { ...navState, isShowOutput: true, focus: undefined }
    }

    return { navState: newNavState }
}

export {
    onSaveHandler
}