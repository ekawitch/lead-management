import { cloneObject } from "../../../service/ObjectService"

let newNodes = []

const onRunWorkflowHandler = ({
    e,
    wf }) => {
    const currentTarget = e.currentTarget
    const dataset = currentTarget.dataset
    // const event = dataset.event
    const id = dataset.id

    if (id === 'play'){
        let tmpNodes = cloneObject(wf.nodes)
        let maxlevel = 0
        //เรียง Node ตามลำดับเลเวล
        for (const node of tmpNodes){
            if(node.level > maxlevel){
                maxlevel = node.level
                for(const newNode of tmpNodes){
                    let listNode = []
                    if(newNode.level === maxlevel){
                        listNode.push(newNode)                              
                    }
                    newNodes.push(listNode)
                }                   
            }
        } 
        if(newNodes.length !== 0){
            runWorkflow(newNodes,"play")
        }    
    } else if (id === 'pause'){
        runWorkflow(newNodes,"pause")
    } else if (id === 'stop'){
        runWorkflow(newNodes,"stop")
    }

    return
}

const runWorkflow = (e,type) =>{
    if(type === "play"){
        newNodes = connectDataBase(e)
    }else if(type === "pause"){
        newNodes = connectDataBase(e)
    }else if(type === "stop"){
        newNodes = []
    }
}

const connectDataBase = (e) =>{

}

export {
    onRunWorkflowHandler
}