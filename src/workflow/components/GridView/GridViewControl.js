import { useEffect, useState } from "react"
import { useContext } from "react"
import categoryContext from "../../context/CategoryContext"
import userContext from "../../context/UserContext"
import Select from "../FormGroup/Select"

const BUTTONS = [
    { action: "add", icon: "fa-plus" },
    { action: "edit", icon: "fa-pencil" },
    { action: "copy", icon: "fa-clone" },
    { action: "delete", icon: "fa-close" },
    { action: "assign", icon: "fa-folder-open" },
]
export default function GridViewControl() {
    const { userState } = useContext(userContext)
    const { categoryState } = useContext(categoryContext)
    const [users, setUsers] = useState([])
    const [categories, setCategories] = useState([])
    const onChange = (e) => {

    }

    useEffect(() => {
        if (userState.users) {
            setUsers([...userState.users.map(usr => {
                return { value: usr.id, label: usr.name }
            })])
        }
    },
        [userState, setCategories,])

    useEffect(() => {
        if (categoryState.categories) {
            setCategories([...categoryState.categories.map(cate => {
                return { value: cate.categoryId, label: cate.name }
            })])
        }
    },
        [categoryState, setCategories,])

    return <div className="row my-3">
        <div className="col-6">
            <div className="form-group row">

                <Select name="user"
                    label="User"
                    labelClass="col-sm-2 col-form-label text-end"
                    controlDivClass="col-sm-3"
                    options={users}
                    onChange={onChange}></Select>

                <Select name="category"
                    label="Category"
                    labelClass="col-sm-2 col-form-label text-end"
                    controlDivClass="col-sm-3"
                    options={categories}
                    onChange={onChange}></Select>

                <button type="button"
                    className="btn btn-light col-sm-1"
                    data-action="refresh"
                    onClick={(e) => onChange(e)}>
                    <i className="fa fa-refresh"></i>
                </button>
            </div>
        </div>
        <div className="col-6 d-flex justify-content-end">
            {BUTTONS.map(btn => <button key={btn.action}
                type="button"
                data-action={btn.action}
                className="btn btn-light"
                onClick={(e) => onChange(e)}>
                <i className={"fa " + btn.icon}></i>
            </button>)}
        </div>
    </div>
}