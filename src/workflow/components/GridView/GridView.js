import { Fragment, useState } from "react";
import { useEffect } from "react";
import { callGet } from "../../service/API/APIService";
import Pagination from "../Pagination";
import GridViewControl from "./GridViewControl";

export default function GridView({
    headers = [],
    dataRows = undefined,
    urlSearch = undefined,
    fieldID = "id",
    EditForm = undefined,
    tableClass = "table",
    ignoreGridViewControl = false,
    ignorePagination = false,
}) {
    const [results, setResults] = useState([])
    const [form, setForm] = useState({ id: "001" })
    const editForm = (tx) => <EditForm tx={tx}></EditForm>

    const onChange = (e) => {
        const currentTarget = e.currentTarget
        const dataset = currentTarget.dataset

        setForm({ id: dataset.id })
    }

    useEffect(() => {
        if (urlSearch) {
            const rslt = callGet(urlSearch)
            rslt.then(rslt => {
                setResults([...rslt.results])
            })
                .catch(err => console.log(err))
        } else if (dataRows) {
            setResults([...dataRows])
        }
    }, [urlSearch, dataRows])

    return <>
        {!ignoreGridViewControl && <GridViewControl></GridViewControl>}
        <table className={tableClass}>
            <thead>
                {headers && headers.length > 0 && <tr>
                    {headers.map(hdr => <th key={hdr.field}>{hdr.header}</th>)}
                </tr>}
            </thead>

            <tbody>
                {(!results || results.length === 0) && <tr>
                    <td colSpan={headers.length}>
                        <b>No data</b>
                    </td>
                </tr>}

                {(results && results.length > 0) && results.map(rslt => <Fragment key={rslt[fieldID]}>
                    <tr
                        data-id={rslt[fieldID]}
                        onClick={(e) => onChange(e)}>
                        {headers.map(hdr => <td key={hdr.field}>

                            {!hdr.getTD && rslt[hdr.field]}
                            {hdr.getTD && hdr.getTD(rslt, hdr.field, onChange)}

                        </td>)}
                    </tr>

                    {editForm && form.id && form.id === rslt[fieldID] && <tr>
                        <td colSpan={headers.length}>
                            {editForm(rslt)}
                        </td>
                    </tr>}
                </Fragment>)}
            </tbody>

        </table>

        {!ignorePagination && <Pagination></Pagination>}
    </>
}