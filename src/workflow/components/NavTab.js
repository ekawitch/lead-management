const NavTab = ({
    tabs,
    value,
    callback,
    navClass = "mt-5" }) => {
    const actived = value ? value : tabs[0].value
    const onClick = (e) => {
        callback({ value: e.currentTarget.dataset.tab })
    }

    return <nav className={navClass}>
        <div className="nav nav-tabs" id="nav-tab" role="tablist">
            {tabs.map(ele => <button key={ele.value} className={"nav-link " + (ele.value === actived ? " active" : "")}
                id="nav-home-tab"
                data-bs-toggle="tab"
                data-tab={ele.value}
                // data-bs-target="#nav-home"
                type="button"
                role="tab"
                // aria-controls="nav-home"
                aria-selected="true"
                onClick={(e) => onClick(e)}>{ele.label}</button>)}
            {/* <button className="nav-link"
                id="nav-profile-tab"
                data-bs-toggle="tab"
                data-bs-target="#nav-profile"
                type="button"
                role="tab"
                aria-controls="nav-profile"
                aria-selected="false">Profile</button> */}
        </div>
    </nav>
}

const isTabActived = (tab, mine) => {
    return tab === undefined || tab === mine
}

export default NavTab
export {
    isTabActived
}