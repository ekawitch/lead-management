const NavbarConf = [
    {
        id: "file", label: "File", children: [
            { id: "new", label: "New" },
            { id: "open_from", label: "Open from" },
            { id: "open_recent", label: "Open recent" },
            { id: "save", label: "Save", isDiv: true },
            { id: "save_as", label: "Save as" },
            { id: "share", label: "Share", isDiv: true },
            { id: "rename", label: "Rename", isDiv: true },
            { id: "make_a_copy", label: "Make a Copy" },
            { id: "import_from", label: "Import from", isDiv: true },
            { id: "export_as", label: "Export as" },
            { id: "embed", label: "Embed", isDiv: true },
            { id: "publish", label: "Publish" },
            { id: "new_library", label: "New Library", isDiv: true },
            { id: "open_library_from", label: "Open Library from" },
            { id: "properties", label: "Properties", isDiv: true },
            { id: "page_setup", label: "Page Setup", isDiv: true },
            { id: "print", label: "Print" },
            { id: "close", label: "Close", isDiv: true }
        ]
    },
    {
        id: "utilities", label: "Utilities", children: [
            { id: "import_data", label: "Import Data", url: "/importData" }
        ]
    },
    {
        id: "edit", label: "Edit", children: [
            { id: "template", label: "Template" },
            { id: "undo", label: "Undo" },
            { id: "redo", label: "Redo" },
            { id: "cut", label: "Cut", isDiv: true },
            { id: "copy", label: "Copy" },
            { id: "copy_as_image", label: "Copy as Image" },
            { id: "paste", label: "Paste" },
            { id: "delete", label: "Delete" },
            { id: "duplicate", label: "Duplicate", isDiv: true },
            { id: "find_replace", label: "Find/Replace", isDiv: true },
            { id: "edit_data", label: "Edit Data", isDiv: true },
            { id: "edit_tooltip", label: "Edit Tooltip" },
            { id: "edit_style", label: "Edit Style", isDiv: true },
            { id: "edit_geometry", label: "Edit Geometry" },
            { id: "edit", label: "Edit", isDiv: true },
            { id: "edit_link", label: "Edit Link", isDiv: true },
            { id: "open_link", label: "Open Link" },
            { id: "select_vertices", label: "Select Vertices", isDiv: true },
            { id: "select_edges", label: "Select Edges" },
            { id: "select_all", label: "Select All" },
            { id: "select_none", label: "Select None" },
            { id: "lock_unlock", label: "Lock/Unlock", isDiv: true }
        ]
    },
    {
        id: "view", label: "View", children: [
            { id: "format", label: "Format" },
            { id: "outline", label: "Outline" },
            { id: "layers", label: "Layers" },
            { id: "tags", label: "Tags" },
            { id: "search_shapes", label: "Search Shapes", isDiv: true },
            { id: "scratchpad", label: "Scratchpad" },
            { id: "shapes", label: "Shapes" },
            { id: "page_view", label: "Page View", isDiv: true },
            { id: "page_scale", label: "Page Scale" },
            { id: "units", label: "Units" },
            { id: "scrollbars", label: "Scrollbars", isDiv: true },
            { id: "tooltips", label: "Tooltips" },
            { id: "ruler", label: "Ruler" },
            { id: "grid", label: "Grid", isDiv: true },
            { id: "guildes", label: "Guildes" },
            { id: "shadow", label: "Shadow" },
            { id: "connection_arrows", label: "Connection Arrows", isDiv: true },
            { id: "connection_points", label: "Connection Points" },
            { id: "reset_view", label: "Reset View", isDiv: true },
            { id: "zoom_in", label: "Zoom In" },
            { id: "zoom_out", label: "Zoom Out" },
            { id: "fullscreen", label: "Fullscreen", isDiv: true }
        ]
    },
    {
        id: "arrange", label: "Arrange", children: [
            { id: "to_front", label: "To Front" },
            { id: "to_back", label: "To Back" },
            { id: "bring_forward", label: "Bring Forward" },
            { id: "send_backward", label: "Send Backward" },
            { id: "direction", label: "Direction", isDiv: true },
            { id: "rotation_shape_only_by_90_reverse", label: "Rotation shape only by 90 degree / Reverse" },
            { id: "align", label: "Align", isDiv: true },
            { id: "distribute", label: "Distribute" },
            { id: "navigation", label: "Navigation", isDiv: true },
            { id: "insert", label: "Insert" },
            { id: "layout", label: "Layout" },
            { id: "group", label: "Group", isDiv: true },
            { id: "ungroup", label: "Ungroup" },
            { id: "remove_from_group", label: "Remove from Group" },
            { id: "clear_waypoints", label: "Clear Waypoints", isDiv: true },
            { id: "autosize", label: "Autosize" }
        ]
    },
    {
        id: "extras", label: "Extras", children: [
            { id: "language", label: "Language" },
            { id: "theme", label: "Theme" },
            { id: "mathematical_typesetting", label: "Mathematical Typesetting", isDiv: true },
            { id: "copy_on_connect", label: "Copy on connect" },
            { id: "collapse_expand", label: "Collapse/Expand" },
            { id: "autosave", label: "Autosave", isDiv: true },
            { id: "plugins", label: "Plugins", isDiv: true },
            { id: "edit_diagram", label: "Edit Diagram", isDiv: true },
            { id: "show_start_screen", label: "Show Start Screen", isDiv: true },
            { id: "configuration", label: "Configuration" }
        ]
    },
    {
        id: "help", label: "Help", children: [
            { id: "template", event: "sample", label: "Template" },
            { id: "sample01", event: "sample", label: "Sample" },
            { id: "usecase_v1_3", event: "sample", label: "Usecase V1.3" },
            // { id: "sample_waterfall", event: "sample", label: "1.1 ScriptWaterfall" },
            // { id: "sample_waterfall_2", event: "sample", label: "1.2 ScriptWaterfall with Template" },
            // { id: "last_activity", event: "sample", label: "2.1 Max Last Activity" },
            // { id: "last_activity_2", event: "sample", label: "2.2 Max Last Activity with Template" },
            // { id: "last_activity_3", event: "sample", label: "2.3 Max Last Activity with Staging" },
            // { id: "segmentation", event: "sample", label: "3. Segmentation" },
            { id: "get-json", label: "Get JSON", isDiv: true },
            { id: "keyboard_shortcusts", label: "Keyboard Shortcusts" },
            { id: "quick_start_video", label: "Quick Start Video" },
            { id: "support", label: "Support" },
            { id: "about", label: "About 20.4.0", isDiv: true }
        ]
    }
]

export default NavbarConf