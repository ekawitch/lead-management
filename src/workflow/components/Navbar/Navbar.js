import React from 'react'
import NavbarConf from './NavbarConf'
import PlainTextPopup from '../PlainTextPopup'
import template from '../../payload/template'
import sample01 from '../../payload/sample01'
import sample_waterfall from '../../payload/sample_waterfall'
import sample_waterfall_2 from '../../payload/sample_waterfall_2'
import last_activity from '../../payload/last_activity'
import last_activity_2 from '../../payload/last_activity_2'
import last_activity_3 from '../../payload/last_activity_3'
import segmentation from '../../payload/segmentation'
import usecase_v1_3 from '../../payload/usecase_v1_3'

const sample = {
    template: template,
    sample01: sample01,
    sample_waterfall: sample_waterfall,
    sample_waterfall_2: sample_waterfall_2,
    last_activity: last_activity,
    last_activity_2: last_activity_2,
    last_activity_3: last_activity_3,
    segmentation: segmentation,
    usecase_v1_3: usecase_v1_3
}

const Navbar = ({ navState = {}, onNavbarChange, workflowState, getOutout }) => {

    return (<><nav className="navbar navbar-expand-lg bg-gray p-0 border-bottom">
        <div className="container-fluid">

            <div className="collapse navbar-collapse">
                <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                    {/* <li className="nav-item dropdown">
                        <a className="nav-link dropdown-toggle" href="/#" role="button" data-bs-toggle="dropdown" aria-expanded="false" data-event="dropdown" data-nav="help" onClick={(e) => onNavbarChange(e)}>
                            Help
                        </a>
                        <ul className={"dropdown-menu" + (navState.focus === "help" ? " show" : "")}>
                            <li><a className="dropdown-item" href="/#" data-nav="sample" onClick={(e) => onNavbarChange(e)}>Sample Data</a></li>
                            <li><a className="dropdown-item" href="/#" data-nav="get-json" onClick={(e) => onNavbarChange(e)}>Get WF JSON</a></li>
                        </ul>
                    </li> */}
                    {NavbarConf.map(nav => <li key={nav.id} className="nav-item dropdown">
                        <a className="nav-link dropdown-toggle" href={nav.url ? nav.url : "/#"} role="button" data-bs-toggle="dropdown" aria-expanded="false" data-event="dropdown" data-nav={nav.id} onClick={(e) => onNavbarChange(e)}>
                            {nav.label}
                        </a>

                        {nav.children !== undefined && <ul className={"dropdown-menu" + (navState.focus === nav.id ? " show" : "")}>
                            {nav.children.map(nav2 => <li key={nav2.id} className={nav2.isDiv ? "border-top" : ""}>
                                <a className="dropdown-item"
                                    href={nav2.url ? nav2.url : "/#"}
                                    data-nav={nav2.event ? nav2.event : nav2.id}
                                    data-id={nav2.id}
                                    onClick={(e) => {
                                        if (nav2.url === undefined) {
                                            onNavbarChange(e);
                                        }
                                    }}>
                                    {nav2.label}
                                </a>
                            </li>)}
                        </ul>}
                    </li>)}
                </ul>
            </div>
        </div>
    </nav>
        {navState.isShowOutput && <PlainTextPopup text={JSON.stringify(getOutout(workflowState), undefined, 2)} onChange={onNavbarChange}></PlainTextPopup>}
    </>)
}

const onNavbarChangeHandler = ({
    e,
    navState,
    renderSampleData
}) => {
    e.preventDefault()
    const currentTarget = e.currentTarget
    const dataset = currentTarget.dataset
    const event = dataset.event
    const nav = dataset.nav

    let newNavState
    let workflowState

    if (event === "dropdown") {
        newNavState = { ...navState, focus: navState.focus === nav ? undefined : nav }
    } else if (event === "hide-popup") {
        newNavState = { ...navState, isShowOutput: false }
    }

    if (nav === "new") {
        workflowState = { nodes: [] }
        newNavState = { ...navState, focus: undefined }
    } else if (nav === "sample") {
        renderSampleData(sample[dataset.id])
        newNavState = { ...navState, focus: undefined }
    } else if (nav === "get-json") {
        newNavState = { ...navState, isShowOutput: true, focus: undefined }
    } else if (nav === "template") {
        workflowState = { screen: "template" }
    }

    // return newNavState
    // if (newNavState) {
    //     setNavState({ ...newNavState })
    // }

    return {
        navState: newNavState,
        workflowState: workflowState
    }
}

export default Navbar
export {
    onNavbarChangeHandler
}