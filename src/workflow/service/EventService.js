const preventDefault = (e, tag = "A") => {
    if (e && e.currentTarget && e.currentTarget && e.currentTarget.tagName) {
        if (tag === e.currentTarget.tagName) {
            e.preventDefault()
        }
    }
}

export { preventDefault }