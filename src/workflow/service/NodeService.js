import { ComponentRegistry } from "../pages/Workflow/components/BlockComponent"
import { replaceAlias } from "./StringService"

export function expandChild(nodes) {
    return nodes.map(ele => {
        // ele.shape === "Segmentation"
        if (ele.segmentations) {
            return [ele, ...ele.segmentations.map((seg, idx) => {
                return {
                    ...ele,
                    code: seg.code,
                    internal: ele.code,
                    internalIdx: idx,
                    description: ele.description + " - " + seg.value
                }
            })]
        }

        return [ele]
    }).reduce((acc, cur) => {
        return [...acc, ...cur]
    }, [])
}

export function getUnfoldedFields(sqlInfo, nodes, node, tableAlias) {
    let rslt = []
    if (node) {

        if (Array.isArray(node.fields) && node.fields.length > 0) {
            rslt = node.fields.reduce((accFld, curfld) => {
                const fldArr = curfld.name.split(".")
                const fldNm = fldArr[fldArr.length - 1]
                const tbl = node.tables.filter(tblEle => tblEle.alias === fldArr[0])[0]

                if (fldNm === "*") {
                    if (tbl) {
                        if (node.datasource === "WORKFLOW") {
                            const parent = nodes.filter(ele => ele.code === tbl.name)[0]

                            accFld = [...accFld,
                            ...getUnfoldedFields(sqlInfo, nodes, parent, tableAlias)]
                        } else {
                            accFld = [...accFld,
                            ...sqlInfo.tables.filter(tblEle => tblEle.schema + "." + tblEle.name === tbl.name)[0].properties
                                .map(prop => {

                                    return {
                                        ...prop,
                                        alias: prop.name
                                    }
                                })]
                        }
                    }
                } else {
                    const curfldArr = curfld.name.split(".")
                    accFld = [...accFld,
                    {
                        ...curfld,
                        alias: curfld.alias ? curfld.alias : curfldArr[curfldArr.length - 1]
                    }
                    ]
                }

                return accFld
            }, [])
        } else {
            const parent = node.parent ? nodes.filter(ele => ele.code === node.parent) : []

            if (parent.length > 0) {
                rslt = getUnfoldedFields(sqlInfo, nodes, parent[0], tableAlias)
            }
        }
    }

    return rslt
}

export function getDefaultFields(sqlInfo, nodes, node) {
    let rslt = getUnfoldedFields(sqlInfo, nodes, node, node.code)
    // const fields = node.fields ? node.fields : []
    // const parent = node.parent ? nodes.filter(ele => ele.code === node.parent) : []

    // let rslt
    // if (fields.length === 0 && parent.length > 0) {
    //     rslt = getDefaultFields(sqlInfo, nodes, parent[0])
    // } else {
    //     rslt = fields.map(fld => {
    //         let alias = fld.alias
    //         let name = fld.name

    //         const nameSplit = name.split(".")
    //         if (!alias) {

    //             if (nameSplit.length > 0) {
    //                 alias = nameSplit[nameSplit.length - 1]
    //             }
    //         }

    //         if (nameSplit.length > 1 && node.tables) {
    //             const fldTblAlias = nameSplit.slice(0, -1).join(".")
    //             const tblRslt = node.tables.filter(ele => ele.alias === fldTblAlias || ele.name === fldTblAlias)

    //             if (tblRslt.length > 0) {
    //                 name = tblRslt[0].name + "." + nameSplit[nameSplit.length - 1]
    //             }
    //         }

    //         return {
    //             ...fld,
    //             name,
    //             alias
    //         }
    //     })
    // }

    if (node.shape === "Merge") {
        rslt = [rslt, ...node.sources.map(src => src.code)
            .map(src => {
                const srcNode = nodes.filter(nodeEle => nodeEle.code === src)[0]

                return getDefaultFields(sqlInfo, nodes, srcNode)
            })]

        rslt = rslt.reduce((acc, cur) => {
            for (let i = 0; i < cur.length; i++) {
                if (acc.filter(ele => ele.alias === cur[i].alias).length === 0) {
                    acc.push(cur[i])
                }
            }
            return acc
        }, [])
    }

    return rslt
}

export function getDefaultFieldType(node, nodes, idx, sqlInfo) {
    if (sqlInfo) {
        sqlInfo.tables.reduce((tblAcc, tblCur) => {
            tblAcc = tblCur.properties.reduce((fldAcc, fldCur) => {
                fldAcc[tblCur.name + "." + fldCur.name] = fldCur

                return fldAcc
            }, tblAcc)

            return tblAcc
        }, idx)
    }

    let parentArr = []

    if (node.parent) {
        parentArr = [node.parent]
    }
    if (node.sources) {
        parentArr = [...parentArr, ...node.sources.map(src => src.code)]
    }

    if (parentArr.length > 0) {
        for (let i = 0; i < parentArr.length; i++) {
            const parent = nodes.filter(ele => ele.code === parentArr[i])[0]

            getDefaultFieldType(parent, nodes, idx, sqlInfo)
        }
    }

    // const fields = node.fields
    const fields = getDefaultFields(sqlInfo, nodes, node)
    if (fields) {
        for (let i = 0; i < fields.length; i++) {
            const fld = fields[i]
            let alias = fld.alias

            if (fld.name) {
                const nameSplit = fld.name.split(".")
                const len = nameSplit.length

                if (len > 0) {
                    alias = nameSplit[len - 1]
                }

                if (len > 1) {
                    const propName = nameSplit[len - 1]
                    const tblName = fld.name.replace("." + propName, "")
                    let tbl

                    if (node.tables) {
                        const tblRslt = node.tables.filter(tblEle => tblEle.alias === tblName || tblEle.name === tblName)
                        if (tblRslt.length > 0) {
                            tbl = tblRslt[0].name
                        }
                    }

                    if (!tbl) {
                        tbl = node.parent
                    }

                    idx[node.code + "." + alias] = idx[tbl + "." + alias]
                }
            }
        }
    }
}

export function updateAlias(node, fromAlias, toAlias) {
    const fields = ["name", "left", "right", "cmd"]
    const arrFields = ["fields", "tables", "conditions", "groupBy", "orderBy"]

    for (let i = 0; i < fields.length; i++) {
        const field = fields[i]

        if (node[field]) {
            node[field] = replaceAlias(node[field], fromAlias, toAlias)
        }
    }

    for (let i = 0; i < arrFields.length; i++) {
        const arrField = arrFields[i]

        if (node[arrField] && Array.isArray(node[arrField])) {
            const arr = node[arrField]
            for (let j = 0; j < arr.length; j++) {
                updateAlias(arr[j], fromAlias, toAlias)
            }
        }
    }
}

export function getInputJson(jo) {
    jo.nodes = jo.nodes.map(ele => {
        ele.shape = ele.type
        return { ...ele, icon: ComponentRegistry[ele.shape] ? ComponentRegistry[ele.shape].icon : undefined }
    })

    return jo
}

export function getSQLFields(nodes, staging, sqlInfo, node = {}, curTable = undefined) {
    let options = []

    if (node.datasource !== "STAGING" && node.tables) {
        const idxTables = sqlInfo && sqlInfo.tables ? sqlInfo.tables.reduce((acc, cur) => {
            acc[cur.schema + "." + cur.name] = cur

            return acc
        }, {}) : {}

        options = node.tables.filter(frm => !curTable || curTable === frm.alias)
            .filter(frm => idxTables[frm.name]).map(frm => {
                const originalTbl = idxTables[frm.name]
                return {
                    ...frm,
                    ...originalTbl,
                    name: originalTbl.schema + "." + originalTbl.name,
                    alias: frm.alias
                }
            })
    }

    if (node.datasource === "STAGING" ||
        node.sources ||
        staging) {
        let idxNodes = nodes.reduce((acc, cur) => {
            acc[cur.code] = cur

            return acc
        }, {})

        if (staging) {
            options = staging.split(",").map((ele, idx) => {
                return { code: ele, seq: idx }
            })
        } else {
            options = []
            if (node.sources) {
                options = [...options, ...node.sources.map(src => {
                    return { code: src.code, seq: src.seq }
                })]
            }
            if (node.tables) {
                options = [...options, ...node.tables.map(tbl => {
                    return { code: tbl.name, seq: tbl.seq, alias: tbl.alias }
                })]
            }
        }

        options = options.filter(src => !curTable || curTable === src.code || curTable === src.alias).filter(src => idxNodes[src.code]).map(src => {
            let cur = idxNodes[src.code]
            let fields = getDefaultFields(sqlInfo, nodes, cur)

            return {
                alias: src.alias ? src.alias : cur.code,
                name: cur.code,
                description: cur.description,
                seq: src.seq,
                properties: fields.map(fld => { return { name: fld.alias } })
            }
        })
    }

    return options
}