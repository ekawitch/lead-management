const getNextSeq = (arr, attr) => {
    if (Array.isArray(arr)) {
        return arr.reduce((acc, cur) => acc > cur[attr] ? acc : parseInt("" + cur[attr]), 0) + 1
    }

    return 1
}

const autoCode = (node, arr, field, prefix, seq) => {
    node[field] = node[field] ? node[field] + "0" : prefix

    if (arr.filter(ele => ele[field] === node[field] + seq).length > 1) {
        return autoCode((node, arr, field, prefix, seq))
    }

    node[field] += seq
    return node
}

const cloneObject = (obj) => {
    return JSON.parse(JSON.stringify(obj))
}

const removeNode = (nodes, codes, isBegin = true) => {
    let rslt = nodes.filter(ele => codes.indexOf(ele.code) === -1)
    const children = nodes.filter(ele => codes.indexOf(ele.parent) !== -1).map(ele => ele.code)

    for (let codeStr in rslt) {
        const ele = rslt[codeStr]
        if (ele.sources) {
            ele.sources = ele.sources.filter(inp => codes.indexOf(inp.code) === -1)
        }
    }

    rslt = children.length > 0 ? removeNode(rslt, children, false) : rslt

    if (isBegin) {
        const maxLv = rslt.reduce((acc, cur) => cur.parent && cur.level > acc ? cur.level : acc, 1)
        rslt = rslt.map(ele => {
            return {
                ...ele,
                level: ele.level > maxLv ? maxLv : ele.level
            }
        })
    }

    return rslt
}

export {
    getNextSeq,
    autoCode,
    cloneObject,
    removeNode
}