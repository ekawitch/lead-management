import { Fragment } from "react"
import BlockLevel from "../pages/Workflow/components/BlockLevel"
import { Connector } from "../pages/Workflow/components/Connector"


export function calcTree(nodes, tree, lv = 1) {
    tree = tree ? tree : {}

    if (nodes && nodes.length > 0) {
        const validNodes = nodes.filter(ele => ele.level === lv)

        if (validNodes.length > 0) {

            tree = calcTree(nodes, tree, lv + 1)
            for (let nodeIdx in validNodes) {
                const node = validNodes[nodeIdx]
                const children = nodes.filter(ele => ele.parent === node.code).map(ele => ele.code)
                let space = 1

                if (children.length > 0) {
                    space = children.map(child => tree[child] ? tree[child].space : 1).reduce((acc, cur) => { return acc + cur }, 0)
                }

                tree[node.code] = {
                    seq: node.seq,
                    parent: node.parent,
                    children: children,
                    space: space,
                    level: lv,
                    internal: node.internal,
                    internalIdx: node.internalIdx
                }
            }
        }
    }

    return tree
}

export function calcTreeOffset(tree, lv = 1) {
    const nodes = Object.keys(tree)
        .filter(code => tree[code].level === lv)
        .map(code => tree[code])
        .sort((a, b) => {
            let diff = (tree[a.parent] ? tree[a.parent].seq : 999) - (tree[b.parent] ? tree[b.parent].seq : 999)

            return diff === 0 ? a.seq - b.seq : diff
        })
    let parent = undefined
    let offset = 0
    let relOffset = 0
    let preNode = undefined

    // if (lv === 2) {
    //     if (nodes.length === 4) {
    //         console.log(nodes)
    //     }
    // }
    if (nodes && nodes.length > 0) {
        for (let idxVal in nodes) {
            const idx = parseInt(idxVal)

            // if (nodes[idx].internal) {//TODO
            //     nodes[idx].offset = tree[nodes[idx].internal].offset + nodes[idx].internalIdx
            //     nodes[idx].relOffset = 0

            //     relOffset = 0
            // } else 
            if (!nodes[idx].parent) {//independent node
                // nodes[idx].offset = preNode ? preNode.offset + preNode.space : 0
                // nodes[idx].relOffset = preNode ? preNode.space - 1 : 0
                nodes[idx].offset = preNode ? preNode.offset + 1 : 0
                nodes[idx].relOffset = 0


                relOffset = nodes[idx].space - 1
            } else if (nodes[idx].parent !== parent) {
                parent = nodes[idx].parent

                if (parent) { //node under new parent
                    if (tree[parent] && tree[parent].offset) {
                        offset = tree[parent].offset
                    } else {
                        offset = 0
                        for (let i; i < nodes.length; i++) {
                            if (nodes[i].level === (lv - 1) && nodes[i].seq < tree[parent].seq) {
                                offset += tree[nodes[i].code].space
                            }
                        }
                    }

                    if (idx === 0) {
                        // nodes[idx].offset = 0
                        // nodes[idx].relOffset = 0
                        nodes[idx].offset = tree[nodes[idx].parent].offset
                        nodes[idx].relOffset = nodes[idx].offset
                    } else {
                        nodes[idx].offset = offset //nodes[idx - 1].offset + 1
                        nodes[idx].relOffset = (offset - (preNode ? preNode.offset + 1 : 0))
                    }
                    // console.log(["A", nodes[idx]])
                }
                // else { //independent node
                //     nodes[idx].offset = nodes[idx - 1].offset + 1
                //     nodes[idx].relOffset = 0
                //     console.log(["B", nodes[idx], nodes[idx - 1]])
                // }
                relOffset = nodes[idx].space - 1
            } else { //same parent
                nodes[idx].offset = offset
                nodes[idx].relOffset = relOffset
                relOffset = nodes[idx].space - 1
                // console.log(["C", nodes[idx]])
            }

            offset += nodes[idx].space
            preNode = nodes[idx]

            const myOffsetAndSpace = nodes[idx].offset + nodes[idx].space
            tree["CANVAS_SPACE"] = tree["CANVAS_SPACE"] && tree["CANVAS_SPACE"] > myOffsetAndSpace ? tree["CANVAS_SPACE"] : myOffsetAndSpace
            tree["CANVAS_LEVEL_SPACE"] = tree["CANVAS_LEVEL_SPACE"] && tree["CANVAS_LEVEL_SPACE"] > lv ? tree["CANVAS_LEVEL_SPACE"] : lv
        }

        calcTreeOffset(tree, lv + 1)
    }

    return tree
}

export function updateChildLevel(nodes, parent) {
    const children = nodes.filter(ele => ele.parent === parent.code)
    for (const idxStr in children) {
        const node = children[parseInt(idxStr)]
        node.level = parent.level + 1

        updateChildLevel(nodes, node)
    }
}

export function renderBlockLevel(workflowState, handleMouse, level = 1, maxSpace) {
    if (workflowState.nodes.length === 0) { return <></> }
    const validNodes = workflowState.nodes.filter(ele => workflowState.tree[ele.code] && workflowState.tree[ele.code].level === level)
        .sort((a, b) => {
            return (workflowState.tree[a.code] ? workflowState.tree[a.code].offset : 999) - (workflowState.tree[b.code] ? workflowState.tree[b.code].offset : 999)
        })

    return <>
        {
        renderBlockLevelComp(workflowState, handleMouse, level, [...validNodes], maxSpace)}

        {renderBlockLevelConn(workflowState, level, [...validNodes])}

        {validNodes.length > 0 && renderBlockLevel(workflowState, handleMouse, level + 1, maxSpace)}
    </>
}

export function renderBlockLevelComp(workflowState, handleMouse, level, validNodes, maxSpace) {
    return <>{
        validNodes.length > 0 && <BlockLevel
            nodes={workflowState.nodes}
            validNodes={validNodes}
            tree={workflowState.tree}
            level={level}
            maxSpace={maxSpace}
            handleMouse={handleMouse}></BlockLevel>
    }</>
}

export function renderBlockLevelConn(workflowState, level, nodes) {

    return level === 1 ? <></> : <>{nodes.filter(ele => ele.parent || ele.load).map(ele => {

        const from = { ...workflowState.tree[ele.parent], code: ele.parent }
        const to = { ...workflowState.tree[ele.code], code: ele.code }

        return <Fragment key={[ele.parent, ele.code]}>
            <Connector from={from} to={to}></Connector>

            {Array.isArray(ele.sources) && ele.sources.map(eleInp => {
                const fromInp = { ...workflowState.tree[eleInp.code], code: eleInp.code }
                const toInp = { ...workflowState.tree[ele.code], id: ele.code }

                return <Fragment key={[eleInp.code, ele.code]}>
                    <Connector from={fromInp} to={toInp}></Connector>
                </Fragment>
            })}
        </Fragment>
    })}</>
}