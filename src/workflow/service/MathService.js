const findRatio = (arr, len, sum, ratio) => {
    if (!len) {
        len = arr.length
        sum = arr.reduce((acc, cur) => acc + cur, 0)
        ratio = ratio ? ratio : arr.reduce((acc, cur) => [...acc, 0], [])
    }
    let curSum = ratio.reduce((acc, cur) => acc + cur, 0);
    let isDone = true;

    for (let i = 0; i < len; i++) {
        if (curSum === 0 || ((ratio[i] / curSum) < (arr[i] / sum))) {
            ratio[i] += 1;
            curSum++;
            isDone = false;
        }
    }

    if (!isDone) {
        ratio = findRatio(arr, len, sum, ratio);
    }

    return ratio;
}

const findRoundRobinVals = (ratio) => {
    const len = ratio.length
    const sum = ratio.reduce((acc, cur) => acc + cur, 0)
    let rslt = ratio.reduce((acc) => [...acc, []], [])

    const vals = Array(sum).fill(0).map((_, ival) => { return { val: ival, rand: Math.random() } })
        .sort((a, b) => a.rand - b.rand)
        .map(val => val.val)

    // let i = 1
    let iIdx = 0
    // while (rslt.reduce((acc, cur) => acc + cur.length, 0) < sum) {
    while (vals.length > 0) {
        if (rslt[iIdx].length < ratio[iIdx]) {
            // rslt[iIdx].push(i)
            // i++
            rslt[iIdx].push(vals.pop())
        }
        iIdx = (iIdx + 1) % len
    }

    return rslt
}

export {
    findRatio,
    findRoundRobinVals
}