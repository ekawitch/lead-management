import { CONNECTOR_OPERATOR_MAPPING, JOIN_OPERATOR_MAPPING, ORDER_BY_OPERATOR_MAPPING, WHERE_OPERATOR_MAPPING, WHERE_OPERATOR_REVERSE } from "../config/Operator"
import { getDefaultFields } from "./NodeService"

const getNodeQueryCmd = (sqlInfo, nodes, node) => {
    if (node.cmd !== undefined) {
        return node.cmd
    } else if (node.shape === "Merge") {
        return getNodeQueryCmdForMerge(sqlInfo, nodes, node)
    } else if (node.shape === "Output") {
        return getNodeQueryCmdForOutput(sqlInfo, nodes, node)
    } else if (node.shape === "TopPriority") {
        return getNodeQueryCmdForTopPriority(sqlInfo, nodes, node)
    }

    let cmd = "SELECT "

    if (node.fields && node.fields.length > 0) {
        cmd += node.fields.map(ele => {
            let fldCmd = ""

            if (ele.function) {
                fldCmd = ele.function + "(" + ele.name + ") " + ele.alias
            } else {
                const fldNmArr = ele.name.split(".")
                if ((fldNmArr.length > 0 &&
                    fldNmArr[fldNmArr.length - 1] === ele.alias) ||
                    !ele.alias) {
                    fldCmd = ele.name
                } else {
                    fldCmd = ele.name + " AS " + ele.alias
                }
            }

            return fldCmd
        }).join(", \n\t")
    } else {
        cmd += "* "
    }

    if (node.tables && Array.isArray(node.tables) && node.tables.length > 0) {
        cmd += "\n" + getFromCmd(sqlInfo, nodes, node)
    }

    if (node.isLegacyField ||
        node.isDefaultField ||
        (node.datasource && node.datasource === "WORKFLOW")) {
        // const expanded = expandChild(nodes)
        let srcArr = [{ code: node.parent }]

        if (node.sources) {
            srcArr = [...srcArr, ...node.sources]
        }

        cmd += "\nFROM " + srcArr.filter(ele => ele && ele.code).map((ele, eleIdx) => {
            let joinCmd = ""
            const joinNode = nodes.filter(nd => nd.code === ele.code)[0]

            joinCmd = eleIdx === 0 ? "" : node.shape + " "
            joinCmd += joinNode.code
            if (ele.conditions && ele.conditions.length > 0) {
                joinCmd += " ON" + ele.conditions.map(jCondi =>
                    jCondi.cmd ? jCondi.cmd :
                        jCondi.connector + " " +
                        jCondi.left + " " +
                        (WHERE_OPERATOR_MAPPING[jCondi.operator] ? WHERE_OPERATOR_MAPPING[jCondi.operator] : "") + " " +
                        jCondi.right).join("\n\t")
            }

            return joinCmd
        }).join("\n\t")
    }

    if (node.conditions && Array.isArray(node.conditions) && node.conditions.length > 0) {
        cmd += "\nWHERE " + getWhereCmdFromConditions(node.conditions)
    }

    if (node.orderBy && node.orderBy.length > 0) {
        cmd += "\nORDER BY " + getOrderByCmd(node.orderBy)
    }

    if (node.groupBy && node.groupBy.length > 0) {
        cmd += "\nGROUP BY " + node.groupBy.map(ele => ele.name).join(", ")
    }

    return cmd
}

const getFromCmd = (sqlInfo, nodes, node) => {
    return "FROM " + node.tables.map(ele => {
        let joinCmd = ""

        joinCmd += ele.join && JOIN_OPERATOR_MAPPING[ele.join] ? JOIN_OPERATOR_MAPPING[ele.join] + " " : ""

        let tableName = ele.name
        if (node.datasource === "STAGING") {
            const nodeTableRslt = nodes.filter(nodeEle => nodeEle.code === tableName)
            if (nodeTableRslt.length > 0) {
                const nodeTable = nodeTableRslt[0]

                if (nodeTable.isTemplate) {
                    tableName = "(\n" + getNodeQueryCmd(sqlInfo, nodes, nodeTable) + "\n)"
                } else {
                    tableName = nodeTable.code
                }
            }
        }
        joinCmd += tableName + " " + ele.alias
        if (ele.conditions && ele.conditions.length > 0) {
            joinCmd += " \n\t\tON" + ele.conditions.map(jCondi =>
                jCondi.cmd ? jCondi.cmd :
                    jCondi.connector + " " +
                    jCondi.left + " " +
                    (WHERE_OPERATOR_MAPPING[jCondi.operator] ? WHERE_OPERATOR_MAPPING[jCondi.operator] : "") + " " +
                    jCondi.right).join("\n\t")
        }

        return joinCmd
    }).join(" \n\t")
}
const getWhereCmdFromConditions = (arr) => {
    return arr.map(ele => {
        if (ele.cmd) {
            return ele.cmd
        }

        let rslt = ""
        if (ele.connector) {
            rslt += CONNECTOR_OPERATOR_MAPPING[ele.connector] ? CONNECTOR_OPERATOR_MAPPING[ele.connector] + " " : ""
        }
        if (ele.left) {
            rslt += ele.left + " "
        }
        if (ele.operator) {
            rslt += WHERE_OPERATOR_MAPPING[ele.operator] ? WHERE_OPERATOR_MAPPING[ele.operator] + " " : ""
        }
        if (ele.right) {
            rslt += ele.right
        }

        return rslt
    }).join("\n\t")
}

const getOrderByCmd = (arr) => {
    return arr.map(ele => ele.name + " " + (ORDER_BY_OPERATOR_MAPPING[ele.direction] ? ORDER_BY_OPERATOR_MAPPING[ele.direction] : ele.direction)).join(", ")
}

const getNodeQueryCmdForMerge = (sqlInfo, nodes, node) => {
    // node.sources
    // getDefaultFields
    const allSources = [node.parent, ...node.sources.map(src => src.code)]
    const idxSrcNode = allSources.reduce((acc, cur) => {
        const curNode = nodes.filter(ele => ele.code === cur)[0]

        acc[cur] = curNode
        return acc
    }, {})
    const idxSrcFields = allSources.reduce((acc, cur) => {
        acc[cur] = getDefaultFields(sqlInfo, nodes, idxSrcNode[cur]).map(fld => fld.alias)

        return acc
    }, {})
    const allFields = allSources.map(src => {
        return idxSrcFields[src]
    }).reduce((acc, cur) => {
        for (let i = 0; i < cur.length; i++) {
            if (acc.indexOf(cur[i]) === -1) {
                acc.push(cur[i])
            }
        }

        return acc
    }, [])

    return allSources.map(src => {
        const srcSelect = allFields.map(fld => idxSrcFields[src].indexOf(fld) === -1 ? "NULL AS " + fld : fld).join(", ")

        return "SELECT " + srcSelect + "\nFROM " + src
    }).join("\nUNION ALL\n")
}

const getNodeQueryCmdForOutput = (sqlInfo, nodes, node) => {
    const parent = nodes.filter(ele => ele.code === node.parent)[0]
    const allFields = getDefaultFields(sqlInfo, nodes, parent).map(fld => fld.alias)
    const srcSelect = allFields.join(",\n\t")

    let rslt = "SELECT " + srcSelect + "\nFROM " + parent.code + "\nWHERE\n\n"

    if (parent.shape === "Segmentation") {
        rslt = getNodeQueryCmdForOutputFromSegmentation(sqlInfo, nodes, node)
    } else if (parent.shape === "Exclude") {
        rslt = getNodeQueryCmdForOutputFromExclude(sqlInfo, nodes, node)
    }

    return rslt
}

const getNodeQueryCmdForOutputFromSegmentation = (sqlInfo, nodes, node) => {
    const parent = nodes.filter(ele => ele.code === node.parent)[0]
    const grandParent = nodes.filter(ele => ele.code === parent.parent)[0]
    // const allFields = getDefaultFields(sqlInfo, nodes, grandParent).map(fld => fld.alias)
    const grandParentQueryCmd = getNodeQueryCmd(sqlInfo, nodes, grandParent)
    // let srcSelect = allFields.join(",\n\t")

    // let rslt = "SELECT " + srcSelect + "\nFROM (\n" + grandParentQueryCmd + "\n) " + grandParent.code
    let rslt = "SELECT *\nFROM (\n" + grandParentQueryCmd + "\n) " + grandParent.code

    if (parent.segmentType === "CONDITION") {
        const parentSeg = parent.segmentations.filter(ele => ele.seq === node.parentSegment)

        if (parentSeg.length > 0) {
            rslt += "\nWHERE " + getWhereCmdFromConditions(parentSeg[0].conditions)
        }
    } else if (parent.segmentType === "ROUNDROBIN") {
        let srcSelect = "SELECT rownum AS round_robin,\n\t" + grandParent.code + ".*\nFROM " + grandParent.code
        srcSelect += "\nORDER BY " + getOrderByCmd(parent.orderBy)

        rslt = "SELECT * \nFROM (\n" + srcSelect + "\n)"
        let parentSeg = parent.segmentations.filter(ele => ele.seq === node.parentSegment)
        if (parentSeg.length > 0) {
            parentSeg = parentSeg[0]
            // const rrPct = parent.segmentations.reduce((acc, cur) => [...acc, parseInt("" + cur.capacity)], [])
            // const ratio = findRatio(rrPct)
            // const rrVals = findRoundRobinValsByIdx(ratio, parentSeg[0].seq - 1)

            rslt += "\nWHERE " + parentSeg.conditions.map(cond => cond.left + " " + cond.operator + " " + cond.right).join("")
        }
    }

    return rslt
}

const getNodeQueryCmdForOutputFromExclude = (sqlInfo, nodes, node) => {
    const parent = nodes.filter(ele => ele.code === node.parent)[0]
    const grandParent = nodes.filter(ele => ele.code === parent.parent)[0]
    // const allFields = getDefaultFields(sqlInfo, nodes, grandParent).map(fld => fld.alias)
    // const srcSelect = allFields.join(",\n\t")

    // let rslt = "SELECT " + srcSelect + "\nFROM " + grandParent.code
    let rslt = "SELECT " + grandParent.code + ".*\nFROM " + grandParent.code

    if (parent.shape === "Exclude") {
        const whereConn = node.parentSegment === 1 ? "AND " : "OR "
        rslt += "\nWHERE "
        rslt += parent.sources.map(src => {

            return src.conditions.map(cnd => {
                let left
                let right

                if (cnd.left.indexOf(node.parent) > -1) {
                    left = cnd.left
                    right = cnd.right
                } else {
                    left = cnd.left
                    right = cnd.right
                }

                if (right && right.indexOf(cnd.code)) {
                    const rightNode = nodes.filter(ele => ele.code === src.code)[0]

                    right = getNodeQueryCmd(sqlInfo, nodes, rightNode)
                }

                let whereOpt = WHERE_OPERATOR_MAPPING[cnd.operator]

                if (node.parentSegment === 2) {
                    if (cnd.operator && WHERE_OPERATOR_REVERSE[cnd.operator]) {
                        whereOpt = WHERE_OPERATOR_MAPPING[WHERE_OPERATOR_REVERSE[cnd.operator]]
                    }
                }

                return left + " " + whereOpt + " (\n" + right + "\n)"
            }).join("")
        }).join("\n\n" + whereConn)
    }

    return rslt
}

const getNodeQueryCmdForTopPriority = (sqlInfo, nodes, node) => {
    // const parent = nodes.filter(ele => ele.code === node.parent)[0]
    // const allFields = getDefaultFields(sqlInfo, nodes, parent).map(fld => fld.alias)

    // let srcSelect = allFields.join(",\n\t")
    let srcSelect
    if (node.fields.length === 0) {
        srcSelect = "*"

        if (node.tables.length === 1) {
            const tbl = node.tables[0]
            srcSelect = (tbl.alias ? tbl.alias : tbl.name) + ".*"
        }
    } else {
        srcSelect = node.fields.map(fld => fld.name + (fld.alias ? " AS " + fld.alias : "")).join("\n\t")
    }
    srcSelect += ",\n\tROW_NUMBER() OVER (PARTITION BY " + node.partitionBy.map(ele => ele.name).join(", ") +
        " ORDER BY " + getOrderByCmd(node.orderBy) + ") AS rn"

    // let rslt = "SELECT " + srcSelect + "\nFROM " + parent.code
    let rslt = "SELECT " + srcSelect + "\n" + getFromCmd(sqlInfo, nodes, node)

    if (node.conditions && Array.isArray(node.conditions) && node.conditions.length > 0) {
        rslt += "\nWHERE " + getWhereCmdFromConditions(node.conditions)
    }

    // return "SELECT " + [...allFields, "rn"].join(",\n\t") + "\nFROM (" + rslt + ") WHERE rn = 1"
    return "SELECT top_priority.* \nFROM (" + rslt + ") top_priority WHERE top_priority.rn = 1"
}

const getCreateNodeOracleStagingCmd = (nodes, sqlInfo, node, idxAllFields) => {
    let cmd = []
    cmd.push("DROP TABLE " + node.code + ";")
    cmd.push("CREATE TABLE " + node.code + " AS ")
    cmd.push(getNodeQueryCmd(sqlInfo, nodes, node))
    // let fields = getDefaultFields(sqlInfo, nodes, node)

    // cmd = [...cmd, ...fields.map((fld, fldIdx) => {
    //     const fldName = fld.name
    //     const info = idxAllFields[fldName] ? idxAllFields[fldName] : undefined
    //     const fldType = info && info.type ? info.type : "varchar2"
    //     const fldNull = info && info.null ? info.null : "NULL"
    //     let fldLen = " "

    //     if (info && info.type) {
    //         if (["date"].indexOf(info.type.toLowerCase()) !== -1) {
    //             fldLen = " "
    //         } else {
    //             fldLen = "("
    //             fldLen += info && info.length ? info.length : 100
    //             fldLen += ") "
    //         }
    //     } else {
    //         fldLen = "(100) "
    //     }

    //     return " " + fld.alias + " " + fldType + fldLen + (fldNull && fldNull.toLowerCase().trim() === "not null" ? "NOT NULL" : "") + "" + (fldIdx < (fields.length - 1) ? "," : "")
    // })]

    // cmd.push(");")

    return cmd.join("\n")
}

export {
    getNodeQueryCmd,
    getFromCmd,
    getWhereCmdFromConditions,
    getOrderByCmd,
    getCreateNodeOracleStagingCmd
}