const padLeftStr = (val, len, pad = "0") => {
    let rslt = "" + val
    while (rslt.length < len) {
        rslt = pad + rslt
    }

    return rslt
}

const replaceAlias = (val, fromAlias, toAlias) => {
    return val ? val.replaceAll(fromAlias + ".", toAlias + ".") : val
}

export {
    padLeftStr,
    replaceAlias
}