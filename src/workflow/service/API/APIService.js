export async function callGet(url) {
    const resp = await fetch(url)
    return resp.json()
}