import { URL_API_CATEGORIES } from "./APIConfig";
import { callGet } from "./APIService";

export const getCategories = async () => {
    return await callGet(URL_API_CATEGORIES)
}