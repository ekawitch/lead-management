export function getDataSources() {
    return Promise.resolve([
        { "value": "CRM", "label": "CRM" },
        { "value": "CRMWORK", "label": "CRMWORK" },
        { "value": "CSV", "label": "CSV" },
    ])
}