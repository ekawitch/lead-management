import React from 'react'
import { useState } from 'react'
// import { useEffect } from 'react'
import NavTab, { isTabActived } from '../../../components/NavTab'
import { TabRegistry } from '../components/BlockComponent'
import NotificationProperty from './NotificationProperty'
import SegmentationProperty from './SegmentationProperty/SegmentationProperty'
import SourceProperty from './SourceProperty'

const StagingProperty = ({
    tree,
    onChange,
    node,
}) => {
    const tabs = TabRegistry[node.shape] ? TabRegistry[node.shape] : []
    const [tabstate, settabstate] = useState({ value: tabs ? tabs[0].value : undefined })

    return <>
        {tabs && tabs.length > 1 && <NavTab
            tabs={tabs}
            value={tabstate.value}
            callback={settabstate}>
        </NavTab>}

        {isTabActived(tabstate.value, "segmentation") &&
            node.segmentations && <SegmentationProperty
                node={node}
                onChange={onChange}>
            </SegmentationProperty>}
        {isTabActived(tabstate.value, "source") &&
            node.sources && <SourceProperty
                tree={tree}
                node={node}
                onChange={onChange}>
            </SourceProperty>}

        {isTabActived(tabstate.value, "notification") &&
            Array.isArray(node.notifications) && <NotificationProperty node={node} onChange={onChange}></NotificationProperty>}
    </>
}

export default StagingProperty