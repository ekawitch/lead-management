import { Fragment } from "react"
import QueryWhere from "../Query/QueryWhere"

const SegmentDetail = ({
    node,
    onChange
}) => {
    return <>
        {Array.isArray(node.segmentations) && node.segmentations.map((ele) => <Fragment key={ele.seq}>
            <div className="row mt-1">
                {ele.capacity === undefined && <div className="col-10">
                    <input className="form-control"
                        data-event="node-segmentation"
                        name="value"
                        data-seq={ele.seq}
                        value={ele.value}
                        onChange={(e) => onChange({ e })}
                        readOnly={node.isSegmentationReadOnly}>
                    </input>
                </div>}

                {ele.capacity !== undefined && <>
                    <div className="col-8">
                        <input className="form-control"
                            data-event="node-segmentation"
                            name="value"
                            data-seq={ele.seq}
                            value={ele.value}
                            onChange={(e) => onChange({ e })}
                            readOnly={node.isSegmentationReadOnly}>
                        </input>
                    </div>
                    <div className="col-2">
                        <input className="form-control"
                            data-event="node-segmentation"
                            name="capacity"
                            data-seq={ele.seq}
                            value={ele.capacity}
                            onChange={(e) => onChange({ e })}>
                        </input>
                    </div>
                </>}

                <div className="col-2">
                    {!node.isSegmentationReadOnly && <a href="/#"
                        className="text-danger font-70pc"
                        data-event="node-segmentation"
                        data-seq={ele.seq}
                        name="remove"
                        onClick={(e) => onChange({ e })}>remove</a>}
                </div>
            </div>

            {ele.conditions !== undefined && <QueryWhere
                node={node}
                element={ele}
                onChange={onChange}
                segmentationSeq={ele.seq}
                event="segmentation-condition"
                header="Condition"
                headerClass="header3 mb-0"
                type="segmentation-condition"
                leftStaging={node.parent}
                rightStaging={node.parent}>
            </QueryWhere>}
        </Fragment>)}
    </>
}

export default SegmentDetail