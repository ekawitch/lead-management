import React, { useContext, useState } from 'react'
import NavTab from '../../../../components/NavTab'
import { preventDefault } from '../../../../service/EventService'
import { findRatio, findRoundRobinVals } from '../../../../service/MathService'
import { autoCode, getNextSeq } from '../../../../service/ObjectService'
import { padLeftStr } from '../../../../service/StringService'
import QueryOrderBy from '../Query/QueryOrderBy'
import { onWhereChange } from '../Query/QueryWhere'
import SegmentDetail from './SegmentDetail'
import workflowContext from '../../../../context/WorkflowContext'

const SegmentTypes = [
    { value: "ROUNDROBIN", label: "Round Robin" },
    { value: "PERCENT", label: "Random Percent" },
    { value: "AMOUNT", label: "Random Amount" },
    { value: "CONDITION", label: "Condition" }
]
export default function SegmentationProperty({
    node,
    onChange,
}) {
    const { workflowState } = useContext(workflowContext)
    const [state, setState] = useState({ tab: "segment" })
    const isPendingSeg = getPendingSegment(workflowState.nodes, node).length > 0
    const tabs = node.segmentType === "ROUNDROBIN" ? [
        { value: "segment", label: "Segment" },
        { value: "orderby", label: "Order By" }
    ] : undefined

    const onStageChange = (e) => {
        preventDefault(e, "A")
        const curState = { ...state, tab: e.value }

        setState({ ...curState })
    }

    return (<>
        {/* {node.segmentationPropertyHeader && <h3 className="header5 mt-5">{node.segmentationPropertyHeader}</h3>} */}

        {node.hasOwnProperty("segmentType") && <div className="form-group row mt-1">
            <label className="col-sm-4 col-form-label">Segment Type</label>
            <div className="col-sm-8">
                <select className="form-control"
                    data-event="node-segmentation"
                    name="segmentType"
                    value={node.segmentType}
                    onChange={(e) => onChange({ e })}
                    readOnly={node.segmentations.length > 0}>
                    {!node.segmentType && <option></option>}
                    {SegmentTypes.filter(ele => node.segmentations.length === 0 || node.segmentType === ele.value)
                        .map(ele => <option key={ele.value} value={ele.value}>{ele.label}</option>)}
                </select>
            </div>
        </div>}


        {!node.isIgnoreSegmentationPrefix && <div className="form-group row mt-1">
            <label className="col-sm-4 col-form-label">Segment Code Prefix</label>
            <div className="col-sm-8">
                <input type="text"
                    className={"form-control" + (node.segmentPrefix ? "" : " is-invalid")}
                    data-event="node-property"
                    name="segmentPrefix"
                    value={node.segmentPrefix}
                    onChange={(e) => onChange({ e })}
                    readOnly={node.segmentations.length > 0}></input>
            </div>
        </div>}


        {node.segmentPrefix && <div className="row">
            <div className="col-12 d-flex justify-content-end my-3">
                {isPendingSeg && <>
                    <a href="/#"
                        className="text-success font-70pc"
                        data-event="attach-outputs"
                        name="new"
                        onClick={(e) => onChange({ e })}>attach outputs</a>

                    <span className="font-70pc mx-2">/</span>
                </>}
                <a href="/#"
                    className="text-success font-70pc"
                    data-event="node-segmentation"
                    name="new"
                    onClick={(e) => onChange({ e })}>new segment</a>
            </div>
        </div>}

        {!node.segmentPrefix && isPendingSeg && <div className="row">
            <div className="col-12 d-flex justify-content-end my-3">
                <a href="/#"
                    className="text-success font-70pc"
                    data-event="attach-outputs"
                    name="new"
                    onClick={(e) => onChange({ e })}>attach outputs</a>
            </div>
        </div>}

        {tabs !== undefined && <NavTab
            tabs={tabs}
            value={state.tab}
            callback={onStageChange}
            navClass="mb-3 mt-3">
        </NavTab>}

        {state.tab === "segment" && <SegmentDetail
            node={node}
            onChange={onChange}
        ></SegmentDetail>}

        {state.tab === "orderby" && <QueryOrderBy
            node={node}
            onChange={onChange}
            staging={node.parent}
        ></QueryOrderBy>}
    </>)
}

export function onSegmentationPropertyChange(e, node, nodes) {
    const currentTarget = e.currentTarget
    const dataset = currentTarget.dataset
    const name = currentTarget.name
    const seq = dataset.seq ? parseInt(dataset.seq) : undefined
    const isRoundRobin = node.segmentType === "ROUNDROBIN"

    if (dataset.event === "segmentation-condition") {
        const seg = node.segmentations.filter(ele => ele.seq === parseInt(dataset.segmentationSeq))[0]
        onWhereChange(seg, currentTarget)
    } else if (dataset.event === "attach-outputs") {
        const pendingSegs = getPendingSegment(nodes, node)

        for (let i = 0; i < pendingSegs.length; i++) {
            const segEle = pendingSegs[i]
            const nextSeq = getNextSeq(nodes, "seq")
            let newNode = {
                "parentSegment": segEle.seq,
                "shape": "Output",
                "title": "Output",
                "description": segEle.value,
                "image": "output.png",
                "isAutoField": true,
                "oracleNo": 1,
                "queryCmd": true,
                "seq": nextSeq,
                "parent": node.code,
                "level": node.level + 1,
                "type": "Output"
            }

            newNode = autoCode(newNode, nodes, "code", "WF_", nextSeq)
            newNode.staging = newNode.code
            nodes.push(newNode)
        }
    } else {
        if (name === "new") {
            node.segmentations = node.segmentations ? node.segmentations : []
            const nextSeq = getNextSeq(node.segmentations, "seq")
            const segLen = node.segmentations.length
            let codeNum = segLen > 0 ? node.segmentations[segLen - 1].value.replace(node.segmentPrefix, "") : "0"

            const newSeg = {
                seq: nextSeq,
                value: node.segmentPrefix + padLeftStr(isNaN(codeNum) ? 1 : parseInt(codeNum) + 1, 2),
                conditions: node.segmentType === "CONDITION" ? [] : undefined,
                capacity: node.segmentType === "ROUNDROBIN" ||
                    node.segmentType === "PERCENT" ||
                    node.segmentType === "AMOUNT" ? 0 : undefined
            }
            node.segmentations.push(newSeg)
        } else if (name === "value" || name === "capacity") {
            node.segmentations.filter(ele => ele.seq === seq)[0][name] = currentTarget.value

            if (name === "capacity") {
                if (isRoundRobin) {
                    // const parent = nodes.filter(ele => ele.code === node.parent)[0]
                    const rrPct = node.segmentations.reduce((acc, cur) => [...acc, parseInt("" + cur.capacity)], [])
                    const ratio = findRatio(rrPct)
                    const rrVals = findRoundRobinVals(ratio)
                    const sum = ratio.reduce((acc, cur) => {
                        acc += cur
                        return acc
                    }, 0)

                    node.segmentations = node.segmentations.map((seg, iseg) => {
                        return {
                            ...seg, conditions: [{
                                "seq": 1,
                                "name": "",
                                "left": "MOD(round_robin, " + sum + ")",
                                "right": "(" + rrVals[iseg].join(",") + ")",
                                "operator": "IN",
                                "connector": ""
                            }]
                        }
                    })
                }
            }
        } else if (name === "remove") {
            node.segmentations = node.segmentations.filter(ele => ele.seq !== seq)
        } else {
            node[name] = currentTarget.value
        }
    }

    if (isRoundRobin) {
        node.orderBy = Array.isArray(node.orderBy) ? node.orderBy : []
    } else {
        node.orderBy = undefined
    }
}

export function getPendingSegment(nodes, node) {
    return node.segmentations.filter(ele => {
        return nodes.filter(added => added.parent === node.code &&
            added.parentSegment === ele.seq).length === 0
    })
}
