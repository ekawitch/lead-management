import React, { useContext } from 'react'
import fieldHelperContext, { popFieldHelper } from '../../../context/FieldHelperContext'

const SourceKeyProperty = ({ node, onChange }) => {
    const { setFieldHelperState } = useContext(fieldHelperContext)

    return <>
        <p className="header3 mt-5">
            Source Key
            <a href="/#" className="text-success ms-2 font-70pc" data-event="node-sourcekey" name="new" onClick={(e) => onChange({ e })}>add</a>
        </p>
        {Array.isArray(node.sourcekeys) && node.sourcekeys.map(ele => <div key={ele.seq} className="row mt-1">
            <div className="col-10">
                {/* <input className="form-control" data-event="node-sourcekey" name="value" data-seq={ele.seq} value={ele.value} onChange={(e) => onChange({ e })} ></input> */}
                <div className="input-group">
                    <input className="form-control" data-event="node-sourcekey" name="value" data-seq={ele.seq} value={ele.value} readOnly={true}></input>
                    <button type="button"
                        className="btn btn-outline-secondary icon-more"
                        data-seq={ele.seq}
                        data-type="field"
                        onClick={(e) => popFieldHelper(e, setFieldHelperState)}>
                    </button>
                </div>
            </div>

            <div className="col-2">
                <a href="/#" className="text-danger font-70pc" data-event="node-sourcekey" data-seq={ele.seq} name="remove" onClick={(e) => onChange({ e })}>remove</a>
            </div>
        </div>)}
    </>
}

const onSourceKeyPropertyChange = (e, node, currentTarget) => {
    const dataset = currentTarget.dataset
    const name = currentTarget.name
    const seq = isNaN(dataset.seq) ? undefined : parseInt(dataset.seq)

    if (name === "new") {
        e.preventDefault()
        node.sourcekeys = node.sourcekeys ? node.sourcekeys : []

        const nextSeq = node.sourcekeys.reduce((acc, cur) => acc > cur.seq ? acc : cur.seq, 0) + 1
        node.sourcekeys.push({ seq: nextSeq, value: "" })
    } else if (name === "value") {
        node.sourcekeys.filter(ele => ele.seq === seq)[0].value = currentTarget.value
    } else if (name === "remove") {
        node.sourcekeys = node.sourcekeys.filter(ele => ele.seq !== seq)
    }
}

export default SourceKeyProperty
export { onSourceKeyPropertyChange }