import React from 'react'

const SQLProperty = ({ node, onChange }) => {
    return <>
        <h3 className="header3 mt-5">
            SQL
        </h3>
        <div className="row">
            <div className="col-12">
                <textarea className="form-control min-h250"
                    data-event="node-property"
                    name="sql"
                    value={node.sql}
                    onChange={(e) => onChange({ e })}>
                </textarea>
            </div>
        </div>
    </>
}

export default SQLProperty