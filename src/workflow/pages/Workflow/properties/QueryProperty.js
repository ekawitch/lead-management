import React from 'react'
import { useState } from 'react'
// import { useEffect } from 'react'
import NavTab, { isTabActived } from '../../../components/NavTab'
import QueryField from './Query/QueryField'
import QueryGroupBy from './Query/QueryGroupBy'
import QueryPartitionBy from './Query/QueryPartitionBy'
import QueryOrderBy from './Query/QueryOrderBy'
import QueryTable from './Query/QueryTable'
import QueryWhere from './Query/QueryWhere'
import { TabRegistry } from '../components/BlockComponent'

const QueryProperty = ({
    sqlInfo,
    onChange,
    node,
    isShow = true
}) => {
    const tabs = TabRegistry[node.shape] ? [...TabRegistry[node.shape]] : []
    const [tabstate, settabstate] = useState({ value: tabs ? tabs[0].value : undefined })
    const isField = node.datasource === "STAGING" ||
        (Array.isArray(node.fields) && node.tables.length > 0 && node.tables[0].name)
    const isWhere = Array.isArray(node.conditions) &&
        (node.datasource === "STAGING" || (node.tables.length > 0 && node.tables[0].name))
    const isGroupBy = Array.isArray(node.groupBy) &&
        (node.datasource === "STAGING" || (node.tables.length > 0 && node.tables[0].name))
    const isPartitionBy = Array.isArray(node.partitionBy) &&
        (node.datasource === "STAGING" || (node.tables.length > 0 && node.tables[0].name))
    const isOrderBy = Array.isArray(node.orderBy) &&
        (node.datasource === "STAGING" || (node.tables.length > 0 && node.tables[0].name))

    return <>
        {isShow && <>
            {/* <h3 className="header3 mt-5">Query</h3> */}

            {node.isSqlVisualizer && <div className="row">
                <div className="col-12 d-flex justify-content-end mt-5">
                    <a href="/#"
                        className="text-success font-70pc"
                        data-event="sql-visualize-on"
                        onClick={(e) => onChange({ e })}>
                        Join Wizard
                    </a>
                </div>
            </div>}

            {tabs && <NavTab
                tabs={tabs}
                value={tabstate.value}
                callback={settabstate}
                navClass={node.isSqlVisualizer ? "" : "mt-5"}>
            </NavTab>}

            {isTabActived(tabstate.value, "table") &&
                Array.isArray(node.tables) &&
                <QueryTable node={node}
                    sqlInfo={sqlInfo}
                    onChange={onChange}></QueryTable>}

            {isTabActived(tabstate.value, "field") &&
                isField && <QueryField
                    sqlInfo={sqlInfo}
                    node={node}
                    onChange={onChange}></QueryField>}

            {isTabActived(tabstate.value, "where") &&
                isWhere && <QueryWhere
                    node={node}
                    element={node}
                    onChange={onChange}></QueryWhere>}

            {/* Array.isArray(node.groupBy) */}
            {isTabActived(tabstate.value, "group_by") &&
                isGroupBy && <QueryGroupBy
                    node={node}
                    onChange={onChange}></QueryGroupBy>}


            {isTabActived(tabstate.value, "partition_by") &&
                isPartitionBy && <QueryPartitionBy
                    node={node}
                    onChange={onChange}></QueryPartitionBy>}

            {isTabActived(tabstate.value, "order_by") &&
                isOrderBy && <QueryOrderBy
                    node={node}
                    onChange={onChange}></QueryOrderBy>}
        </>}
    </>
}

export default QueryProperty