import React, { useContext } from 'react'
import { useState } from 'react'
import NavTab from '../../../../components/NavTab'
import TemplatePanel from './TemplatePanel'
import FieldPanel from './FieldPanel'
import SQLFunction from './SQLFunction'
import fieldHelperContext, { popFieldHelper } from '../../../../context/FieldHelperContext'

export default function FieldHelper({
    sqlInfo,
    onChange,
    node,
}) {
    const { fieldHelperState, setFieldHelperState } = useContext(fieldHelperContext)
    const [tabstate, settabstate] = useState({ value: "sql" })

    let clsBackdrop = "modal-backdrop fade"
    let clsModal = "modal fade"

    if (fieldHelperState.isShow) {
        clsBackdrop += " show"
        clsModal += " show"
    }

    const isAllowTemplate = fieldHelperState.allowTemplate === "Y"
    const tabs = isAllowTemplate ? [
        { value: "sql", label: "SQL" },
        { value: "sql_function", label: "SQL Function" },
        { value: "template", label: "Template" }
    ] : []

    return <>
        {fieldHelperState.isShow && <div className={clsBackdrop}>
        </div>}
        <div id="SQLFieldModal" className={clsModal}>
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="exampleModalLiveLabel">SQL Fields</h5>
                        <button type="button"
                            className="btn-close"
                            data-bs-dismiss="modal"
                            aria-label="Close"
                            data-event="sql-field-close"
                            onClick={(e) => popFieldHelper(e, setFieldHelperState, false)}></button>
                    </div>
                    <div className="modal-body">

                        {isAllowTemplate && <NavTab
                            tabs={tabs}
                            value={tabstate.value}
                            callback={settabstate}
                            navClass="mb-3">
                        </NavTab>}

                        {tabstate.value === "sql" && <FieldPanel
                            sqlInfo={sqlInfo}
                            node={node}
                            onChange={onChange}>
                        </FieldPanel>}

                        {tabstate.value === "sql_function" && <SQLFunction
                            onChange={onChange}>
                        </SQLFunction>}

                        {tabstate.value === "template" && <TemplatePanel
                            onChange={onChange}>
                        </TemplatePanel>}
                    </div>
                </div>
            </div>
        </div>
    </>
}