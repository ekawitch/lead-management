import { useContext } from "react"
import { Fragment, useState } from "react"
import { preventDefault } from "../../../../service/EventService"
import workflowContext from '../../../../context/WorkflowContext';
import fieldHelperContext from '../../../../context/FieldHelperContext';
import { getSQLFields } from "../../../../service/NodeService";

export default function FieldPanel({
    sqlInfo = {},
    node,
    onChange,
}) {
    const { workflowState } = useContext(workflowContext)
    const { fieldHelperState } = useContext(fieldHelperContext)
    const [myState, setMyState] = useState({ less: [] })
    const options = getSQLFields(workflowState.nodes, fieldHelperState.staging, sqlInfo, node)

    const onToggleLess = ({ e }) => {
        preventDefault(e)

        const currentTarget = e.currentTarget
        const dataset = currentTarget.dataset
        const event = dataset.event
        const table = dataset.table
        let lessVal = myState.less

        if (event === "less") {
            lessVal.push(table)
        } else {
            lessVal = myState.less.filter(ele => ele !== table)
        }

        setMyState({ ...myState, less: [...lessVal] })
    }

    const onSelectionToggle = (e) => {
        preventDefault(e)

        const currentTarget = e.currentTarget
        const dataset = currentTarget.dataset
        const eParam = {
            e: {
                currentTarget: {
                    dataset: {
                        event: fieldHelperState.returnEvent,
                        table: dataset.table,
                        property: dataset.property,
                        seq: dataset.seq
                    },
                    name: currentTarget.name
                }
            },
            sqlInfo
        }

        if (currentTarget.checked) {
            eParam.e.currentTarget.name = "new"

            const tblRslt = getSQLFields(workflowState.nodes, fieldHelperState.staging, sqlInfo, node, dataset.table)
            const allFlds = tblRslt.length === 0 ? [] : tblRslt[0].properties.map(prop => dataset.table + "." + prop.name)
            const leftFlds = allFlds.filter(fldName => node.fields.filter(fld => fld.name === fldName).length === 0)

            if (leftFlds.length === 1) {
                if (leftFlds[0] === dataset.table + "." + dataset.property) {
                    eParam.e.currentTarget.name = "selectall"
                    eParam.e.currentTarget.dataset.tableAlias = dataset.table
                }
            }
        } else {
            eParam.e.currentTarget.name = "remove"
        }

        onChange(eParam)
    }

    return <>{options.map(opt => {
        const optAlias = opt.alias ? opt.alias : ""
        const optAliasPrefix = optAlias ? optAlias + "." : ""

        return <Fragment key={opt.seq}>
            <p className="header3 mb-1">
                <strong>{opt.description ? opt.description : opt.name}</strong> {optAlias && <>({optAlias})</>}

                <a href="/#"
                    className="text-success font-70pc ms-3"
                    data-event={fieldHelperState.returnEvent}
                    data-table={opt.name}
                    data-table-alias={optAlias}
                    name="selectall"
                    onClick={(e) => onChange({ e, sqlInfo })}>
                    Select All
                </a>
                <span className="mx-2">/</span>
                <a href="/#"
                    className="text-success font-70pc"
                    data-event={fieldHelperState.returnEvent}
                    data-table={opt.name}
                    data-table-alias={optAlias}
                    name="unselectall"
                    onClick={(e) => onChange({ e, sqlInfo })}>
                    Unselect All
                </a>



                {myState.less.indexOf(optAlias) === -1 && <>
                    <span className="mx-2">/</span>
                    <a href="/#"
                        className="text-success font-70pc"
                        data-event="less"
                        data-table={optAlias}
                        onClick={(e) => onToggleLess({ e })}>Less</a>
                </>}

                {myState.less.indexOf(optAlias) !== -1 && <>
                    <span className="mx-2">/</span>
                    <a href="/#"
                        className="text-success font-70pc"
                        data-event="more"
                        data-table={optAlias}
                        onClick={(e) => onToggleLess({ e })}>More</a>
                </>}

            </p>
            <div className="mb-3">
                {myState.less.indexOf(optAlias) === -1 && opt.properties && <>
                    {!fieldHelperState.isSelection && opt.properties.map(prop => <Fragment key={prop.name}>
                        <p className="m-0">
                            <a href="/#"
                                className="text-primary ms-3"
                                data-event="sql-field-helper"
                                data-return-event={fieldHelperState.returnEvent}
                                data-type={fieldHelperState.type}
                                data-field={fieldHelperState.field}
                                data-table={optAlias}
                                data-property={prop.name}
                                data-append={fieldHelperState.append}
                                onClick={(e) => onChange({ e })}>{optAliasPrefix}{prop.name}</a>
                        </p>
                    </Fragment>)}
                    {fieldHelperState.isSelection && opt.properties.map(prop => {
                        const optVal = optAliasPrefix + prop.name
                        const optStar = optAliasPrefix + "*"
                        const optSelected = node[fieldHelperState.type].filter(ele => ele[fieldHelperState.field] === optVal || ele[fieldHelperState.field] === optStar)
                        const isOptSelected = optSelected.length > 0

                        return <Fragment key={prop.name}>
                            <div className="form-check">
                                <input className="form-check-input"
                                    type="checkbox"
                                    value={optVal}
                                    name={isOptSelected ? "remove" : "new"}
                                    id={"MultiSQLField" + opt.seq + "_" + prop.name}
                                    data-type={fieldHelperState.type}
                                    data-seq={isOptSelected ? optSelected[0].seq : undefined}
                                    data-field={fieldHelperState.field}
                                    data-table={optAlias}
                                    data-property={prop.name}
                                    onChange={(e) => onSelectionToggle(e)}
                                    checked={isOptSelected}></input>
                                <label className="form-check-label" htmlFor={"MultiSQLField" + opt.seq + "_" + prop.name}>
                                    <span className="ms-1" data-type={fieldHelperState.type} data-table={optAlias} data-property={prop.name}>{optAliasPrefix}{prop.name}</span>
                                </label>
                            </div>
                        </Fragment>
                    })}
                </>}
            </div>
        </Fragment>
    })}
    </>
}