import React, { Fragment } from 'react'
import { useContext } from 'react'
import { JOIN_OPERATOR } from '../../../../config/Operator'
import workflowContext from '../../../../context/WorkflowContext'
import { updateAlias } from '../../../../service/NodeService'
import { getNextSeq } from '../../../../service/ObjectService'
import { calcTree, calcTreeOffset } from '../../../../service/WorkflowService'
import QueryWhere from './QueryWhere'

export default function QueryTable({
    sqlInfo,
    node,
    onChange,
}) {
    const { workflowState, setWorkflowState } = useContext(workflowContext)
    const isTableLimit = node.tableLimit !== undefined && node.tables.length >= node.tableLimit
    let tables = []

    if (node.datasource === "WORKFLOW") {
        tables = workflowState.nodes.filter(ele => ele.code !== node.code).map(ele => {
            return { value: ele.code, name: ele.code, label: ele.code + " " + ele.description }
        })
    } else {
        tables = sqlInfo.tables.map(ele => {
            return { ...ele, value: ele.schema + "." + ele.name }
        })
    }

    return <>
        <div className="row">
            <div className="col-12 d-flex justify-content-end my-3">
                {!isTableLimit && <a href="/#"
                    className="text-success ms-2 font-70pc"
                    data-event="node-table"
                    data-node-code={node.code}
                    name="new"
                    onClick={(e) => onTableChange(e, workflowState, setWorkflowState)}>
                    Add Table
                </a>}
            </div>
        </div>

        {node.datasource === "STAGING" && node.parent && <div className="row mt-1"></div>}

        {node.tables.map(table => <Fragment key={table.seq}>
            <div className="row mt-1">
                <div className="col-5">
                    {node.datasource && <select className="form-control"
                        data-event="node-table"
                        data-seq={table.seq}
                        name="table-name"
                        data-node-code={node.code}
                        value={table.name}
                        onChange={(e) => onTableChange(e, workflowState, setWorkflowState)}>
                        {!table.name && <option></option>}
                        {/* {tables.map(t => <option key={t.key ? t.key : t.name} value={t.name}>{t.label ? t.label : t.name}</option>)} */}
                        {tables.map(t => <option key={t.value} value={t.value}>{t.label}</option>)}
                    </select>}

                    {/* {node.datasource === "STAGING" && <input className="form-control" data-event="node-table" data-seq={table.seq} name="table-name" value={table.name} onChange={(e) => onChange({ e })}></input>} */}
                </div>
                <div className=" col-5">
                    <input className="form-control"
                        type="text"
                        data-event="node-table"
                        data-node-code={node.code}
                        data-seq={table.seq}
                        name="table-alias"
                        value={table.alias}
                        onChange={(e) => onTableChange(e, workflowState, setWorkflowState)}
                        onBlur={(e) => onTableChange(e, workflowState, setWorkflowState)}
                        onFocus={(e) => onTableChange(e, workflowState, setWorkflowState)}
                        placeholder="alias"></input>
                </div>
                <div className=" col-2">
                    <a href="/#"
                        className="text-danger font-70pc"
                        data-event="node-table"
                        data-node-code={node.code}
                        data-seq={table.seq}
                        name="remove" onClick={(e) => onTableChange(e, workflowState, setWorkflowState)}>remove</a>
                </div>
            </div>


            {table.seq > 1 && <div className="row mb-3 font-70pc">
                <div className="col-12">
                    <div className="row">
                        <div className="col-12">
                            <p className="header3 mb-0 text-end">Join</p>
                            <div className="row d-flex justify-content-end">
                                <div className="col-4">
                                    <select className="form-control text-end"
                                        data-event="node-table"
                                        data-node-code={node.code}
                                        data-seq={table.seq}
                                        name="table-join"
                                        value={table.join}
                                        onChange={(e) => onTableChange(e, workflowState, setWorkflowState)}>
                                        {!table.join && <option></option>}
                                        {JOIN_OPERATOR.map(ele => <option key={ele.value} value={ele.value}>{ele.label}</option>)}
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <QueryWhere node={node}
                        element={table}
                        onChange={onChange}
                        event="table-condition"
                        header="Condition"
                        headerClass="header3 mb-0"
                        // type="table-condition"
                        tableSeq={table.seq}
                        leftStaging={node.datasource === "STAGING" ? node.parent : undefined}
                        rightStaging={node.datasource === "STAGING" ? table.name : undefined}>
                    </QueryWhere>
                </div>
            </div>}
        </Fragment>)
        }
    </>
}

const getAutoAlias = (node, tableSeg, tableName) => {
    // const existing = node.tables.filter(ele => ele.seq !== tableSeg).map(ele => ele.alias)
    // let rslt = tableName

    // for (let i = 1; existing.indexOf(rslt) > -1; i++) {
    //     rslt = tableName + padLeftStr(i, 2, "0")
    // }

    // return rslt
    return "tbl" + tableSeg
}

export function onTableChange(
    e,
    workflowState,
    setWorkflowState
) {
    const currentTarget = e.currentTarget
    const dataset = currentTarget.dataset
    const nodeCode = dataset.nodeCode
    const nodes = workflowState.nodes
    const node = nodes.filter(ele => ele.code === nodeCode)[0]
    const name = currentTarget.name

    if (name === "new") {
        node.tables = node.tables ? node.tables : []

        const nextSeq = getNextSeq(node.tables, "seq")
        let newTable = dataset.table

        if (!newTable) {
            if (node.datasource === "STAGING") {
                if (node.tables.length === 0) {
                    if (node.parent) {
                        newTable = nodes.filter(srch => srch.code === node.parent)[0].code
                    }
                }
            }
        }

        const newTableObj = {
            seq: nextSeq,
            name: newTable ? newTable : "",
            alias: newTable ? getAutoAlias(node, nextSeq, newTable) : "",
            join: node.tables.length > 0 ? "INNER" : "",
            conditions: []
        }
        node.tables.push(newTableObj)
    } else if (name === "remove") {
        node.tables = node.tables ? node.tables : []
        node.tables = node.tables.filter(ele => ele.seq !== parseInt(dataset.seq))

        if (node.tables.length === 0) {
            node.fields = []
            node.conditions = []
            node.groupBy = []
        }
    } else if (["table-name",
        "table-alias",
        "table-join",
        "condition-new",
        "condition-cmd",
        "condition-remove"].indexOf(name) > -1) {
        const table = node.tables.filter(ele => ele.seq === parseInt(dataset.seq))[0]

        if (name === "table-name") {
            table.name = currentTarget.value
            if (!table.alias) {
                table.alias = getAutoAlias(node, table.seq, table.name)
            }
        } else if (name === "table-alias") {
            if (e.type === "focus") {
                currentTarget.dataset.persist = currentTarget.value
            } else if (e.type === "blur") {
                if (currentTarget.dataset.persist) {
                    if (currentTarget.dataset.persist !== currentTarget.value) {
                        // //update select
                        // node.fields = node.fields.map(ele => {
                        //     return {
                        //         ...ele,
                        //         name: replaceAlias(ele.name, currentTarget.dataset.persist, currentTarget.value)
                        //     }
                        // })

                        //update where
                        // node.conditions = node.conditions.map(ele => {
                        //     let params = ["left", "right", "cmd"].reduce((acc, cur) => {
                        //         if (ele[cur]) {
                        //             ele[cur] = replaceAlias(ele[cur], currentTarget.dataset.persist, currentTarget.value)
                        //         }

                        //         return acc
                        //     }, {})

                        //     return {
                        //         ...ele, ...params
                        //     }

                        // })

                        // if (node.conditions) {
                        //     node.conditions = updateCoditionAlias(node, currentTarget.dataset.persist, currentTarget.value)
                        // }
                        // if (node.tables) {
                        //     node.tables = node.tables.map(tbl => {
                        //         return {
                        //             ...tbl,
                        //             conditions: updateCoditionAlias(tbl, currentTarget.dataset.persist, currentTarget.value)
                        //         }
                        //     })
                        // }
                        updateAlias(node, currentTarget.dataset.persist, currentTarget.value)
                    }
                }
            } else {
                table.alias = currentTarget.value
            }
        } else if (name === "table-join") {
            table.join = currentTarget.value
        } else if (name === "condition-new") {
            // const nexSeq = table.conditions.reduce((acc, cur) => { return acc > cur.seq ? acc : cur.seq }, 0) + 1
            const nexSeq = getNextSeq(table.conditions, "seq")
            let newCondi = {
                seq: nexSeq,
                connection: "",
                cmd: ""
            }

            if (dataset.left &&
                dataset.right) {
                newCondi = {
                    ...newCondi,
                    connector: nexSeq === 1 ? "" : "AND",
                    operator: "EQ",
                    left: dataset.left,
                    right: dataset.right,
                    cmd: undefined
                }
            }

            table.conditions.push(newCondi)
        } else if (name === "condition-cmd") {
            const condition = table.conditions.filter(ele => ele.seq === parseInt(dataset.conditionSeq))[0]
            condition.cmd = currentTarget.value
        } else if (name === "condition-remove") {
            table.conditions = table.conditions.filter(ele => ele.seq !== parseInt(dataset.conditionSeq))
        }
    }

    if (node.description === node.title) {
        if (Array.isArray(node.tables) &&
            node.tables.length === 1 &&
            node.tables[0].alias) {
            node.description = node.tables[0].alias
        }
    }

    setWorkflowState({ ...workflowState, nodes: nodes, tree: calcTreeOffset(calcTree(nodes)) })
}