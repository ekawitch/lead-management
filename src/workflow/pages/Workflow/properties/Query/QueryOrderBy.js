import { useContext } from "react"
import { ORDER_BY_OPERATOR } from "../../../../config/Operator"
import fieldHelperContext, { popFieldHelper } from "../../../../context/FieldHelperContext"
import { getSQLFields } from "../../../../service/NodeService"
import { getNextSeq } from "../../../../service/ObjectService"

export default function QueryOrderBy({
    node,
    onChange,
    staging
}) {
    const { setFieldHelperState } = useContext(fieldHelperContext)

    return (<>
        <div className="row">
            <div className="col-12 d-flex justify-content-end my-3">
                <a href="/#"
                    className="text-success ms-2 font-70pc"
                    data-is-selection="true"
                    data-return-event="node-order-by"
                    data-type="orderBy"
                    data-field="name"
                    data-staging={staging}
                    onClick={(e) => popFieldHelper(e, setFieldHelperState)}>
                    Existing Field
                </a>
                <span className="mx-2">/</span>
                <a href="/#"
                    className="text-success font-70pc"
                    data-event="node-order-by"
                    name="new"
                    onClick={(e) => onChange({ e })}>
                    Freetext Order
                </a>
            </div>
        </div>
        {Array.isArray(node.orderBy) &&
            node.orderBy.length > 0 &&
            node.orderBy.map(ele => <div key={ele.seq} className="row mt-1">
                <div className="col-8">
                    <div className="input-group">
                        <input type="text"
                            className="form-control"
                            // readOnly={true}
                            data-event="node-order-by"
                            data-seq={ele.seq}
                            data-field="name"
                            name="name"
                            value={ele.name}
                            onChange={(e) => onChange({ e })}></input>

                        <button type="button"
                            className="btn btn-outline-secondary icon-more"
                            data-return-event="node-order-by"
                            data-field="name"
                            data-seq={ele.seq}
                            data-append="Y"
                            onClick={(e) => popFieldHelper(e, setFieldHelperState)}>
                        </button>
                    </div>
                </div>
                <div className="col-2">
                    <select className="form-control"
                        data-event="node-order-by"
                        data-seq={ele.seq}
                        data-field="direction"
                        name="direction"
                        value={ele.direction}
                        onChange={(e) => onChange({ e })}>
                        {ORDER_BY_OPERATOR.map(ordEle => <option key={ordEle.value} value={ordEle.value}>{ordEle.label}</option>)}
                    </select>
                </div>
                <div className="col-2">
                    <a href="/#"
                        className="text-danger font-70pc"
                        data-event="node-order-by"
                        data-seq={ele.seq}
                        name="remove"
                        onClick={(e) => onChange({ e })}>remove</a>
                </div>
            </div>)}


    </>)
}

export function onOrderByChange(
    node,
    currentTarget,
    dataset,
    nodes,
    sqlInfo
) {
    const name = currentTarget.name

    if (name === "new") {
        const nextSeq = getNextSeq(node.orderBy, "seq")
        node.orderBy.push({
            seq: nextSeq,
            name: dataset.property ? dataset.table + "." + dataset.property : "",
            direction: "ASC"
        })
    } else if (name === "remove") {
        node.orderBy = node.orderBy.filter(ele => ele.seq !== parseInt(dataset.seq))
    } else if (name === "selectall") {
        const nextSeq = getNextSeq(node.orderBy, "seq")
        const sqlTables = getSQLFields(nodes, undefined, sqlInfo, node, dataset.table)
        const validSqlFields = sqlTables.length > 0 ?
            sqlTables[0].properties.filter(fld => node.orderBy.filter(xFld => xFld.name === fld).length === 0)
                .map((vFld, iFld) => {

                    return {
                        seq: nextSeq + iFld,
                        name: dataset.tableAlias + "." + vFld.name,
                        direction: "ASC"
                    }
                }) :
            []

        node.orderBy = [...node.orderBy, ...validSqlFields]
    } else if (name === "unselectall") {
        const sqlTables = getSQLFields(nodes, undefined, sqlInfo, node, dataset.table)
        const sqlFields = sqlTables.length > 0 ? sqlTables[0].properties
            .map(fld => dataset.tableAlias + "." + fld.name) : []

        node.orderBy = node.orderBy.filter(fld => sqlFields.indexOf(fld.name) === -1)
    } else if (name === "name" || name === "direction") {
        const grp = node.orderBy.filter(ele => ele.seq === parseInt(dataset.seq))[0]
        grp[name] = currentTarget.value
    }
}