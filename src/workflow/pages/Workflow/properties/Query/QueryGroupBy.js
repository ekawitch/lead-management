import { useContext } from "react"
import fieldHelperContext, { popFieldHelper } from "../../../../context/FieldHelperContext"
import { getNextSeq } from "../../../../service/ObjectService"

const QueryGroupBy = ({
    node,
}) => {
    const { setFieldHelperState } = useContext(fieldHelperContext)

    return (<>
        <div className="row">
            <div className="col-12 d-flex justify-content-end my-3">
                <a href="/#"
                    className="text-success ms-2 font-70pc"
                    data-is-selection="true"
                    data-return-event="node-group-by"
                    data-type="groupBy"
                    data-field="name" onClick={(e) => popFieldHelper(e, setFieldHelperState)}>
                    Existing Field
                </a>
            </div>
        </div>
        {!node.isLegacyField && <p className="header3 mt-3">
            {Array.isArray(node.groupBy) && node.groupBy.length > 0 && <span className="ms-3">{node.groupBy.map(ele => ele.name).join(", ")}</span>}

        </p>}


    </>)
}

const onGroupByChange = (node, currentTarget, dataset) => {
    const name = currentTarget.name

    if (name === "new") {
        const nextSeq = getNextSeq(node.groupBy, "seq")
        node.groupBy.push({
            seq: nextSeq,
            name: dataset.table + "." + dataset.property
        })
    } else if (name === "remove") {
        node.groupBy = node.groupBy.filter(ele => ele.name !== dataset.table + "." + dataset.property)
    } else if (name === "name") {
        const grp = node.groupBy.filter(ele => ele.seq === parseInt(dataset.seq))[0]
        grp.name = currentTarget.value
    }
}

export default QueryGroupBy
export {
    onGroupByChange
}