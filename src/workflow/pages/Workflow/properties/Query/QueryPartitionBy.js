import { useContext } from "react"
import fieldHelperContext, { popFieldHelper } from "../../../../context/FieldHelperContext"
import { getNextSeq } from "../../../../service/ObjectService"

const QueryPartitionBy = ({
    node,
    staging
}) => {
    const { setFieldHelperState } = useContext(fieldHelperContext)

    return (<>
        <div className="row">
            <div className="col-12 d-flex justify-content-end my-3">
                <a href="/#"
                    className="text-success ms-2 font-70pc"
                    data-is-selection="true"
                    data-return-event="node-partition-by"
                    data-type="partitionBy"
                    data-field="name"
                    data-staging={staging}
                    onClick={(e) => popFieldHelper(e, setFieldHelperState)}>
                    Existing Field
                </a>
            </div>
        </div>
        {Array.isArray(node.partitionBy) &&
            node.partitionBy.length > 0 && <div className="row mt-1">
                <div className="col-12">{node.partitionBy.map(ele => ele.name).join(", ")}</div>
            </div>}


    </>)
}

const onPartitionByChange = (node, currentTarget, dataset) => {
    const name = currentTarget.name

    if (name === "new") {
        const nextSeq = getNextSeq(node.partitionBy, "seq")
        node.partitionBy.push({
            seq: nextSeq,
            name: dataset.table + "." + dataset.property
        })
    } else if (name === "remove") {
        node.partitionBy = node.partitionBy.filter(ele => ele.seq !== parseInt(dataset.seq))
    } else if (name === "name") {
        const grp = node.partitionBy.filter(ele => ele.seq === parseInt(dataset.seq))[0]
        grp[name] = currentTarget.value
    }
}

export default QueryPartitionBy
export {
    onPartitionByChange
}