import { useContext } from "react"
import { CONNECTOR_OPERATOR, WHERE_OPERATOR } from "../../../../config/Operator"
import fieldHelperContext, { popFieldHelper } from "../../../../context/FieldHelperContext"
import { getNextSeq } from "../../../../service/ObjectService"

const QueryWhere = ({ node,
    element,
    onChange,
    type = "where",
    event = "node-condition",
    leftStaging,
    rightStaging,
    tableSeq,
    sourceCode,
    segmentationSeq
}) => {
    const { setFieldHelperState } = useContext(fieldHelperContext)

    return (<div className="mb-2">
        {/* <p className={headerClass}>
            {header}
        </p> */}

        <div className="row">
            <div className="col-12 d-flex justify-content-end my-3">
                <a href="/#"
                    className="text-success ms-2 font-70pc"
                    data-event={event}
                    data-node-code={node.code}
                    data-table-seq={tableSeq}
                    data-source-code={sourceCode}
                    data-segmentation-seq={segmentationSeq}
                    name="new"
                    onClick={(e) => onChange({ e })}>
                    New Condition
                </a>
                <span className="mx-2">/</span>
                <a href="/#"
                    className="text-success font-70pc"
                    data-event={event}
                    data-node-code={node.code}
                    data-table-seq={tableSeq}
                    data-source-code={sourceCode}
                    data-segmentation-seq={segmentationSeq}
                    name="new-freetext"
                    onClick={(e) => onChange({ e })}>
                    Freetext Condition
                </a>
            </div>
        </div>
        {element.conditions.map(where => <div key={where.seq} className="row mt-1">
            {where.cmd === undefined && <div className="col-10">
                <div className="row">
                    <div className="col-2">
                        <select className="form-control"
                            data-event={event}
                            data-node-code={node.code}
                            data-seq={where.seq}
                            data-table-seq={tableSeq}
                            data-source-code={sourceCode}
                            data-segmentation-seq={segmentationSeq}
                            data-field="connector"
                            name="connector"
                            value={where.connector}
                            onChange={(e) => onChange({ e })}>
                            {!where.connector && <option></option>}
                            {element.conditions.indexOf(where) > 0 && CONNECTOR_OPERATOR.map(ele => <option key={ele.value} value={ele.value}>{ele.label}</option>)}
                        </select>
                    </div>
                    <div className="col-4">
                        <div className="input-group">
                            <input className="form-control"
                                data-event={event}
                                data-node-code={node.code}
                                data-field="left"
                                data-seq={where.seq}
                                data-table-seq={tableSeq}
                                data-source-code={sourceCode}
                                data-segmentation-seq={segmentationSeq}
                                value={where.left}
                                onChange={(e) => onChange({ e })}>
                            </input>

                            <button type="button"
                                className="btn btn-outline-secondary icon-more"
                                data-append="Y"
                                data-field="left"
                                data-seq={where.seq}
                                data-return-event={event}
                                data-segmentation-seq={segmentationSeq}
                                data-source-code={sourceCode}
                                data-table-seq={tableSeq}
                                data-type={type}
                                data-staging={leftStaging ? leftStaging : ""}
                                onClick={(e) => popFieldHelper(e, setFieldHelperState)}></button>
                        </div>
                    </div>
                    <div className="col-2">
                        <select className="form-control"
                            data-field="operator"
                            data-event={event}
                            data-node-code={node.code}
                            data-seq={where.seq}
                            data-table-seq={tableSeq}
                            data-source-code={sourceCode}
                            data-segmentation-seq={segmentationSeq}
                            value={where.operator} onChange={(e) => onChange({ e })}>
                            {!where.operator && <option></option>}
                            {WHERE_OPERATOR.map(ele => <option key={ele.value} value={ele.value}>{ele.label}</option>)}
                        </select>
                    </div>
                    <div className="col-4">
                        <div className="input-group">
                            <input className="form-control"
                                data-event={event}
                                data-node-code={node.code}
                                data-field="right"
                                data-seq={where.seq}
                                data-table-seq={tableSeq}
                                data-source-code={sourceCode}
                                value={where.right}
                                onChange={(e) => onChange({ e })}>
                            </input>

                            <button type="button"
                                className="btn btn-outline-secondary icon-more"
                                data-append="Y"
                                data-field="right"
                                data-return-event={event}
                                data-segmentation-seq={segmentationSeq}
                                data-seq={where.seq}
                                data-staging={rightStaging ? rightStaging : ""}
                                data-source-code={sourceCode}
                                data-table-seq={tableSeq}
                                data-type={type}
                                onClick={(e) => popFieldHelper(e, setFieldHelperState)}></button>
                        </div>
                    </div>
                </div>
            </div>}
            {where.cmd !== undefined && <div className="col-10">
                <div className="input-group">
                    {/* <input className="form-control"
                        data-event={event}
                        data-node-code={node.code}
                        data-field="cmd"
                        data-seq={where.seq}
                        value={where.cmd}
                        data-table-seq={tableSeq}
                        data-source-code={sourceCode}
                        data-segmentation-seq={segmentationSeq}
                        onChange={(e) => onChange({e})}></input> */}
                    <textarea className="form-control"
                        data-event={event}
                        data-node-code={node.code}
                        data-field="cmd"
                        data-seq={where.seq}
                        value={where.cmd}
                        data-table-seq={tableSeq}
                        data-source-code={sourceCode}
                        data-segmentation-seq={segmentationSeq}
                        onChange={(e) => onChange({ e })}>
                    </textarea>
                    <button type="button"
                        className="btn btn-outline-secondary icon-more"
                        data-append="Y"
                        data-field="cmd"
                        data-seq={where.seq}
                        data-allow-template="Y"
                        data-return-event={event}
                        data-segmentation-seq={segmentationSeq}
                        data-source-code={sourceCode}
                        data-table-seq={tableSeq}
                        data-type={type}
                        onClick={(e) => popFieldHelper(e, setFieldHelperState)}></button>
                </div>
            </div>}

            <div className="col-2">
                <a href="/#"
                    className="text-danger font-70pc"
                    data-event={event}
                    data-node-code={node.code}
                    data-seq={where.seq}
                    data-table-seq={tableSeq}
                    data-source-code={sourceCode}
                    data-segmentation-seq={segmentationSeq}
                    name="remove"
                    onClick={(e) => onChange({ e })}>Remove</a>
            </div>
        </div>)}
    </div>)
}

const onWhereChange = (element, currentTarget) => {
    const dataset = currentTarget.dataset
    const name = currentTarget.name

    if (name === "new") {
        const nextSeq = getNextSeq(element.conditions, "seq")
        // element.conditions.push({ seq: nextSeq, name: "", alias: "", cmd: "", left: "", right: "", operator: "", connector: "" })
        element.conditions.push({
            seq: nextSeq,
            name: "",
            left: "",
            right: "",
            operator: "",
            connector: ""
        })
    } else if (name === "new-freetext") {
        const nextSeq = getNextSeq(element.conditions, "seq")
        element.conditions.push({ seq: nextSeq, cmd: "" })
    } else if (name === "remove") {
        element.conditions = element.conditions.filter(ele => ele.seq !== parseInt(dataset.seq))
    }
    // else if (name === "operator") {
    //     const condi = element.conditions.filter(ele => ele.seq === parseInt(dataset.seq))[0]
    //     condi.operator = currentTarget.value
    // } 
    else {
        let condi

        if (dataset.conditionSeq) {
            condi = element.conditions.filter(ele => ele.seq === parseInt(dataset.conditionSeq))[0]
        } else {
            condi = element.conditions.filter(ele => ele.seq === parseInt(dataset.seq))[0]
        }

        if (dataset.append === "Y") {
            condi[dataset.field] += currentTarget.value
        } else {
            condi[dataset.field] = currentTarget.value
        }
    }
}

export default QueryWhere
export {
    onWhereChange
}