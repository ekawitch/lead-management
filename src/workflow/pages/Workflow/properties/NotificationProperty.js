import React, { Fragment } from 'react'
import { NOTIFICATION_OPERATOR } from '../../../config/Operator'
import { getNextSeq } from '../../../service/ObjectService'

const NotificationProperty = ({ node, onChange }) => {
    return <>
        {/* <h3 className="header3 mt-5">
            Notification
        </h3> */}
        <div className="row">
            <div className="col-12 d-flex justify-content-end my-3">
                <a href="/#" className="text-success font-70pc ms-2" data-event="node-notification" name="new" onClick={(e) => onChange({ e })}>
                    Add Channel
                </a>
            </div>
        </div>

        {node.notifications.map(ele => {

            return <Fragment key={ele.seq}>
                <div className="row">
                    <div className="col-2">Function</div>
                    <div className="col-6">
                        <select className="form-control"
                            data-event="node-notification"
                            name="function"
                            data-seq={ele.seq}
                            onChange={(e) => onChange({ e })}
                            value={ele.function}>
                            {!ele.function && <option></option>}
                            {NOTIFICATION_OPERATOR.map(opt => <option key={opt.value} value={opt.value}>{opt.label}</option>)}
                        </select>
                    </div>
                    <div className="col-4">
                        <a href="/#" className="text-danger font-70pc" data-event="node-notification" data-seq={ele.seq} name="remove" onClick={(e) => onChange({ e })}>remove</a>
                    </div>
                </div>
                <div className="row mb-3 mt-1">
                    <div className="col-2">Value</div>
                    <div className="col-10">
                        <input type="text"
                            placeholder="email, phone"
                            className="form-control"
                            data-event="node-notification"
                            name="value"
                            data-seq={ele.seq}
                            value={ele.value}
                            onChange={(e) => onChange({ e })}></input>
                    </div>
                </div>
            </Fragment>
        })}
    </>
}

const onNotificationPropertyChange = (node, currentTarget, dataset) => {
    const name = currentTarget.name

    if (name === "new") {
        const nextSeq = getNextSeq(node.notifications, "seq")
        node.notifications.push({
            seq: nextSeq,
            function: "",
            value: ""
        })
    } else if (name === "remove") {
        node.notifications = node.notifications.filter(ele => ele.seq !== parseInt(dataset.seq))
    } else {
        const field = node.notifications.filter(ele => ele.seq === parseInt(dataset.seq))[0]
        field[name] = currentTarget.value
    }
}

export default NotificationProperty
export { onNotificationPropertyChange }