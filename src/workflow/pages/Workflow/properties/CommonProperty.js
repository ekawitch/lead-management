import React, { useContext } from 'react'
import { useEffect } from 'react';
import { useState } from 'react';
import workflowContext from '../../../context/WorkflowContext';
import { getDataSources } from '../../../service/API/DatabaseAPIService';

const MOCK_WORKFLOW = [
    { "value": "WF1", "label": "Workflow 01" },
    { "value": "WF2", "label": "Workflow 02" },
    { "value": "WF3", "label": "Workflow 03" }
]
const CommonProperty = ({
    node,
    onChange
}) => {
    const [datasources, setDatasources] = useState()
    const { workflowState } = useContext(workflowContext)
    const nodeKeys = Object.keys(node)
    const parentNode = node.parent ? workflowState.nodes.filter(prn => prn.code === node.parent)[0] : undefined

    useEffect(() => {
        getDataSources().then(rslt => {
            setDatasources([...rslt])
        })
    }, [setDatasources])

    return <>
        <div className="form-group row">
            <label className="col-sm-4 col-form-label">Code</label>
            <div className="col-sm-8">
                <input type="text" className="form-control" data-event="node-property" name="code" value={node.code} onChange={(e) => onChange({ e })}></input>
            </div>
        </div>
        <div className="form-group row mt-1">
            <label className="col-sm-4 col-form-label">Description</label>
            <div className="col-sm-8">
                <input type="text" className="form-control" data-event="node-property" name="description" value={node.description} onChange={(e) => onChange({ e })}></input>
            </div>
        </div>
        {nodeKeys.indexOf("isTemplate") > -1 && <div className="form-group row mt-1">
            <label className="col-sm-4 col-form-label">Is a Template</label>
            <div className="col-sm-8 form-check">
                <input type="checkbox"
                    className="form-check-source"
                    data-event="node-property"
                    data-data-type="boolean"
                    id="is_template_property"
                    name="isTemplate"
                    value="isTemplate"
                    checked={node.isTemplate}
                    onChange={(e) => onChange({ e })}></input>

                <label className="form-check-label ms-2"
                    htmlFor="is_template_property">
                    Reusable Waterfall
                </label>
            </div>
        </div>}
        {node.oracleNo !== undefined && <div className="form-group row mt-1">
            <label className="col-sm-4 col-form-label">Oracle Job Number</label>
            <div className="col-sm-8">
                <input type="text" className="form-control" data-event="node-property" name="oracleNo" value={node.oracleNo} onChange={(e) => onChange({ e })}></input>
            </div>
        </div>}
        {nodeKeys.indexOf("parentSegment") > -1 &&
            parentNode &&
            Array.isArray(parentNode.segmentations) &&
            parentNode.segmentations.length > 0 && <>
                <div className="form-group row mt-1">
                    <label className="col-sm-4 col-form-label">Parent Segment</label>
                    <div className="col-sm-8">
                        <select className={"form-control" + (node.parentSegment === 0 ? " is-invalid" : "")}
                            data-data-type="int"
                            data-event="node-property"
                            name="parentSegment"
                            value={node.parentSegment}
                            onChange={(e) => onChange({ e })}>
                            {node.parentSegment === 0 && <option></option>}
                            {parentNode.segmentations.map(sg => <option key={sg.seq} value={sg.seq}>{sg.value}</option>)}
                        </select>
                    </div>
                </div>
            </>}
        {node.workflow !== undefined && <div className="form-group row mt-1">
            <label className="col-sm-4 col-form-label">Workflow</label>
            <div className="col-sm-8">
                <select className="form-control"
                    data-event="node-property"
                    name="workflow"
                    value={node.workflow}
                    onChange={(e) => onChange({ e })}>
                    {!node.workflow && <option></option>}
                    {MOCK_WORKFLOW.map(wf => <option value={wf.value}>{wf.label}</option>)}
                </select>
            </div>
        </div>}
        {node.staging !== undefined && <div className="form-group row mt-1">
            <label className="col-sm-4 col-form-label">Staging Table</label>
            <div className="col-sm-8">
                <input type="text" className="form-control" data-event="node-property" name="staging" value={node.staging} onChange={(e) => onChange({ e })}></input>
            </div>
        </div>}
        {nodeKeys.indexOf("datasource") > -1 && <div className="form-group row mt-1">
            <label className="col-sm-4 col-form-label">Data Source</label>
            <div className="col-sm-8">
                <select className={"form-control " + (node.datasource ? "" : "is-invalid")}
                    data-event="node-datasource"
                    name="datasource"
                    value={node.datasource}
                    onChange={(e) => onChange({ e })}>
                    {!node.datasource && <option></option>}
                    {datasources && datasources.filter(ele => {
                        return node.datasourceReadOnly === undefined || ele.value === node.datasource
                    })
                        .map(ele => <option key={ele.value} value={ele.value}>{ele.label}</option>)}
                </select>

            </div>
        </div>
        }


        {node.campaignName !== undefined && <div className="form-group row mt-1">
            <label className="col-sm-4 col-form-label">Campaign Code</label>
            <div className="col-sm-8">
                <input type="text"
                    className={"form-control" + (node.campaignName ? "" : " is-invalid")}
                    data-event="node-property"
                    name="campaignName"
                    value={node.campaignName}
                    onChange={(e) => onChange({ e })}></input>
            </div>
        </div>}


        <input type="file" accept=".csv" id="fileDS" className="invisible"></input>
    </>
}

const onCommonPropertyChange = (node, currentTarget) => {
    const dataset = currentTarget.dataset
    const dataType = dataset.dataType

    if (dataType === "int") {
        node[currentTarget.name] = parseInt(currentTarget.value)
    } else if (dataType === "boolean") {
        node[currentTarget.name] = currentTarget.checked
    } else {
        node[currentTarget.name] = currentTarget.value
    }

    if (currentTarget.name === "datasource") {
        if (currentTarget.value === "CSV") {
            document.getElementById("fileDS").click()
        }
    } else if (currentTarget.name === "parentSegment") {
        if (node.description === "Output") {
            if (currentTarget.options && currentTarget.selectedIndex) {
                node.description = currentTarget.options[currentTarget.selectedIndex].text
            }
        }
    }
}

export default CommonProperty
export { onCommonPropertyChange }