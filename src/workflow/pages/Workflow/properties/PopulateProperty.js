import React from 'react'
import QueryField from './Query/QueryField'

const PopulateProperty = ({ nodes, sqlInfo, onChange, node }) => {
    return <QueryField nodes={nodes}
        sqlInfo={sqlInfo}
        node={node}
        onChange={onChange}></QueryField>
}

export default PopulateProperty