import { Fragment, useContext } from "react"
import fieldHelperContext from "../../../../context/FieldHelperContext"
import template from "../../../../payload/template"
import { getTemplateTag } from "../../../../service/TemplateService"

const TemplatePanel = ({ onChange }) => {
    const { fieldHelperState } = useContext(fieldHelperContext)
    const options = template.nodes.map(ele => {
        return {
            code: ele.code,
            description: ele.description,
            tag: getTemplateTag(ele.code)
        }
    })

    return <>
        {options.map(opt => <Fragment key={opt.code}>
            <p className="m-0">
                <a href="/#"
                    className="text-primary ms-3"
                    data-event="sql-field-helper"
                    data-return-event={fieldHelperState.returnEvent}
                    data-type={fieldHelperState.type}
                    data-field={fieldHelperState.field}
                    data-tag={opt.tag}
                    onClick={(e) => onChange({ e })}>
                    <strong>{opt.tag}</strong> - {opt.description}
                </a>
            </p>
        </Fragment>)}
    </>
}
export default TemplatePanel