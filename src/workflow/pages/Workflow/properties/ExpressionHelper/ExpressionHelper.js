import React from 'react'
import { useState } from 'react'
import NavTab from '../../../../components/NavTab'
import TemplatePanel from './TemplatePanel'
import FieldPanel from './FieldPanel'
import SQLFunction from './SQLFunction'

export default function ExpressionHelper({
    sqlInfo,
    onChange,
    node,
    isAllowTemplate = "Y"
}) {
    const [tabstate, settabstate] = useState({ value: "sql" })

    const tabs = isAllowTemplate ? [
        { value: "sql", label: "SQL" },
        { value: "sql_function", label: "SQL Function" },
        { value: "template", label: "Template" }
    ] : []

    return <>
        <div className="modal-backdrop fade show">
        </div>
        <div id="SQLFieldModal" className="modal fade show">
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="exampleModalLiveLabel">SQL Fields</h5>
                        <button type="button"
                            name="close"
                            className="btn-close"
                            data-bs-dismiss="modal"
                            aria-label="Close"
                            data-event="sql-field-close"
                            onClick={(e) => oncancel}></button>
                    </div>
                    <div className="modal-body">

                        {isAllowTemplate && <NavTab
                            tabs={tabs}
                            value={tabstate.value}
                            callback={settabstate}
                            navClass="mb-3">
                        </NavTab>}

                        {tabstate.value === "sql" && <FieldPanel
                            sqlInfo={sqlInfo}
                            node={node}
                            onChange={onChange}>
                        </FieldPanel>}

                        {tabstate.value === "sql_function" && <SQLFunction
                            onChange={onChange}>
                        </SQLFunction>}

                        {tabstate.value === "template" && <TemplatePanel
                            onChange={onChange}>
                        </TemplatePanel>}
                    </div>
                </div>
            </div>
        </div>
    </>
}