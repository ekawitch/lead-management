import { useContext } from "react"
import fieldHelperContext from "../../../../context/FieldHelperContext"

const SQLFunction = ({ onChange }) => {
    const { fieldHelperState } = useContext(fieldHelperContext)
    const options = [
        { "value": "GET_REF_NO", "label": "GET_REF_NO" }
    ]

    return <div className="mb-3">
        {options.map(opt => <p key={opt.value} className="m-0">
            <a href="/#"
                className="text-primary ms-3"
                data-event="sql-field-helper"
                data-return-event={fieldHelperState.returnEvent}
                data-type={fieldHelperState.type}
                data-field={fieldHelperState.field}
                data-tag={opt.value}
                onClick={(e) => onChange({e})}>{opt.label}</a>
        </p>)}
    </div>
}
export default SQLFunction