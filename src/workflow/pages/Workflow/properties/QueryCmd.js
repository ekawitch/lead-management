import React, { useState } from 'react'
import { getDefaultFieldType } from '../../../service/NodeService'
import { renderTemplateTag } from '../../../service/TemplateService'
import template from '../../../payload/template'
import NavTab from '../../../components/NavTab'
import { preventDefault } from '../../../service/EventService'
import { getCreateNodeOracleStagingCmd, getNodeQueryCmd } from '../../../service/QueryService'
import { useContext } from 'react'
import workflowContext from '../../../context/WorkflowContext'

export default function QueryCmd({
    sqlInfo,
    node
}) {
    const { workflowState } = useContext(workflowContext)
    const tabs = [
        { value: "staging", label: "Create Staging" },
        { value: "select", label: "Select" }
    ]
    const [state, setState] = useState({ isCopied: false, tab: tabs[0].value })
    let cmdRslt = getNodeQueryCmd(sqlInfo, workflowState.nodes, node)
    cmdRslt = renderTemplateTag(cmdRslt, template, sqlInfo)

    const idxAllFields = {}
    getDefaultFieldType(node, workflowState.nodes, idxAllFields, sqlInfo)
    const tableRslt = getCreateNodeOracleStagingCmd(workflowState.nodes, sqlInfo, node, idxAllFields)

    const onQueryCmdChange = (e) => {
        preventDefault(e, "A")

        const currentTarget = e.currentTarget
        const name = currentTarget ? currentTarget.name : ""
        let curState

        if (name === "copy") {
            navigator.clipboard.writeText(state.tab === "select" ? cmdRslt : tableRslt);
            curState = { ...state, isCopied: true }
        } else {
            curState = { ...state, tab: e.value }
        }

        if (curState) {
            setState({ ...curState })
            if (curState.isCopied) {
                setTimeout(() => {
                    setState({ ...curState, isCopied: false })
                }, 1000)
            }
        }
    }

    return <>
        <p className="header3 mt-5">
            SQL Command
            {state.isCopied && <strong className="text-success ms-2 font-70pc">Copied</strong>}
            {!state.isCopied && <a href="/#"
                name="copy"
                className="text-secondary ms-2 font-70pc" onClick={(e) => onQueryCmdChange(e)}>Copy</a>}
        </p>

        <NavTab
            tabs={tabs}
            value={state.tab}
            callback={onQueryCmdChange}
            navClass="mb-3">
        </NavTab>
        <pre className="p-3 mt-3 mb-2 bg-info text-white rounded">
            {state.tab === "select" ? cmdRslt : tableRslt}
        </pre>
    </>
}