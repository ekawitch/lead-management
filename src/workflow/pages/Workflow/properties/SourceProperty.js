import React from 'react'
import { useContext } from 'react'
import workflowContext from '../../../context/WorkflowContext'
import { expandChild } from '../../../service/NodeService'
import { getNextSeq } from '../../../service/ObjectService'
import QueryWhere, { onWhereChange } from './Query/QueryWhere'

const SourceProperty = ({
    tree,
    node,
    onChange,
}) => {
    const { workflowState } = useContext(workflowContext)
    let parent
    let validNodes = workflowState.nodes.filter(ele => {
        let rslt = false
        try {
            rslt = ele.code !== node.parent && tree[ele.code].level < tree[node.code].level
        } catch (err) {
            console.log([err, node, ele])
        }

        return rslt
    })

    if (node.parentLabel) {
        if (node.parent) {
            parent = expandChild(validNodes).filter(ele => ele.code === node.parent)[0]
        }
    }

    return <>
        {node.parentLabel && <><div className="form-group row mt-3">
            <label className="col-sm-4 col-form-label">{node.parentLabel}</label>
        </div>
            <div className="form-check">
                <input type="checkbox"
                    className="form-check-source me-1"
                    checked={true}
                    readOnly={true}></input>
                <label className="form-check-label">
                    <strong>{parent.code}</strong> {parent.description}
                </label>
            </div>
        </>}
        {Array.isArray(validNodes) && validNodes.length > 0 && <>
            <div className="form-group row mt-3">
                <label className="col-sm-4 col-form-label">{node.sourceLabel ? node.sourceLabel : "Source"}</label>
            </div>

            {validNodes.map(ele => {
                const source = Array.isArray(node.sources) && node.sources.filter(src => src.code === ele.code)
                const isChecked = source.length > 0

                return <div key={[node.code, ele.code]} className="form-check">
                    <input className="form-check-source me-1"
                        type="checkbox"
                        id={"nodeSource" + ele.code}
                        data-event="node-source"
                        readOnly={true}
                        value={ele.code}
                        onChange={(e) => onChange({ e })}
                        checked={isChecked}></input>
                    <label className="form-check-label" htmlFor={"nodeSource" + ele.code}>
                        <strong>{ele.code}</strong> {ele.description}
                    </label>

                    {isChecked && <QueryWhere
                        node={node}
                        element={source[0]}
                        onChange={onChange}
                        event="source-condition"
                        header="Condition"
                        headerClass="header3"
                        leftStaging={node.parent}
                        rightStaging={ele.code}
                        sourceCode={ele.code}>
                    </QueryWhere>}
                </div>
            })}
        </>}
    </>
}

const onSourcePropertyChange = (e, node, currentTarget) => {
    const dataset = currentTarget.dataset
    const event = dataset.event

    if (event === "node-source") {
        const value = currentTarget.value
        node.sources = node.sources ? node.sources : []
        const isExisting = node.sources.filter(ele => ele.code === value).length > 0

        if (isExisting) {
            node.sources = node.sources.filter(ele => ele.code !== value)
        } else {
            const nextSeq = getNextSeq(node.sources, "seq")
            node.sources.push({ code: value, seq: nextSeq, conditions: [] })
        }
    } else if (event === "source-condition") {
        const source = node.sources.filter(ele => ele.code === dataset.sourceCode)[0]
        onWhereChange(source, currentTarget)
    }
}

export default SourceProperty
export { onSourcePropertyChange }