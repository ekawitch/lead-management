import React, { Fragment } from 'react'
import BlockComponent, { ComponentRegistry } from './components/BlockComponent'

const BlockList = ({ handleMouse, focus }) => {
    return <div id="blocklist">
        {Object.keys(ComponentRegistry).map(nm => {
            const props = {
                ...ComponentRegistry[nm],
                handleMouse,
                focus
            }
            return <Fragment key={nm}>
                <BlockComponent {...props}></BlockComponent>
                {focus.action === "move" &&
                    focus.shape === props.shape &&
                    focus.code && <BlockComponent {...props} code={"new"} ></BlockComponent>}
            </Fragment>
        }
        )}
    </div>
}

export default BlockList