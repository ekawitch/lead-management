import React, { Fragment, useContext } from 'react'
import mouseContext from '../../../context/MouseContext'
import BlockComponent from './BlockComponent'

const BlockLevel = ({
    validNodes,
    tree,
    level,
    handleMouse,
    maxSpace
}) => {
    const { mouseState } = useContext(mouseContext)
    const BlockLevelStyle = { height: ((126 * maxSpace) + 50) + "px" }
    const htmlClasses = ["block-level"]
    const myRef = document.getElementById("level_" + level)

    if (mouseState.level === level) {
        htmlClasses.push("level-focus");
    }

    if (myRef) {
        const myPos = myRef.getBoundingClientRect()
        let isOnMe = myPos.x <= mouseState.x && mouseState.x <= (myPos.x + myPos.width)
        isOnMe = isOnMe && (myPos.y <= mouseState.y && mouseState.y <= (myPos.y + myPos.height))

        const canvasPos = document.getElementById("canvas").getBoundingClientRect()
        let isInCanvas = canvasPos.x <= mouseState.x && mouseState.x <= (canvasPos.x + canvasPos.width)
        isInCanvas = isInCanvas && (canvasPos.y <= mouseState.y && mouseState.y <= (canvasPos.y + canvasPos.height))


        if (isInCanvas && !mouseState.parent) {
            if (isOnMe) {
                if (mouseState.level === undefined) {
                    // handleMouse({ type: "mouseonlevel", level: level })
                }
            }
        }

        if (mouseState.parent || !isOnMe) {
            if (mouseState.level === level) {
                // handleMouse({ type: "mouseonlevel", level: undefined })
            }
        }
    }

    return <div id={"level_" + level} className={htmlClasses.join(" ")} data-level={level} style={BlockLevelStyle}>{
        validNodes.map(ele => {
            const offset = tree[ele.code].offset
            const relOffset = tree[ele.code].relOffset
            const space = tree[ele.code].space

            return <Fragment key={ele.code}>
                <BlockComponent {...ele}
                    handleMouse={handleMouse}
                    level={level}
                    offset={offset}
                    relOffset={relOffset}
                    space={space}
                    focus={mouseState}
                    tree={tree}></BlockComponent>
            </Fragment>
        })
    }</div>
}

export default BlockLevel