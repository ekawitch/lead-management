import React from 'react'
import { COMPONENT_HEIGHT, COMPONENT_SPACE_X, COMPONENT_SPACE_Y, COMPONENT_WIDTH } from './BlockComponent'

const CONNECTOR_WIDTH = 60
const SELF_CONNECTOR_WIDTH = 60
const calcComponentConnector = (from, to) => {
    const fromPos = {
        x: (from.level - 1) * (COMPONENT_WIDTH + COMPONENT_SPACE_X),
        y: from.offset * (COMPONENT_HEIGHT + COMPONENT_SPACE_Y)
        // y: (from.offset + ((from.space - 1) / 2)) * (COMPONENT_HEIGHT + COMPONENT_SPACE_Y)
    }//!$from ? { x: 0, y: 0 } : $from.getBoundingClientRect()
    const toPos = {
        x: (to.level - 1) * (COMPONENT_WIDTH + COMPONENT_SPACE_X),
        y: to.offset * (COMPONENT_HEIGHT + COMPONENT_SPACE_Y)
        // y: (to.offset + ((to.space - 1) / 2)) * (COMPONENT_HEIGHT + COMPONENT_SPACE_Y)
    }//!$to ? { x: 0, y: 0 } : $to.getBoundingClientRect()
    const linePath = []
    const arrowPath = []
    const fromOffset = from.offset //+ ((from.space - 1) / 2)
    const toOffset = to.offset //+ ((to.space - 1) / 2)
    let left = fromPos.x < toPos.x ? fromPos.x : toPos.x//canvasPos.x - fromPos.x
    let top = fromPos.y < toPos.y ? fromPos.y : toPos.y //canvasPos.y - (fromPos.y < toPos.y ? fromPos.y : toPos.y)

    const lvDiff = Math.abs(from.level - to.level)
    const style = {
        left: left + "px",
        top: top + "px",
        width: ((lvDiff > 0 ? CONNECTOR_WIDTH : 0) + (lvDiff > 1 ? (lvDiff - 1) * COMPONENT_WIDTH : 0)) + "px"
    }

    // const baseOffset = from.offset < to.offset ? from.offset : to.offset
    const obj1X = COMPONENT_WIDTH - (CONNECTOR_WIDTH / 2)
    const obj12X = obj1X + (CONNECTOR_WIDTH / 2) + ((to.level - from.level - 1) * COMPONENT_WIDTH)
    const obj2X = obj1X + CONNECTOR_WIDTH + ((to.level - from.level - 1) * COMPONENT_WIDTH)
    const obj1Y = ((fromOffset < toOffset ? 0 : (fromOffset - toOffset)) + 0.5) * (COMPONENT_HEIGHT + COMPONENT_SPACE_Y)
    const obj2Y = ((toOffset < fromOffset ? 0 : (toOffset - fromOffset)) + 0.5) * (COMPONENT_HEIGHT + COMPONENT_SPACE_Y)
    linePath.push("M" + obj1X + " " + obj1Y)
    linePath.push("L" + obj12X + " " + obj1Y)
    linePath.push("L" + obj12X + " " + obj2Y)
    linePath.push("L" + obj2X + " " + obj2Y)

    // M0 75H10L5 80L0 75Z
    arrowPath.push("M" + (obj2X - 10) + " " + (obj2Y - 5))
    // arrowPath.push("H10")
    arrowPath.push("L" + (obj2X - 10) + " " + (obj2Y + 5))
    arrowPath.push("L" + (obj2X - 1) + " " + (obj2Y))
    arrowPath.push("L" + (obj2X - 10) + " " + (obj2Y - 5) + "Z")

    return { style, arrowPath, linePath }
}
const calcSelfConnector = (from, toIdx) => {
    const linePath = []
    const arrowPath = []
    // let left = (from.level - 1) * (COMPONENT_WIDTH + COMPONENT_SPACE_X)
    // let top = from.offset * (COMPONENT_HEIGHT + COMPONENT_SPACE_Y)

    const style = {
        // left: left + "px",
        // top: top + "px"
        left: "0px",
        top: "0px"
    }

    const obj1X = (COMPONENT_WIDTH / 2) - (SELF_CONNECTOR_WIDTH / 2)
    const obj12X = obj1X + (SELF_CONNECTOR_WIDTH / 2)
    const obj2X = obj1X + SELF_CONNECTOR_WIDTH
    const obj1Y = (0.5) * (COMPONENT_HEIGHT + COMPONENT_SPACE_Y)
    const obj2Y = obj1Y + (toIdx * (COMPONENT_HEIGHT + COMPONENT_SPACE_Y))

    linePath.push("M" + obj1X + " " + obj1Y)
    linePath.push("L" + obj12X + " " + obj1Y)
    linePath.push("L" + obj12X + " " + obj2Y)
    linePath.push("L" + obj2X + " " + obj2Y)

    arrowPath.push("M" + (obj2X - 10) + " " + (obj2Y - 5))
    arrowPath.push("L" + (obj2X - 10) + " " + (obj2Y + 5))
    arrowPath.push("L" + (obj2X - 1) + " " + (obj2Y))
    arrowPath.push("L" + (obj2X - 10) + " " + (obj2Y - 5) + "Z")

    return { style, arrowPath, linePath }
}

const Connector = ({ from, to, toIdx }) => {
    let connParam

    if (to) {
        connParam = calcComponentConnector(from, to)
    } else {
        connParam = calcSelfConnector(from, toIdx)
    }

    // console.log([from, to, COMPONENT_HEIGHT, COMPONENT_SPACE_Y])
    return <div className="arrowblock" style={connParam.style}>
        <svg preserveAspectRatio="none" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d={connParam.linePath.join("")} stroke="#C5CCD0" strokeWidth="2px"></path>
            <path d={connParam.arrowPath.join("")} fill="#C5CCD0"></path>
        </svg>
    </div>
}

export {
    Connector
}