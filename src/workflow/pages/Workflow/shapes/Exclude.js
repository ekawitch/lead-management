const Props = {
    shape: "Exclude",
    title: "Exclude",
    description: "Exclude",
    icon: ["fa", "fa-xmark-circle"],
    image: "excluding.png",
    isSegmentationReadOnly: true,
    segmentations: [],
    isLegacyField: true,
    datasource: "STAGING",
    datasourceReadOnly: true,
    sources: [],
    parentLabel: "Exclude",
    sourceLabel: "from",
    // segmentationPropertyHeader: "Result",
    oracleNo: 1,
    isIgnoreSegmentationPrefix: true,
    isStagingProperty: true
}

const onExcludeAdded = (node) => {
    if (node.shape === "Exclude") {
        node.segmentations.push({ seq: 1, value: "Qualified" })
        node.segmentations.push({ seq: 2, value: "Disqualified" })
    }
}

const getTabs = () => {
    return [
        { value: "source", label: "Source" },
        { value: "segmentation", label: "Result" }
    ]
}

export {
    Props,
    getTabs,
    onExcludeAdded
}