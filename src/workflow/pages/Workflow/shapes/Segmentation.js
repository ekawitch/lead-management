const Props = {
    shape: "Segmentation",
    title: "Segmentation",
    description: "Segmentation",
    icon: ["fa", "fa-layer-group"],
    image: "segmentation.png",
    datasource: "STAGING",
    datasourceReadOnly: true,
    segmentations: [],
    isLegacyField: true,
    segmentType: "PERCENT",
    segmentPrefix: "",
    fields: [],
    queryCmd: false,
    oracleNo: 1,
    isStagingProperty: true
}

const getTabs = () => {
    return [
        { value: "segmentation", label: "Result" }
    ]
}

export {
    Props,
    getTabs
}