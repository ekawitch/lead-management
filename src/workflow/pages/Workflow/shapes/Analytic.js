const Props = {
    shape: "Analytic",
    title: "Analytic",
    description: "Analytic",
    image: "analytic.png",
    datasource: "STAGING",
    datasourceReadOnly: true,
    fields: [],
    conditions: [],
    groupBy: [],
    queryCmd: true,
    oracleNo: 1,
    isQueryProperty: true
}

const getTabs = () => {
    return [
        { value: "field", label: "Field" },
        { value: "where", label: "Where" },
        { value: "group_by", label: "Group By" }
    ]
}

export {
    Props,
    getTabs
}