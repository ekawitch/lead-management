const Props = {
    shape: "Notification",
    title: "Notification",
    description: "Notification",
    image: "notification.png",
    sources: [],
    parentLabel: "Notification",
    sourceLabel: "and",
    notifications: [],
    oracleNo: 1,
    isStagingProperty: true
}

const getTabs = () => {
    return [
        { value: "source", label: "Source" },
        { value: "notification", label: "Notification" }
    ]
}

export {
    Props,
    getTabs
}