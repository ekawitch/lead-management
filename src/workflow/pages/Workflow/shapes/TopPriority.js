const Props = {
    shape: "TopPriority",
    title: "Top Priority",
    datasource: "STAGING",
    datasourceReadOnly: true,
    description: "TopPriority",
    icon: ["fa", "fa-xmark-circle"],
    image: "top_priority.png",
    partitionBy: [],
    tables: [],
    fields: [],
    conditions: [],
    orderBy: [],
    // fieldFunctions: ["AddFunction"],
    queryCmd: true,
    oracleNo: 1,
    isQueryProperty: true
}

const getTabs = () => {
    return [
        { value: "table", label: "Table" },
        { value: "field", label: "Field" },
        { value: "partition_by", label: "Partition By" },
        { value: "order_by", label: "Partition Order By" },
        { value: "where", label: "Where" }
    ]
}

export {
    Props,
    getTabs
}