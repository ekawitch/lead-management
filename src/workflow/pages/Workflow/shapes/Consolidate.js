const Props = {
    shape: "Consolidate",
    title: "Consolidate",
    description: "Consolidate",
    image: "consolidate.png",
    isAutoField: true,
    oracleNo: 1,
    staging: ""
}

export {
    Props
}