const Props = {
    shape: "Populate",
    title: "Populate",
    description: "Populate",
    // icon: ["fa", "fa-layer-group"],
    image: "populate.png",
    datasource: "STAGING",
    datasourceReadOnly: true,
    fields: [],
    isDefaultField: true,
    queryCmd: true,
    oracleNo: 1,
    campaignName: ""
}

export {
    Props
}