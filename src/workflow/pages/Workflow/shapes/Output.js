const Props = {
    parentSegment: 0,
    shape: "Output",
    title: "Output",
    description: "Output",
    image: "output.png",
    isAutoField: true,
    oracleNo: 1,
    staging: "",
    queryCmd: true
}

export {
    Props
}