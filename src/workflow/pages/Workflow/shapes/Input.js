const Props = {
    shape: "Input",
    title: "Input",
    description: "Input",
    image: "input.png",
    datasource: "CRMWORK",
    datasourceReadOnly: true,
    fields:[],
    tables: [],
    conditions: [],
    tableLimit: 1,
    isAutoField: true,
    queryCmd: true,
    oracleNo: 1,
    isQueryProperty: true,
    isTemplate: false
}

const getTabs = () => {
    return [
        { value: "table", label: "Table" },
        { value: "field", label: "Field" },
        { value: "where", label: "Where" }
    ]
}

export {
    Props,
    getTabs
}