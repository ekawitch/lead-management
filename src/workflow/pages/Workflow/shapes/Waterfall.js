const Props = {
    shape: "Waterfall",
    title: "Waterfall",
    description: "Waterfall",
    icon: ["fa", "fa-diagram-project"],
    image: "waterfall.png",
    datasource: "CRMWORK",
    tables: [],
    fields: [],
    conditions: [],
    groupBy: [],
    orderBy: [],
    queryCmd: true,
    oracleNo: 1,
    isSqlVisualizer: true,
    isQueryProperty: true,
    isTemplate: false
}
const onWaterfallAdded = (node) => {
    if (node.parent) {
        node.datasource = "STAGING"
    }
}
const getTabs = () => {
    return [
        { value: "table", label: "Table" },
        { value: "field", label: "Field" },
        { value: "where", label: "Where" },
        { value: "group_by", label: "Group By" },
        { value: "order_by", label: "Order By" }
    ]
}
export {
    Props,
    getTabs,
    onWaterfallAdded
}