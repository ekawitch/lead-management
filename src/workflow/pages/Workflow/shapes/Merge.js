const Props = {
    shape: "Merge",
    title: "Merge",
    description: "Merge",
    icon: ["fa", "fa-code-merge"],
    image: "merge.png",
    parentLabel: "Merge",
    sourceLabel: "with",
    conditions: [],
    isLegacyField: true,
    datasource: "STAGING",
    datasourceReadOnly: true,
    sources: [],
    queryCmd: true,
    oracleNo: 1,
    isStagingProperty: true
}

const getTabs = () => {
    return [
        { value: "source", label: "Source" }
    ]
}

export {
    Props,
    getTabs
}