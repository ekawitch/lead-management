import React from 'react';
import CommonProperty from './properties/CommonProperty';
import QueryCmd from './properties/QueryCmd';
import FieldHelper from './properties/FieldHelper/FieldHelper';
import QueryProperty from './properties/QueryProperty';
import SQLProperty from './properties/SQLProperty';
import StagingProperty from './properties/StagingProperty';

export default function Properties({
    sqlInfo,
    tree,
    onChange,
    node
}) {
    const nodeKeys = Object.keys(node)

    return (<>
        <div id="properties" className="expanded">
            <div className="row">
                <div className="col-12 text-end">
                    <a href="/#" className="text-info font-70pc" data-event="make-a-copy" data-seq={node.seq} onClick={(e) => onChange({ e })}>make a copy</a>
                </div>
            </div>
            <h2 className="header2 mt-5">
                Properties
                <div id="close" data-event="node-property-close" onClick={(e) => onChange({ e })}>
                    <img alt="close" src="assets/close.svg"></img>
                </div>
            </h2>


            <CommonProperty
                node={node}
                onChange={onChange}>
            </CommonProperty>

            {(node.isQueryProperty) && <QueryProperty
                sqlInfo={sqlInfo}
                onChange={onChange}
                node={node}
                isShow={nodeKeys.indexOf("datasource") === -1 || node.datasource !== ""}>
            </QueryProperty>}

            {(node.isStagingProperty) && <StagingProperty
                tree={tree}
                sqlInfo={sqlInfo}
                onChange={onChange}
                node={node}>
            </StagingProperty>}


            {nodeKeys.indexOf("sql") > -1 && <SQLProperty node={node} onChange={onChange}></SQLProperty>}
            {node.queryCmd && <QueryCmd
                sqlInfo={sqlInfo}
                node={node}></QueryCmd>}
        </div>

        <FieldHelper
            sqlInfo={sqlInfo}
            onChange={onChange}
            node={node}>
        </FieldHelper>
    </>)
}
