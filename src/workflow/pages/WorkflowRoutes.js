import { useContext, useEffect } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import ImportData from "./ImportData/ImportData";
import Workflow from "./Workflow/Workflow";
import categoryContext from '../context/CategoryContext';
import { getCategories } from '../service/API/CategoryAPIService';
import userContext from "../context/UserContext";
import { getUsers } from "../service/API/UserAPIService";

export default function WorkflowRoutes() {
    const { setCategoryState } = useContext(categoryContext)
    const { setUserState } = useContext(userContext)

    useEffect(() => {
        const rslt = getUsers()
        rslt.then(rslt => {
            setUserState({ ...rslt })
        })
            .catch(err => console.log(err))
    }, [setUserState])

    useEffect(() => {
        const rslt = getCategories()
        rslt.then(rslt => {
            setCategoryState({ ...rslt })
        })
            .catch(err => console.log(err))
    }, [setCategoryState])

    return <>
        <BrowserRouter basename={process.env.PUBLIC_URL}>
            <Routes>
                <Route path="/" element={<Workflow />}></Route>
                <Route path="/importData" element={<ImportData />}></Route>
            </Routes>
        </BrowserRouter>
    </>
}