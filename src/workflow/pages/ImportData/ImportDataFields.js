import GridView from "../../components/GridView/GridView";
import ImportDataForm from "./ImportDataForm";

export default function ImportDataFields({ fields }) {

    const getTD = (obj, fld, onChange) => {
        if (fld === "sequence") {
            return obj[fld]
        } else if (fld === "move") {
            return <div className="input-group mb-3">
                <div className="input-group-prepend">
                    <button className="btn btn-secondary">
                        <i className="fa fa-angle-up"></i>
                    </button>
                </div>
                <input type="text"
                    className="form-control text-end w-25"
                    value={obj.sequence}
                    onChange={(e) => onChange(e)}></input>
                <div className="input-group-append">
                    <button className="btn btn-secondary">
                        <i className="fa fa-angle-down"></i>
                    </button>
                    <button className="btn btn-secondary">
                        <i className="fa fa-trash"></i>
                    </button>
                </div>
            </div>
        }
        else {
            return <input data-seq={fld.sequence}
                className="form-control"
                value={obj[fld]}
                onChange={(e) => onChange(e)}></input>
        }
    }

    const headers = [
        { header: "#", field: "sequence", getTD: getTD },
        { header: "Field Name", field: "fieldName", getTD: getTD },
        { header: "Date Type", field: "dataType", getTD: getTD },
        { header: "Data Format", field: "format", getTD: getTD },
        { header: "Data Length", field: "dataLength", getTD: getTD },
        { header: "Precision", field: "dataPrecision", getTD: getTD },
        { header: "Move Rows", field: "move", getTD: getTD },
    ]

    return <div className="row pd-5">
        <div className="col-12">
            <GridView headers={headers}
                dataRows={fields}
                fieldID="sequence"
                editForm={ImportDataForm}
                tableClass="table border border-secondary mt-3"
                ignoreGridViewControl={true}
                ignorePagination={true}></GridView>
        </div>
    </div>
}