import { useEffect } from "react";
import { useState } from "react";
import { callGetImportDataEdit } from "../../service/API/ImportDataAPIService";
import { cloneObject } from "../../service/ObjectService";
import ImportDataFields from "./ImportDataFields";

const BUTTONS = [
    { action: "execute", icon: "fa-play", text: "Execute" },
    { action: "save", icon: "fa-floppy-o", text: "Save" },
    { action: "saveas", icon: "fa-files-o", text: "Save As" },
    { action: "addcolumn", icon: "fa-plus", text: "Add Column" },
    { action: "clear", icon: "fa-trash", text: "Clear" },
    { action: "reload", icon: "fa-refresh", text: "Reload" },
    { action: "close", icon: "fa-close", text: "Close" },
]

export default function ImportDataForm({
    tx
}) {
    const [form, setForm] = useState({
        "importId": "",
        "importDesc": "",
        "sourcetable": "",
        "sourcetype": "",
        "targettable": "",
        "delimiter": "",
        "headingflg": "",
        "createDt": "",
        "Owner": "",
        "creator": "",
        "updateDt": "",
        "lastUpdateUser": "",
        importFields: []
    })

    const onChange = (e) => {
        const currentTarget = e.currentTarget
        const id = currentTarget.id
        const val = currentTarget.value

        form[id] = val
        setForm({ ...form })
    }

    useEffect(() => {
        callGetImportDataEdit().then(rslt => {
            setForm(cloneObject(rslt.result))
        }).catch(err => {
            console.log(err)
        })
    }, [tx])

    return <>
        <form>
            <div className="row mt-3 mb-3">
                <div className="col-12 d-flex justify-content-end">
                    {BUTTONS.map(btn => <button key={btn.action}
                        className="btn btn-sm btn-secondary me-2">
                        <i className={"fa me-2 " + btn.icon}></i>
                        {btn.text}
                    </button>)}
                </div>
            </div>

            <div className="form-group row mb-3">
                <label htmlFor="importId" className="col-sm-1 col-form-label">ID</label>
                <div className="col-sm-3">
                    <input type="text"
                        className="form-control"
                        id="importId"
                        value={form.importId}
                        onChange={(e) => onChange(e)}></input>
                </div>

                <label htmlFor="importId" className="col-sm-1 col-form-label">Destination</label>
                <div className="col-sm-3">
                    <div className="input-group">
                        <input type="text"
                            className="form-control"
                            id="inlineFormInputGroup"
                            value={form.inlineFormInputGroup}
                            onChange={(e) => onChange(e)}></input>
                        <div className="input-group-append">
                            <button className="btn btn-outline-secondary">
                                <i className="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <div className="form-group row mb-3">
                <label htmlFor="importDesc" className="col-sm-1 col-form-label">Description</label>
                <div className="col-sm-3">
                    <input type="text"
                        className="form-control"
                        id="importDesc"
                        value={form.importDesc}
                        onChange={(e) => onChange(e)}></input>
                </div>

                <label htmlFor="tabDelimiter" className="col-sm-1 col-form-label text-end">Tab Delimiter</label>
                <div className="col-sm-1">
                    <div className="input-group-text bg-transparent border-0">
                        <input type="checkbox"
                            id="tabDelimiter"
                            checked={form.tabDelimiter === true}
                            onChange={(e) => onChange(e)}></input>
                    </div>
                </div>

                <label htmlFor="hedingFLG" className="col-sm-1 col-form-label text-end">Inc. Header</label>
                <div className="col-sm-1">
                    <div className="input-group-text bg-transparent border-0">
                        <input type="checkbox"
                            id="hedingFLG"
                            checked={form.hedingFLG === true}
                            onChange={(e) => onChange(e)}></input>
                    </div>
                </div>
            </div>


            <div className="form-group row mb-3">
                <label htmlFor="importId" className="col-sm-1 col-form-label">File Type</label>
                <div className="col-sm-3">
                    <select className="form-control"
                        value={form.tabDelimiter}
                        onChange={(e) => onChange(e)}>
                        <option value=""></option>
                    </select>
                </div>

                <label htmlFor="delimeter" className="col-sm-1 col-form-label">Delimited by</label>
                <div className="col-sm-3">
                    <input type="text"
                        className="form-control"
                        id="delimeter"
                        value={form.delimeter}
                        onChange={(e) => onChange(e)}></input>
                </div>

                <label htmlFor="uploadFile" className="col-sm-1 col-form-label">File name</label>
                <div className="col-sm-3">
                    <input type="file"
                        className="form-control"
                        id="uploadFile"
                        onChange={(e) => onChange(e)}></input>
                </div>
            </div>
        </form>

        <form>
            <ImportDataFields fields={form.importFields}></ImportDataFields>
        </form>
    </>
}