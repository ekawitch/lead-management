import GridView from "../../components/GridView/GridView";
import Navbar from "../../components/Navbar/Navbar";
import { URL_API_IMPORT_DATA_SEARCH } from "../../service/API/APIConfig";
import ImportDataForm from "./ImportDataForm";

export default function ImportData() {
    const headers = [
        { header: "ID", field: "importId" },
        { header: "Description", field: "importDesc" },
        { header: "Source Table", field: "sourcetable" },
        { header: "Source Type", field: "sourcetype" },
        { header: "Target Table", field: "targettable" },
        { header: "Delimiter", field: "delimiter" },
        { header: "Heading Flag", field: "headingflg" },
        { header: "Create Date", field: "createDt" },
        { header: "Owner", field: "creator" },
        { header: "Date of Updation", field: "updateDt" },
        { header: "Updated by", field: "lastUpdateUser" },
    ]

    return <>

        <Navbar></Navbar>

        <div className="container-fluid overflow-auto">
            <div className="row pd-5">
                <div className="col-12">
                    <GridView headers={headers}
                        urlSearch={URL_API_IMPORT_DATA_SEARCH}
                        fieldID="importId"
                        EditForm={ImportDataForm}></GridView>
                </div>
            </div>
        </div>
    </>
}