import { useContext } from "react"
import { useState } from "react"
import workflowContext from "../../context/WorkflowContext"
import { preventDefault } from "../../service/EventService"

const TreeView = ({
    sqlInfo,
    schema,
    onChange
}) => {
    const { workflowState } = useContext(workflowContext)
    const iniMystate = { more: [], tables: [] }
    const [mystate, setMystate] = useState({ ...iniMystate })

    const onTreeChange = (e) => {
        preventDefault(e, "A")

        const currentTarget = e.currentTarget
        const dataset = currentTarget.dataset
        const event = dataset.event

        if (event === "table-more") {
            mystate.more.push(dataset.table)
        } else if (event === "table-less") {
            mystate.more = mystate.more.filter(ele => ele !== dataset.table)
        }

        setMystate({ ...mystate })
    }

    return <div className="row">
        <div className="col-12">
            {sqlInfo.tables.length > 0 && <ul className="list-unstyled">
                {sqlInfo.tables.filter(tbl => tbl.schema === schema)
                    .sort((tbl1, tbl2) => tbl1.name.localeCompare(tbl2.name))
                    .map(tbl => {
                        const isTblMore = mystate.more.indexOf(tbl.schema + "." + tbl.name) > -1

                        return <li key={tbl.name}>
                            {tbl.name}
                            <a href="/#"
                                className="ms-2 text-success font-70pc"
                                data-event={isTblMore ? "table-less" : "table-more"}
                                data-table={tbl.schema + "." + tbl.name}
                                onClick={(e) => onTreeChange(e)}>
                                {isTblMore && <i className="fa fa-angle-up"></i>}
                                {!isTblMore && <i className="fa fa-angle-down"></i>}
                            </a>

                            <a href="/#"
                                name="new"
                                className="ms-2 text-success font-70pc"
                                data-event="node-table"
                                data-table={tbl.schema + "." + tbl.name}
                                onClick={(e) => onChange({ e, sqlInfo, nodes: workflowState.nodes })}>
                                <i className="fa fa-plus"></i>
                            </a>

                            {isTblMore && <ul>
                                {tbl.properties.map(prop => <li key={prop.name}>{prop.name}</li>)}
                            </ul>}
                        </li>
                    })}
            </ul>}
        </div>
    </div>
}

export default TreeView