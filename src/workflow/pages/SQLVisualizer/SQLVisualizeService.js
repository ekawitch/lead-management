const getRelations = (node) => {
    return node.tables.map(tbl => {
        return tbl.conditions ? tbl.conditions.map(condi => {
            return { ...condi, tableSeq: tbl.seq }
        }) : []
    }).reduce((acc, cur) => [...acc, ...cur], [])
}

const getVisualFieldId = (alias, property) => {
    return "visual_field_" + alias + "___" + property
}

const getRelationDrag = (rel, offset) => {
    if (rel.left && rel.right) {
        let l = rel.left.split(".")
        let r = rel.right.split(".")

        if (l.length === 3) {
            l = [l[0] + "." + l[1], l[2]]
        }
        if (r.length === 3) {
            r = [r[0] + "." + r[1], r[2]]
        }

        if (l.length === 2 && r.length === 2) {
            const lfield = document.getElementById(getVisualFieldId(l[0], l[1]))
            const rfield = document.getElementById(getVisualFieldId(r[0], r[1]))

            if (lfield && rfield) {
                let lpos = lfield.getElementsByClassName("join-l")[0].getBoundingClientRect()
                let rpos = rfield.getElementsByClassName("join-l")[0].getBoundingClientRect()

                if (lpos.left < rpos.left) {
                    lpos = lfield.getElementsByClassName("join-r")[0].getBoundingClientRect()
                } else {
                    rpos = rfield.getElementsByClassName("join-r")[0].getBoundingClientRect()
                }

                return {
                    from: {
                        x: lpos.left - (offset ? offset.x : 0),
                        y: lpos.top - (offset ? offset.y : 0)
                    },
                    to: {
                        x: rpos.left - (offset ? offset.x : 0),
                        y: rpos.top - (offset ? offset.y : 0)
                    }
                }
            }
        }
    }

    return undefined
}

export {
    getRelations,
    getVisualFieldId,
    getRelationDrag
}