import { CONNECTOR_OPERATOR_MAPPING, WHERE_OPERATOR_MAPPING } from "../../config/Operator"
import { getVisualFieldId } from "./SQLVisualizeService"

const TableView = ({
    node,
    table,
    added,
    dragfrom,
    dragto,
    onChange
}) => {
    const addedFields = node.fields.reduce((acc, cur) => {
        acc[cur.name] = cur

        return acc
    }, {})

    return <div className="d-inline-block width-180">
        <ul className="list-group">
            <li className="list-group-item list-group-item-dark">
                <strong className="me-2">Table</strong>
                {added.alias}
                {/* {table.name}
                {added && added.alias && added.alias !== table.name && <strong className="ms-2">[{added.alias}]</strong>} */}

                <a href="/#"
                    name="remove"
                    className="float-right text-danger font-60pc"
                    data-event="node-table"
                    data-seq={added.seq}
                    onClick={(e) => onChange({ e })}>
                    <i className="fa fa-close"></i>
                </a>
            </li>
            {table.properties.map(prop => {
                const fieldVal = added.alias + "." + prop.name
                const isAddedField = addedFields[fieldVal] !== undefined
                const cls = ["list-group-item data-table-field"]
                const conditions = added.conditions.filter(condi => condi.right === fieldVal)

                if (dragfrom.table) {
                    if (dragfrom.table === table.name &&
                        dragfrom.tableAlias === added.alias &&
                        dragfrom.property === prop.name) {
                        cls.push("list-group-item-warning")
                    }
                }

                if (dragto.table) {
                    if (dragto.table === table.name &&
                        dragto.tableAlias === added.alias &&
                        dragto.property === prop.name) {
                        cls.push("list-group-item-warning")
                    }
                }

                return <li key={prop.name}
                    id={getVisualFieldId(added.alias, prop.name)}
                    className={cls.join(" ")}
                    data-table={table.name}
                    data-table-seq={added.seq}
                    data-table-alias={added.alias}
                    data-property={prop.name}>
                    <div className="join-l"></div>
                    <div className="join-r"></div>

                    <input type="checkbox"
                        className="form-check-source me-1"
                        data-event="node-field"
                        data-table={added.alias}
                        data-property={prop.name}
                        data-seq={isAddedField ? addedFields[fieldVal].seq : undefined}
                        name={isAddedField ? "remove" : "new"}
                        value={prop.name}
                        onChange={(e) => onChange({ e })}
                        checked={isAddedField}
                        id={"SQL_VISUALIZE_FIELD_" + added.alias + "_" + prop.name}>
                    </input>
                    <label className="form-check-label"
                        htmlFor={"SQL_VISUALIZE_FIELD_" + added.alias + "_" + prop.name}>
                        {prop.name}
                    </label>

                    {/* {conditions.map(condi => <TableViewCondition key={condi.seq} added={added} condi={condi}></TableViewCondition>)} */}
                    {conditions.map(condi => <div key={condi.seq} className="row font-60pc mt-2">
                        <div className="col-12">
                            <span className="ms-1">{condi.connector ? CONNECTOR_OPERATOR_MAPPING[condi.connector] : ""}</span>
                            <strong className="ms-1">{condi.left}</strong>
                            <span className="ms-1">{condi.operator ? WHERE_OPERATOR_MAPPING[condi.operator] : ""}</span>
                            <strong className="ms-1">{condi.right}</strong>

                            <a href="/#"
                                name="remove"
                                className="ms-2 text-danger font-60pc"
                                data-event="table-condition"
                                data-table-seq={added.seq}
                                data-seq={condi.seq}
                                onClick={(e) => onChange({ e })}>
                                <i className="fa fa-close"></i>
                            </a>
                        </div>
                    </div>)}
                </li>
            })}
        </ul>
    </div>
}

export default TableView