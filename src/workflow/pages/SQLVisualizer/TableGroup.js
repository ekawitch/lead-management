const TableGroup = ({
    children
}) => {
    return <div className="sql-table-group">{children}</div>
}
export default TableGroup