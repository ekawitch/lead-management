import { useState } from "react";
import FormButtons from "../../components/FormGroup/FormButtons";
import InputText, { INPUT_TYPE_TEXTAREA } from "../../components/FormGroup/InputText";
import ExpressionHelper from "../Workflow/properties/ExpressionHelper/ExpressionHelper";

export default function TemplateDetail({ template }) {
    const [frm, setFrm] = useState({ ...template, isFieldHelper: false })

    const onChange = (e) => {
        const currentTarget = e.currentTarget

        frm[currentTarget.name] = currentTarget.value
        setFrm({ ...frm })
    }

    const onExpressionHelperChange = (e) => {
        const currentTarget = e.currentTarget
        const name = currentTarget.name

        if (name === "close") {
            setFrm({ ...frm, isFieldHelper: false })
        }
    }

    const getExpressionLabel = () => {
        return <label htmlFor={"frmGrpInputexpression"}>Expression</label>
    }

    return <div className="row">
        <div className="col-12">
            <FormButtons></FormButtons>

            <form className="mt-3">
                <InputText name="code"
                    value={frm.code}
                    label="Template"
                    placeholder="Code"
                    onChange={onChange}
                ></InputText>
                <InputText name="name"
                    value={frm.name}
                    label="Name"
                    placeholder="Name"
                    onChange={onChange}
                ></InputText>
                <InputText name="description"
                    value={frm.description}
                    label="Description"
                    placeholder="Description"
                    onChange={onChange}
                ></InputText>
                <InputText type={INPUT_TYPE_TEXTAREA}
                    name="expression"
                    value={frm.expression}
                    getLabel={getExpressionLabel}
                    placeholder="expression"
                    onChange={onChange}
                ></InputText>
            </form>

            {frm.isFieldHelper === true && <ExpressionHelper onChange={onExpressionHelperChange}></ExpressionHelper>}
        </div>
    </div>
}