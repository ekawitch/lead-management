import { useState } from "react";
import { useContext } from "react";
import TreeView from "../../components/Treeview/TreeView";
import templateContext, { TEMPLATE_INI } from "../../context/TemplateContext";
import TemplateDetail from "./TemplateDetail";

// const TEMPLATE_FRM_INI = {}
const TEMPLATE_FRM_INI = {
    ...TEMPLATE_INI,
    parent: 1,
    order: 1,

}
export default function Template() {
    const [frm, setFrm] = useState({ ...TEMPLATE_FRM_INI })
    const { templateState } = useContext(templateContext)
    const onChange = (e) => {
        const currentTarget = e.currentTarget
        const dataset = currentTarget.dataset
        const parentId = parseInt(dataset.id)
        const nextOrder = templateState.values.filter(ele => ele.parent === parentId)
            .reduce((acc, cur) => {
                if (acc < cur.order) {
                    acc = cur.order
                }

                return acc
            }, 0) + 1
        const newItem = {
            ...TEMPLATE_INI,
            parent: parentId,
            order: nextOrder,

        }

        setFrm({ ...newItem })
    }

    const getTreeNode = (leaf) => {
        const isDir = leaf.parent === undefined

        return <>
            {isDir && <i className="fa fa-folder me-1 text-warning"></i>}
            <span className={isDir ? "" : "ms-5"}>{leaf.name}</span>

            {leaf.parent === undefined && <a href="/#"
                name="new"
                className="ms-2 text-success font-70pc"
                data-event="add-folder"
                data-id={leaf.id}
                data-parent={leaf.parent}
                data-level={leaf.level}
                onClick={(e) => onChange(e)}>
                <i className="fa fa-plus"></i>
            </a>}
        </>
    }

    return <div className="container-fluid pt-3">
        <div className="row">
            <div className="col-2 overflow-auto pd-5">
                <TreeView values={templateState.values}
                    getTreeNode={getTreeNode}>
                </TreeView>
            </div>
            {frm.parent !== undefined && <div className="col-10">
                <TemplateDetail template={frm}></TemplateDetail>
            </div>}
        </div>
    </div>
}