import { createContext } from "react"

export const navbarStateINI = { focus: undefined, isShowOutput: false }
const navbarContext = createContext({ ...navbarStateINI })
export default navbarContext