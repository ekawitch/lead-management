import { createContext } from "react"

export const toolbarStateINI = { focus: undefined, isShowOutput: false, tool: "100%" }
const toolbarContext = createContext({ ...toolbarStateINI })
export default toolbarContext