import { createContext } from "react"

export const mouseStateINI = {
    level: undefined,
    shape: undefined,
    code: undefined,
    action: undefined
}
const mouseContext = createContext({ ...mouseStateINI })
export default mouseContext