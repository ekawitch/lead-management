import { createContext } from "react";

export const fieldHelperINI = {
    allowTemplate: "",
    append: "",
    event: "",
    field: "",
    isSelection: false,
    isShow: false,
    returnEvent: "",
    segmentationSeq: "",
    seq2: "",
    seq: "",
    sourceCode: "",
    staging: "",
    tableSeq: "",
    type: ""
}
const fieldHelperContext = createContext({ ...fieldHelperINI })
export default fieldHelperContext
const fieldHelperType = {
    isSelection: "boolean",
    isShow: "boolean",
    segmentationSeq: "number",
    seq2: "number",
    seq: "number",
    tableSeq: "number",
}

export function popFieldHelper(
    e,
    setFieldHelperState,
    isShow = true) {
    const currentTarget = e.currentTarget
    const dataset = currentTarget.dataset

    const rslt = Object.keys(fieldHelperINI).reduce((acc, cur) => {
        if (dataset[cur]) {
            if (fieldHelperType[cur] === "boolean") {
                acc[cur] = dataset[cur] === "true"
            } else if (fieldHelperType[cur] === "number") {
                acc[cur] = parseInt(dataset[cur])
            } else {
                acc[cur] = dataset[cur]
            }
        }

        return acc
    }, {})

    setFieldHelperState({ ...rslt, isShow })
}
