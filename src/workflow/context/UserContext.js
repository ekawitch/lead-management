import { createContext } from "react";

export const userStateINI = {
    users: []
}
const userContext = createContext({
    userState: { ...userStateINI },
    setUserState: (state) => { }
})
export default userContext