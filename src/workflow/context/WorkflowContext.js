import { createContext } from "react";

export const workflowStateINI = { nodes: [], tree: {}, screen: "" }

const workflowContext = createContext({
    workflowState: { ...workflowStateINI },
    setWorkflowState: (state) => { }
})
export default workflowContext