import { createContext } from "react"

export const undoStateINI = { nodes: [[]], tool: [] }
const undoContext = createContext({ ...undoStateINI })
export default undoContext