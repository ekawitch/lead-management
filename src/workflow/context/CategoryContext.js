import { createContext } from "react";

export const categoryStateINI = {
    categories: []
}
const categoryContext = createContext({
    categoryState: { ...categoryStateINI },
    setCategoryState: (state) => { }
})
export default categoryContext