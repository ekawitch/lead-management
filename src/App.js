
import { useState } from 'react';
import './App.css';
import fieldHelperContext, { fieldHelperINI } from './workflow/context/FieldHelperContext';
import mouseContext, { mouseStateINI } from './workflow/context/MouseContext';
import navbarContext, { navbarStateINI } from './workflow/context/NavbarContext';
import toolbarContext, { toolbarStateINI } from './workflow/context/ToolbarContext';
import undoContext, { undoStateINI } from './workflow/context/UndoContext';
import workflowContext, { workflowStateINI } from './workflow/context/WorkflowContext';
import templateContext, { templateStateINI } from './workflow/context/TemplateContext';
import WorkflowRoutes from './workflow/pages/WorkflowRoutes';
import categoryContext, { categoryStateINI } from './workflow/context/CategoryContext';
import userContext, { userStateINI } from './workflow/context/UserContext';

function App() {
  const [userState, setUserState] = useState({ ...userStateINI })
  const [categoryState, setCategoryState] = useState({ ...categoryStateINI })
  const [workflowState, setWorkflowState] = useState({ ...workflowStateINI })
  const [navbarState, setNavbarState] = useState({ ...navbarStateINI })
  const [toolbarState, setToolbarState] = useState({ ...toolbarStateINI })
  const [mouseState, setMouseState] = useState({ ...mouseStateINI })
  const [undoState, setUndoState] = useState({ ...undoStateINI })
  const [fieldHelperState, setFieldHelperState] = useState({ ...fieldHelperINI })
  const [templateState, setTemplateState] = useState({ ...templateStateINI })

  return (<userContext.Provider value={{ userState, setUserState }}>
    <categoryContext.Provider value={{ categoryState, setCategoryState }}>
      <workflowContext.Provider value={{ workflowState, setWorkflowState }}>
        <navbarContext.Provider value={{ navbarState, setNavbarState }}>
          <toolbarContext.Provider value={{ toolbarState, setToolbarState }}>
            <mouseContext.Provider value={{ mouseState, setMouseState }}>
              <undoContext.Provider value={{ undoState, setUndoState }}>
                <fieldHelperContext.Provider value={{ fieldHelperState, setFieldHelperState }}>
                  <templateContext.Provider value={{ templateState, setTemplateState }}>
                    <WorkflowRoutes></WorkflowRoutes>
                  </templateContext.Provider>
                </fieldHelperContext.Provider>
              </undoContext.Provider>
            </mouseContext.Provider>
          </toolbarContext.Provider>
        </navbarContext.Provider>
      </workflowContext.Provider>
    </categoryContext.Provider>
  </userContext.Provider>)
}

export default App;